import Vue from 'vue'
import Router from 'vue-router'
//baseInfoManage
import ClassBaseInfo from '@/components/baseInfoManage/classBaseInfo'
import ClassBaseInfoOperate from '@/components/baseInfoManage/classBaseInfoOperate'
import StudentBaseInfo from '@/components/baseInfoManage/studentBaseInfo'
import ProfessionBaseInfo from '@/components/baseInfoManage/professionBaseInfo'
import NewProfessionBaseInfo from '@/components/baseInfoManage/newProfessionBaseInfo'
import TeacherBaseInfo from '@/components/baseInfoManage/teacherBaseInfo'
import StudentPersonInfo from '@/components/baseInfoManage/studentPersonInfo'
import TeacherPersonInfo from '@/components/baseInfoManage/teacherPersonInfo'
import ProfessionBaseInfoFix from '@/components/baseInfoManage/professionBaseInfoFix'
import ClassBaseInfoFix from '@/components/baseInfoManage/classBaseInfoFix'
import TeacherPersonInfoFix from '@/components/baseInfoManage/teacherPersonInfoFix'
import StudentPersonInfoFix from '@/components/baseInfoManage/studentPersonInfoFix'

//teach materialManage
import TeachMaterialDBManage from '@/components/teachMaterialManage/teachMaterialDBManage'
import NewTeachMaterial from '@/components/teachMaterialManage/newTeachMaterial'
import NewOrder from '@/components/teachMaterialManage/newOrder'
import ConfirmOrder from '@/components/teachMaterialManage/confirmOrder'
import CheckConfirmOrder from '@/components/teachMaterialManage/checkConfirmOrder'
import HistoryOrder from '@/components/teachMaterialManage/historyOrder'
import TeachMaterialFix from '@/components/teachMaterialManage/teachMaterialFix'

import TrainProgram from '@/components/professionTrainingManagement/trainProgram'
import TrainPlanStuff from '@/components/professionTrainingManagement/trainPlanStuff'
import TrainContent from '@/components/professionTrainingManagement/trainContent'
import PublishGraduation from '@/components/graduation/PublishGraduation'
import GraduationPlan from '@/components/graduation/GraduationPlan'
import CourseTitle from '@/components/graduation/CourseTitle'
import QueryGraduation from '@/components/graduation/queryGraduation'
import studentSelectCourse from '@/components/graduation/studentSelectCourse'
import ConfirmGraduationRes from '@/components/graduation/confirmGraduationRes'
import ClassQueryRes from '@/components/graduation/classQueryRes'
import TeacherQueryRes from '@/components/graduation/teacherQueryRes'
import StudentQueryRes from '@/components/graduation/StudentQueryRes'
import CommitGraduation from '@/components/graduation/CommitGraduation'
import ClassAssociate from '@/components/graduation/ClassAssociate'
import TeacherQueryStudent from '@/components/graduation/TeacherQueryStudent'
import GraduationInfo from '@/components/graduation/GraduationInfo'
import UpdateGraduationPlan from '@/components/graduation/UpdateGraduationPlan'
import UploadGraduation from '@/components/graduation/uploadGraduation'
import AssociateInfo from '@/components/graduation/AssociateInfo'


import lookGraduation from '@/components/graduation/lookGraduation'


//person info manage
import StudentInfoManage from '@/components/personInfomanage/studentInfoManage'
import TeacherInfoManage from '@/components/personInfomanage/teacherInfoManage'
import ChangePassword from '@/components/personInfomanage/changePassword'
//
import teacherWork from '@/components/statisticalAnalysis/teacherWork'
import teacherWorkQuery from '@/components/statisticalAnalysis/teacherWorkQuery'
import studentInfoQuery from '@/components/statisticalAnalysis/studentInfoQuery'
import majorPlan from '@/components/statisticalAnalysis/majorPlan'
import teacherInfoQuery from '@/components/statisticalAnalysis/teacherInfoQuery'

//courseInfo
import CourseBaseInfo from '@/components/courseInfo/courseBaseInfo'
import TeacherCourseInfo from '@/components/courseInfo/teacherCourseInfo'
import StudentCourseInfo from '@/components/courseInfo/studentCourseInfo'
import TeachingProgramInfo from '@/components/courseInfo/teachingProgramInfo'
import NewTeachingProgram from '@/components/courseInfo/newTeachingProgram'
import CourseArrangement from '@/components/courseInfo/courseArrangement'
import NewCourseArrangement from '@/components/courseInfo/newCourseArrangement'
import UpdateArrange from '@/components/courseInfo/updateArrange'
import CourseExamination from '@/components/courseInfo/courseExamination'
import AuditSituation from '@/components/courseInfo/auditSituation'
import CourseScheduleManage from '@/components/courseInfo/courseScheduleManage'
import NewCourseSchedule from '@/components/courseInfo/newCourseSchedule'
import TestProgramInfo from '@/components/courseInfo/testProgramInfo'
import NewTestProgram from '@/components/courseInfo/newTestProgram'
import NewTeachingDocument from '@/components/courseInfo/newTeachingDocument'
import TeachingDocumentManage from '@/components/courseInfo/teachingDocumentManage'
import TeachingDocument from '@/components/courseInfo/teachingDocument'
import CheckCourseArrangement from '@/components/courseInfo/checkCourseArrangement'
import UpdateCourseArrangement from '@/components/courseInfo/updateCourseArrangement'


//courseDesignInfo
import UploadCourseDesign from '@/components/courseDesignInfo/uploadCourseDesign'
import CheckCourseDesign from '@/components/courseDesignInfo/checkCourseDesign'
import NewCourseDesign from '@/components/courseDesignInfo/newCourseDesign'
import UpdateCourseDesign from '@/components/courseDesignInfo/updateCourseDesign'
import CourseDesignTeacher from '@/components/courseDesignInfo/courseDesignTeacher'
//profession training management
import NewTrainPlanStuff from '@/components/professionTrainingManagement/newTrainPlanStuff'
import UpdateTrainPlanStuff from '@/components/professionTrainingManagement/updateTrainPlanStuff'
//systemMange
import CollegeInit from '@/components/systemManage/collegeInit'
import studentManage from '@/components/systemManage/studentManage'
import teacherManage from '@/components/systemManage/teacherManage'
import DictionaryManage from '@/components/systemManage/dictionaryManage'
import RoleManage from '@/components/systemManage/roleManage'
//login
import Login from '@/components/login/Login'

Vue.use(Router)

export default new Router({
  routes: [
    //baseInfoManage
    {
      path: '/ClassBaseInfo',
      name: 'ClassBaseInfo',
      component: ClassBaseInfo
    },
    {
      path: '/ClassBaseInfoOperate',
      //path: '/ClassBaseInfoOperate/:classId',
      name: 'ClassBaseInfoOperate',
      component: ClassBaseInfoOperate
    },
    {
      path: '/StudentBaseInfo',
      name: 'StudentBaseInfo',
      component: StudentBaseInfo
    },
    {
      path: '/ProfessionBaseInfo',
      name: 'ProfessionBaseInfo',
      component: ProfessionBaseInfo
    },
    {
      path: '/NewProfessionBaseInfo',
      name: 'NewProfessionBaseInfo',
      component: NewProfessionBaseInfo
    },
    {
      path: '/TeacherBaseInfo',
      name: 'TeacherBaseInfo',
      component: TeacherBaseInfo
    },
    {
      path: '/StudentPersonInfo',
      name: 'StudentPersonInfo',
      component: StudentPersonInfo
    },
    {
      path: '/TeacherPersonInfo',
      name: 'TeacherPersonInfo',
      component: TeacherPersonInfo
    },
    {
      path: '/ProfessionBaseInfoFix/:majorId',
      name: 'ProfessionBaseInfoFix',
      component: ProfessionBaseInfoFix
    },
    {
      path: '/ClassBaseInfoFix/:classId',
      name: 'ClassBaseInfoFix',
      component: ClassBaseInfoFix
    },
    {
      path: '/TeacherPersonInfoFix/:teacherId',
      name: 'TeacherPersonInfoFix',
      component: TeacherPersonInfoFix
    },
    {
      path: '/StudentPersonInfoFix/:studentId',
      name: 'StudentPersonInfoFix',
      component: StudentPersonInfoFix
    },
    //
    {
      path: '/TrainProgram',
      name: 'TrainProgram',
      component: TrainProgram
    },
    {
      path: '/TrainPlanStuff',
      name: 'TrainPlanStuff',
      component: TrainPlanStuff
    },
    {
      path: '/TrainContent',
      name: 'TrainContent',
      component: TrainContent
    },
    // teach Material manage
    {
      path: '/TeachMaterialDBManage',
      name: 'TeachMaterialDBManage',
      component: TeachMaterialDBManage
    },
    {
      path: '/NewTeachMaterial',
      name: 'NewTeachMaterial',
      component: NewTeachMaterial
    },
    {
      path: '/TeachMaterialFix/:textbookId',
      name: 'TeachMaterialFix',
      component: TeachMaterialFix
    },
    {
      path: '/ConfirmOrder',
      name: 'ConfirmOrder',
      component: ConfirmOrder
    },
    {
      path: '/CheckConfirmOrder',
      name: 'CheckConfirmOrder',
      component: CheckConfirmOrder
    },
    {
      path: '/HistoryOrder',
      name: 'HistoryOrder',
      component: HistoryOrder
    },

    {
      path: '/PublishGraduation',
      name: 'PublishGraduation',
      component: PublishGraduation
    },
    {
      path: '/QueryGraduation',
      name: 'QueryGraduation',
      component: QueryGraduation
    },
    {
      path: '/NewOrder',
      name: 'NewOrder',
      component: NewOrder
    },
    //person info manage
    {
      path: '/StudentInfoManage',
      name: 'StudentInfoManage',
      component: StudentInfoManage
    },
    {
        path: '/TeacherInfoManage',
        name: 'TeacherInfoManage',
        component: TeacherInfoManage
      },
    {
      path: '/ChangePassword',
      name: 'ChangePassword',
      component: ChangePassword
    },
    {
      path: '/teacherWork',
      name: 'teacherWork',
      component: teacherWork
    },
    {
      path: '/teacherWorkQuery',
      name: 'teacherWorkQuery',
      component: teacherWorkQuery
    },
    {
      path: '/studentInfoQuery',
      name: 'studentInfoQuery',
      component: studentInfoQuery
    },
    //courseInfo
    {
      path: '/CourseBaseInfo',
      name: 'CourseBaseInfo',
      component: CourseBaseInfo
    },
    {
      path: '/TeacherCourseInfo',
      name: 'TeacherCourseInfo',
      component: TeacherCourseInfo
    },
    {
      path: '/StudentCourseInfo',
      name: 'StudentCourseInfo',
      component: StudentCourseInfo
    },
    {
      path: '/TeachingProgramInfo',
      name: 'TeachingProgramInfo',
      component: TeachingProgramInfo
    },
    {
      path: '/NewTeachingProgram',
      name: 'NewTeachingProgram',
      component: NewTeachingProgram
    },
    {
      path: '/CourseArrangement',
      name: 'CourseArrangement',
      component: CourseArrangement
    },
    {
      path: '/NewCourseArrangement',
      name: 'NewCourseArrangement',
      component: NewCourseArrangement
    },
    {
        path: '/UpdateArrange',
        name: 'UpdateArrange',
        component: UpdateArrange
      },
    {
      path: '/CourseExamination',
      name: 'CourseExamination',
      component: CourseExamination
    },
    {
      path: '/AuditSituation',
      name: 'AuditSituation',
      component: AuditSituation
    },
    {
      path: '/CourseScheduleManage',
      name: 'CourseScheduleManage',
      component: CourseScheduleManage
    },
    {
      path: '/NewCourseSchedule',
      name: 'NewCourseSchedule',
      component: NewCourseSchedule
    },
    {
      path: '/TestProgramInfo',
      name: 'TestProgramInfo',
      component: TestProgramInfo
    },
    {
      path: '/NewTestProgram',
      name: 'NewTestProgram',
      component: NewTestProgram
    },
    {
        path: '/NewTeachingDocument',
        name: 'NewTeachingDocument',
        component: NewTeachingDocument
     },
    {
      path: '/TeachingDocumentManage',
      name: 'TeachingDocumentManage',
      component: TeachingDocumentManage
    },
    {
        path: '/TeachingDocument',
        name: 'TeachingDocument',
        component: TeachingDocument
      },
    {
      path: '/CheckCourseArrangement',
      name: 'CheckCourseArrangement',
      component: CheckCourseArrangement
    },
    {
      path: '/UpdateCourseArrangement',
      name: 'UpdateCourseArrangement',
      component: UpdateCourseArrangement
    },
    //courseDesignInfo
    {
      path: '/UploadCourseDesign',
      name: 'UploadCourseDesign',
      component: UploadCourseDesign
    },
    {
      path: '/CheckCourseDesign',
      name: 'CheckCourseDesign',
      component: CheckCourseDesign
    },
    {
      path: '/NewCourseDesign',
      name: 'NewCourseDesign',
      component: NewCourseDesign
    },
    {
    	path: '/CourseDesignTeacher',
      name: 'CourseDesignTeacher',
      component: CourseDesignTeacher
    },
    {
      path: '/UpdateCourseDesign',
      name: 'UpdateCourseDesign',
      component: UpdateCourseDesign
    },
    {
      path: '/majorPlan',
      name: 'MajorPlan',
      component: majorPlan
    },
    {
      path: '/TeacherInfoQuery',
      name: 'teacherInfoQuery',
      component: teacherInfoQuery
    },
    {
      path: '/UploadGraduation',
      name: 'uploadGraduation',
      component: UploadGraduation
    },
    {
      path: '/lookGraduation',
      name: 'lookGraduation',
      component: lookGraduation
    },
    //profession training management
    {
      path: '/NewTrainPlanStuff',
      name: 'NewTrainPlanStuff',
      component: NewTrainPlanStuff
    },
    {
        path: '/UpdateTrainPlanStuff',
        name: 'UpdateTrainPlanStuff',
        component: UpdateTrainPlanStuff
      },
    {
      path: '/GraduationPlan',
      name: 'GraduationPlan',
      component: GraduationPlan
    },
    {
      path:'/UpdateGraduationPlan',
      name:'UpdateGraduationPlan',
      component:UpdateGraduationPlan
    },
    {
      path: '/CourseTitle',
      name: 'CourseTitle',
      component: CourseTitle
    },
    {
      path: '/StudentSelectCourse',
      name: 'StudentSelectCourse',
      component: studentSelectCourse
    },
    {
      path: '/ConfirmGraduationRes',
      name: 'ConfirmGraduationRes',
      component: ConfirmGraduationRes
    },
    {
      path: '/ClassQueryRes',
      name: 'ClassQueryRes',
      component: ClassQueryRes
    },
    {
      path: '/TeacherQueryRes',
      name: 'TeacherQueryRes',
      component: TeacherQueryRes
    },
    {
      path: '/StudentQueryRes',
      name: 'StudentQueryRes',
      component: StudentQueryRes
    },
    {
      path: '/CommitGraduation',
      name: 'CommitGraduation',
      component: CommitGraduation
    },
    {
      path: '/TeacherQueryStudent',
      name: 'TeacherQueryStudent',
      component: TeacherQueryStudent
    },
    {
      path: '/GraduationInfo',
      name: 'GraduationInfo',
      component: GraduationInfo
    },
    {
      path: '/ClassAssociate',
      name: 'ClassAssociate',
      component: ClassAssociate
    },
    {
      path: '/AssociateInfo',
      name: 'AssociateInfo',
      component: AssociateInfo
    },
    //systemMange
    {
      path: '/CollegeInit',
      name: 'CollegeInit',
      component: CollegeInit
    },
    {
      path: '/studentManage',
      name: 'studentManage',
      component: studentManage
    },
    {
      path: '/teacherManage',
      name: 'teacherManage',
      component: teacherManage
    },
    {
      path: '/DictionaryManage',
      name: 'DictionaryManage',
      component: DictionaryManage
    },
    {
      path: '/RoleManage',
      name: 'RoleManage',
      component: RoleManage
    },
    //Login
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
  ]
})

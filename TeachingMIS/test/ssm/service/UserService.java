package ssm.service;

import ssm.entity.User;

public interface UserService {

    User getUserById(String id);
}

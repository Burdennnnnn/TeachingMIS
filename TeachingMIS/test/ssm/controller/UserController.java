package ssm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import ssm.entity.User;
import ssm.service.UserService;

@Controller  //@Service @Component
@RequestMapping("/user")
public class UserController {

    
    @Autowired
    private UserService userService;
    @RequestMapping(value="/getUserById/{userId}")
    public String getUserById(@PathVariable("userId") String userId,Model model){
        User user = userService.getUserById(userId);
        model.addAttribute("userName", user.getName());
        return "success";
    }
}

package ssm.test;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ssm.entity.User;
import ssm.mapper.UserMapper;

/**
 * 配置spring和junit整合，junit启动时加载springIOC容器
 * spring-test,junit
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/spring-mybatis.xml"})
public class TestMapper {

    @Resource
    private UserMapper userMapper;
    
    @Test
    public void testGetUser(){
        User user = userMapper.selectByPrimaryKey("1");
        System.out.println(user);
    }
}

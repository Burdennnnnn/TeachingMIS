package ssm.test;

import java.sql.SQLException;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import teaching.entity.Major;
import teaching.service.baseInfoManage.majorBaseInfo.MajorBaseInfoService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/spring-mybatis.xml","classpath:spring/spring-service.xml"})
public class TestService {

    /*@Resource
    private UserService userService;*/
    @Resource
    private MajorBaseInfoService majorBaseInfoService;
    /*@Test
    public void testGetUser(){
        User user = userService.getUserById("1");
        System.out.println(user);
    }*/
    @Test
    public void testAddMajor() throws SQLException{
    	Major major = new Major();
    	major.setMajorId("111111");
    	major.setCollegeId("111111");
    	major.setMajorManager("张三");
    	
    	int status = majorBaseInfoService.addMajor(major);
    	System.out.println(status);
    }
}

package teaching.controller.SystemManage;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.service.SystemManage.StudentManageService;

@Controller
@RequestMapping("studentManage")
public class StudentManage {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private StudentManageService studentManageService;
	
	@RequestMapping("initPassword")
	public @ResponseBody Map<String,Object> initPassword(@RequestBody Map<String,String> request){
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			List<String> studentIDs = studentManageService.getStudentIds(request);
			Map<String,String> userInfo = new HashMap<String,String>();
			userInfo.put("password", request.get("password"));
			for(String studetID:studentIDs){
				userInfo.put("userID", studetID);
				userInfo.put("roleID", "1");
				studentManageService.addUser(userInfo);
				studentManageService.addUserRole(userInfo);
			}
		} catch (SQLException e) {
			result.put("error", e.getMessage());
		}
		return result;
	}
	
	@RequestMapping("deleteUser")
	public @ResponseBody Map<String,Object> deleteUser(@RequestBody Map<String,String> request){
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			List<String> studentIDs = studentManageService.getStudentIds(request);
			for(String studetID:studentIDs){
				studentManageService.deleteUser(studetID);
			}
		} catch (SQLException e) {
			result.put("error", e.getMessage());
		}
		return result;
	}
	
}

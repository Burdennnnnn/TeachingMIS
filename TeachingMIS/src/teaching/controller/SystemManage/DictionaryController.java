package teaching.controller.SystemManage;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.service.SystemManage.DictionaryService;

@Controller
@RequestMapping("dictionary")
public class DictionaryController {

	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private DictionaryService dictionaryService;
	
	@RequestMapping("getTree")
	public @ResponseBody Map<String,Object>  getTree(){
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			List<Map<String,Object>> tree =  dictionaryService.getOneForTree();
			for(Map<String,Object> one:tree){
				List<Map<String,Object>> twoTree = dictionaryService.getTwoForTree((String) one.get("id"));
				one.put("children", twoTree);
			}
			result.put("tree", tree);
		} catch (SQLException e) {
			result.put("error", e.getMessage());
		}
		return result;
	}
	
	@RequestMapping("getOneDictionary")
	public @ResponseBody Map<String,Object>  getOneDictionary(){
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			List<Map<String,Object>> dictionary =  dictionaryService.getOneDictionary();
			result.put("dictionary", dictionary);
		} catch (SQLException e) {
			result.put("error", e.getMessage());
		}
		return result;
	}
	
	@RequestMapping("getDictionnaryByUpId")
	public @ResponseBody Map<String,Object>  getDictionnaryByUpId(@RequestBody Map<String,String> request){
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			Map<String,String> id = new HashMap<String,String>();
			id.put("updictionaryOptionID", request.get("dictionaryOptionID"));
			List<Map<String,Object>> dictionary =  dictionaryService.getDictionnaryByUpId(id);
			result.put("dictionary", dictionary);
		} catch (SQLException e) {
			result.put("error", e.getMessage());
		}
		return result;
	}
	
	@RequestMapping("addDictionary")
	public @ResponseBody Map<String,Object>  addDictionary(@RequestBody Map<String,String> dictionary){
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			dictionaryService.addDictionary(dictionary);
		} catch (SQLException e) {
			result.put("error", e.getMessage());
		}
		return result;
	}
	
	
	@RequestMapping("getDictionary")
	public @ResponseBody Map<String,Object>  getDictionary(@RequestBody Map<String,String> request){
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			Map<String,String> dictionary = dictionaryService.getDictionary(request.get("dictionaryOptionID"));
			result.put("dictionary", dictionary);
		} catch (SQLException e) {
			result.put("error", e.getMessage());
		}
		return result;
	}
	
	@RequestMapping("updateDictionary")
	public @ResponseBody Map<String,Object>  updateDictionary(@RequestBody Map<String,String> dictionary){
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			dictionaryService.updateDictionary(dictionary);
		} catch (SQLException e) {
			result.put("error", e.getMessage());
		}
		return result;
	}
	
	@RequestMapping("deleteDictionary")
	public @ResponseBody Map<String,Object>  deleteDictionary(@RequestBody Map<String,String> dictionary){
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			dictionaryService.deleteDictionary(dictionary.get("dictionaryID"));
		} catch (SQLException e) {
			result.put("error", e.getMessage());
		}
		return result;
	}
	
}

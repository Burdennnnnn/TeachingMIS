package teaching.controller.SystemManage;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.service.SystemManage.RoleService;

@Controller
@RequestMapping("role")
public class RoleController {

	@Autowired
	private RoleService roleService;
	
	@RequestMapping("getRole")
	@ResponseBody
	public List<Map<String,Object>> getRole(HttpServletRequest request,HttpServletResponse response){
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		try {
			list=roleService.getRole();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@RequestMapping("getOneRole")
	@ResponseBody
	public Map<String,Object> getOneRole(HttpServletRequest request,HttpServletResponse response){
		String roleID = request.getParameter("roleID");
		Map<String,Object> map = new HashMap<>();
		try {
			map=roleService.getOneRole(roleID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping("addRole")
	@ResponseBody
	public Map<String,Object> addRole(HttpServletRequest request,HttpServletResponse response){
		String roleID = request.getParameter("roleID");
		String roleName = request.getParameter("roleName");
		String description = request.getParameter("description");
		String remark1 = request.getParameter("remark1");
		String isUse = request.getParameter("isUse");
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("roleID", roleID);
		map.put("roleName", roleName);
		map.put("description", description);
		map.put("remark1", remark1);
		map.put("isUse", isUse);
		Map<String,Object> resultMap = new HashMap<String, Object>();
		try {
			int status = roleService.addRole(map);
			if(status == 1){
				resultMap.put("message", "添加成功！");
			}else{
				resultMap.put("message", "添加失败！");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultMap;
	}
	
	@RequestMapping("updateRole")
	@ResponseBody
	public Map<String,Object> updateRole(HttpServletRequest request,HttpServletResponse response){
		String roleID = request.getParameter("roleID");
		String roleName = request.getParameter("roleName");
		String description = request.getParameter("description");
		String remark1 = request.getParameter("remark1");
		String isUse = request.getParameter("isUse");
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("roleID", roleID);
		map.put("roleName", roleName);
		map.put("description", description);
		map.put("remark1", remark1);
		map.put("isUse", isUse);
		Map<String,Object> resultMap = new HashMap<String, Object>();
		try {
			int status = roleService.updateRole(map);
			if(status == 1){
				resultMap.put("message", "修改成功！");
			}else{
				resultMap.put("message", "修改失败！");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultMap;
	}
	
	@RequestMapping("deleteRole")
	@ResponseBody
	public Map<String,Object> deleteRole(HttpServletRequest request,HttpServletResponse response){
		String roleID = request.getParameter("roleID");
		Map<String,Object> resultMap = new HashMap<String, Object>();
		try {
			int status = roleService.deleteRole(roleID);
			if(status == 1){
				resultMap.put("message", "删除成功！");
			}else{
				resultMap.put("message", "删除失败！");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultMap;
	}
	
	@RequestMapping("getAllQX")
	@ResponseBody
	public List<Map<String,Object>> getallQX(HttpServletRequest request,HttpServletResponse response) throws SQLException{
		List<Map<String,Object>> firstList = new ArrayList<>();
		firstList = roleService.getFirstQX();
		for (Map<String, Object> map : firstList) {
			List<Map<String,Object>> childMap = new ArrayList<>();
			childMap = roleService.getChildQX((String) map.get("moduleID"));
			map.put("children", childMap);
		}
		return firstList;	
	}
	
	@RequestMapping("getRoleQX")
	@ResponseBody
	public Map<String,Object> getRoleQX(HttpServletRequest request,HttpServletResponse response) throws SQLException{
		String roleID = request.getParameter("roleID");
		List<Map<String,Object>> list = new ArrayList<>();
		list=roleService.getRoleQXF(roleID);
		List<Map<String,Object>> list1 = new ArrayList<>();
		list1=roleService.getRoleQXC(roleID);
		Map<String,Object> map = new HashMap<>();
		map.put("first", list);
		map.put("child", list1);
		return map;	
	}
	
	@RequestMapping("addRoleQX")
	@ResponseBody
	public Map<String,Object> addRoleQX(HttpServletRequest request,HttpServletResponse response) throws SQLException{
		String roleID = request.getParameter("roleID");
		roleService.deleteRoleQX(roleID);
		String menuID = request.getParameter("menuID");
		String[] IDs = menuID.split(",");
		
		Set<String> set = new TreeSet<String>();//新建一个set集合
        for (String i : IDs) {
            set.add(i);
        }
        Object[] arr =  set.toArray();
        String[] ID = new String[arr.length];
        for (int i = 0;i<arr.length;i++) {
			ID[i] = (String) arr[i];
		}
		List<Map<String,Object>> list = new ArrayList<>();
		for (String id : ID) {
			Map<String,Object> map = new HashMap<>();
			map.put("roleID", roleID);
			map.put("menuID", id);
			list.add(map);
		}
		int status = roleService.addRoleQX(list);
		Map<String,Object> map = new HashMap<>();
		if(status == IDs.length){
			map.put("message", "修改成功！");
		}else{
			map.put("message", "修改失败！");
		}
		return map;	
	}
	
}

package teaching.controller.SystemManage;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.service.SystemManage.StudentManageService;

@Controller
@RequestMapping("teacherManage")
public class TeacherManage {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private StudentManageService studentManageService;
	
	@RequestMapping("createTUser")
	public @ResponseBody Map<String,Object> initPassword(@RequestBody Map<String,String> request){
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			
			Map<String,String> userInfo = new HashMap<String,String>();
			userInfo.put("password", request.get("password"));
			userInfo.put("userID", request.get("teacherID"));
			studentManageService.addUser(userInfo);
		} catch (SQLException e) {
			result.put("error", e.getMessage());
		}
		return result;
	}
	
	@RequestMapping("deleteTUser")
	public @ResponseBody Map<String,Object> deleteUser(@RequestBody Map<String,String> request){
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			String userID = request.get("teacherID");
		    studentManageService.deleteUser(userID);
		} catch (SQLException e) {
			result.put("error", e.getMessage());
		}
		return result;
	}
	
}

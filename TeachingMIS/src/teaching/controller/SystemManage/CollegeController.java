package teaching.controller.SystemManage;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.entity.College;
import teaching.service.SystemManage.CollegeService;

@Controller
@RequestMapping("college")
public class CollegeController {

	@Autowired
	private CollegeService collegeService;
	@RequestMapping("save")
	@ResponseBody
	public Map<String,Object> save(@RequestBody College college){
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			String collegeID = collegeService.getCollege().getCollegeID();
			if(collegeID != null && !collegeID.equals("")){
				if(collegeService.update(college)>0){
					result.put("message", "保存成功");
				}
			}else{
				if(collegeService.insert(college)>0){
					result.put("message", "保存成功");
				}
			}
		} catch (SQLException e) {
			result.put("message", e.getMessage());
		}
		return result;
	}
	
	@RequestMapping("get")
	@ResponseBody
	public Map<String,Object> get(){
		Map<String,Object> result = new HashMap<String,Object>();
		College college = new College();
		try {
			college = collegeService.getCollege();
			
		} catch (SQLException e) {
			result.put("message", e.getMessage());
		}
		result.put("college", college);
		return result;
	}
}

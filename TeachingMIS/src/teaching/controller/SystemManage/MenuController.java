package teaching.controller.SystemManage;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.entity.User;
import teaching.service.SystemManage.MenuService;

@Controller
@RequestMapping("menu")
public class MenuController {

	@Autowired
	private MenuService menuService;
	
	@RequestMapping("getMenu")
	@ResponseBody
	public List<Map<String,Object>> getMenu(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws SQLException{
		
		//要用session 现在还没有用
		//String userID = request.getParameter("userID");
		//String userID = "201320010101";
		User user = (User) session.getAttribute("loginuser");
		
		String userID = user.getUserID();
		System.out.println(userID+"********************");
		List<Map<String,Object>> firstList = new ArrayList<>();
		firstList = menuService.getFIrstMenu(userID);
		for (Map<String, Object> map : firstList) {
			List<Map<String,Object>> childList = new ArrayList<>();
			Map<String,Object> map1 = new HashMap<String, Object>();
			map1.put("userID", userID);
			map1.put("moduleID", map.get("moduleID"));
			childList = menuService.getChildMenu(map1);
			map.put("option", childList);
		}
		return firstList;
	}
	
}

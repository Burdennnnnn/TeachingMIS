package teaching.controller.designManage.graduteProgram;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.Region;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import teaching.entity.User;
import teaching.service.baseInfoManage.common.ExcelUtil;
import teaching.service.designManage.graduateProgram.GraduateProgramService;
import teaching.util.file.word2pdf.WordToPDFUtil;

/**
 * 
 */
@Controller
@RequestMapping(value = "graduateProgram")
public class GraduateProgramController {

	@Autowired
	private GraduateProgramService graduateProgramService;

	// graduateProgramID,teacherID,majorID,term,academicYear,graduateTitle,titleAttachment,
	// titleAttachmentName,titleSource,itemCategoryName,itemCategoryID,num,titleIntroduce
	@RequestMapping(value = "/addGraduateProgram")
	@ResponseBody
	public Map<String, Object> addGraduateProgram(HttpServletRequest request, HttpServletResponse response,
			HttpSession session)throws IllegalStateException, IOException {
		
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		
		String graduateProgramID = UUID.randomUUID().toString().replace("-", "");
		String graduateProPlanID = request.getParameter("graduateProPlanID");
		User user = (User) session.getAttribute("loginuser");
		
		String teacherID = user.getUserID();
		//String teacherID = request.getParameter("teacherID");
		String majorID = request.getParameter("majorID");
		String term = request.getParameter("term");
		String academicYear = request.getParameter("academicYear");
		String graduateTitle = request.getParameter("graduateTitle");
		String titleSource = request.getParameter("titleSource");
		String itemCategoryName = request.getParameter("itemCategoryName");
		String itemCategoryID = request.getParameter("itemCategoryID");
		String num = request.getParameter("num");
		String titleIntroduce = request.getParameter("titleIntroduce");
		
		String titleAttachmentName = request.getParameter("titleAttachmentName");
		String titleAttachment = request.getParameter("titleAttachment");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("graduateProgramID", graduateProgramID);
		map.put("graduateProPlanID", graduateProPlanID);
		map.put("teacherID", teacherID);
		map.put("majorID", majorID);
		map.put("term", term);
		map.put("academicYear", academicYear);
		map.put("graduateTitle", graduateTitle);
		map.put("titleSource", titleSource);
		map.put("itemCategoryName", itemCategoryName);
		map.put("itemCategoryID", itemCategoryID);
		map.put("num", num);
		map.put("titleIntroduce", titleIntroduce);
		map.put("titleAttachmentName", titleAttachmentName);
		map.put("titleAttachment", titleAttachment);
		Map<String, Object> messageMap = new HashMap<>();
		try {
			int status = graduateProgramService.addGraduateProgram(map);
			if (status == 1) {
				messageMap.put("message", "添加成功");
			} else {
				messageMap.put("message", "添加失败");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return messageMap;
	}
	
	@RequestMapping(value = "/uploadTitleAttachment")
	@ResponseBody
	public Map<String,Object>uploadTitleAttachment (@RequestParam("titleAttachmentName") MultipartFile file,HttpServletRequest request) {
		String fileName = file.getOriginalFilename();
		String prefix=fileName.substring(fileName.lastIndexOf(".")+1);
		String saveFileName = UUID.randomUUID().toString().replaceAll("-", "") + "." + prefix;
		String path = "file/designFileCS/graduateProgram";
		String pathtrue = path + saveFileName;
		String realPath = request.getSession().getServletContext().getRealPath("/")+"/"+pathtrue;
		
		
		Map<String,Object> map = new HashMap<>();
		
		if(prefix.equals("docx") || prefix.equals("doc")){
			String saveFileName1 = null ;
			if((prefix.equals("docx"))){
				saveFileName1 = saveFileName.replace("docx", "pdf");
				fileName = fileName.replace("docx", "pdf");
			}else if(prefix.equals("doc")){
				saveFileName1 = saveFileName.replace("doc", "pdf");
				fileName = fileName.replace("doc", "pdf");
			}
			String pathtrue1 = path + saveFileName1;
			String realPath1 = request.getSession().getServletContext().getRealPath("/")+""+pathtrue1;	
			try {
				file.transferTo(new File(realPath));
				if(new WordToPDFUtil().word2pdf(realPath, realPath1)){
					System.out.println("转换成功");
				}else{
					System.out.println("转换失败");
				}
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			map.put("titleAttachmentName", fileName);
			map.put("titleAttachment", pathtrue1);
		}else{
			try {
				file.transferTo(new File(realPath));
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			map.put("titleAttachmentName", fileName);
			map.put("titleAttachment", pathtrue);
		}
		
		return map;
	}

	@RequestMapping(value = "/getGraduateProgram")
	@ResponseBody
	public Map<String, Object> getGraduateProgram(@RequestParam(value="page" ,required=false) Integer page,@RequestParam(value="pageSize" ,required=false) Integer pageSize,
			HttpServletRequest request, HttpServletResponse response) {
		int start = (page-1)*pageSize;
		String majorID = request.getParameter("majorID");
		String term = request.getParameter("term");
		String academicYear = request.getParameter("academicYear");
		Map<String,Object> map = new HashMap<>();
		map.put("majorID", majorID);
		map.put("term", term);
		map.put("academicYear", academicYear);
		map.put("start", start);
		map.put("pageSize", pageSize);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String,Object> resultMap = new HashMap<>();
		try {
			resultMap.put("totalNum", graduateProgramService.getGraduateProgramNum(map));
			list = graduateProgramService.getGraduateProgram(map);
			resultMap.put("program", list);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/getGraduateProgramStudent")
	@ResponseBody
	public Map<String, Object> getGraduateProgramStudent(HttpServletRequest request,
			HttpServletResponse response,HttpSession session) {
		User user = (User) session.getAttribute("loginuser");
		
		String studentID = user.getUserID();
		//String studentID = request.getParameter("studentID");
		Calendar date = Calendar.getInstance();
		String academicYear = date.get(Calendar.YEAR)+"";
		Map<String,Object> map = new HashMap<>();
		map.put("studentID", studentID);
		map.put("academicYear", academicYear);
		List<Map<String, Object>> list1 = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		Map<String,Object> resultMap = new HashMap<>();
		try {
			list1 = graduateProgramService.getGraduateProgramStudent(map);
			list2 = graduateProgramService.getGraduateInStudent(studentID);
			resultMap.put("all", list1);
			resultMap.put("student", list2);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/addStudentGraduate")
	@ResponseBody
	public Map<String, Object> addStudentGraduate(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {

		String graduateProgramID = request.getParameter("graduateProgramID");
		User user = (User) session.getAttribute("loginuser");
		
		String studentID = user.getUserID();
		//String studentID = request.getParameter("studentID");
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String selectTime = format.format(date);
		Map<String, Object> map = new HashMap<>();
		map.put("graduateProgramID", graduateProgramID);
		map.put("studentID", studentID);
		map.put("selectTime", selectTime);
		Map<String, Object> messageMap = new HashMap<>();
		try {
			int status = graduateProgramService.addStudentGraduate(map);
			if (status == 1) {
				messageMap.put("message", "申请成功");
			} else {
				messageMap.put("message", "申请失败");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return messageMap;
	}

	@RequestMapping(value = "/deleteStudentGraduate")
	@ResponseBody
	public Map<String, Object> deleteStudentGraduate(HttpServletRequest request, 
			HttpSession session,HttpServletResponse response) {
		String studentID = request.getParameter("studentID");
		Map<String, Object> map = new HashMap<>();
		try {
			int status = graduateProgramService.deleteStudentGraduate(studentID);
			if(status ==1){
				map.put("message", "取消成功");
			}else{
				map.put("message", "取消失败");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/getStudentNumByTeacher")
	@ResponseBody
	public Map<String, Object> getStudentNumByTeacher(@RequestParam(value="page" ,required=false) int page,@RequestParam(value="pageSize" ,required=false) int pageSize,
			HttpServletRequest request, HttpServletResponse response,HttpSession session) {
		int start = (page-1)*pageSize;
		User user = (User) session.getAttribute("loginuser");
		
		String teacherID = user.getUserID();
		//String teacherID = request.getParameter("teacherID");
		String academicYear = request.getParameter("academicYear");
		Map<String,Object> map = new HashMap<>();
		map.put("teacherID", teacherID);
		map.put("academicYear", academicYear);
		map.put("start", start);
		map.put("pageSize", pageSize);
		List<Map<String, Object>> list = new ArrayList<>();
		Map<String,Object> resultMap = new HashMap<>();
		try {
			list = graduateProgramService.getStudentNumByTeacher(map);
			resultMap.put("totalNum", graduateProgramService.getStudentByTeacherNum(map));
			resultMap.put("student", list);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultMap;
	}

	@RequestMapping(value = "/getStudentNum")
	@ResponseBody
	public Map<String, Object> getStudentNum(@RequestParam(value="page" ,required=false) int page,@RequestParam(value="pageSize" ,required=false) int pageSize,
			HttpServletRequest request, HttpServletResponse response) {
		int start = (page-1)*pageSize;
		String majorID = request.getParameter("majorID");
		String academicYear = request.getParameter("academicYear");
		Map<String,Object> map = new HashMap<>();
		map.put("majorID", majorID);
		map.put("academicYear", academicYear);
		map.put("start", start);
		map.put("pageSize", pageSize);
		List<Map<String, Object>> list = new ArrayList<>();
		Map<String,Object> resultMap = new HashMap<>();
		try {
			list = graduateProgramService.getStudentNum(map);
			resultMap.put("totalNum", graduateProgramService.getStudentTotalNum(map));
			resultMap.put("program", list);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultMap;
	}

	@RequestMapping(value = "/getStudentNOBelong")
	@ResponseBody
	public List<Map<String, Object>> getStudentNOCheck(HttpServletRequest request, HttpServletResponse response) {
		Calendar date = Calendar.getInstance();
		String enrollmentTime = date.get(Calendar.YEAR)-4+"";
		String graduateProgramID = request.getParameter("graduateProgramID");
		Map<String,Object> map = new HashMap<>();
		map.put("enrollmentTime", enrollmentTime);
		map.put("graduateProgramID", graduateProgramID);
		List<Map<String, Object>> list = new ArrayList<>();
		try {
			list = graduateProgramService.getStudentNOBelong(map);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@RequestMapping(value = "/teacherChooseStudentDelete")
	@ResponseBody
	public Map<String, Object> teacherChooseStudentDelete(HttpServletRequest request, HttpServletResponse response) {
		String IDs = request.getParameter("studentID");
		List<String> lists= new ArrayList<String>();
		String[] strs = IDs.split(",");
		for (String string : strs) {
			lists.add(string);
		}
		String graduateProgramID = request.getParameter("graduateProgramID");
		Map<Object, Object> map = new HashMap<>();
		map.put("list", lists);
		map.put("graduateProgramID", graduateProgramID);
		Map<String, Object> messageMap = new HashMap<>();
		try {
			int status = graduateProgramService.teacherChooseStudentDelete(map);
			if (status == 1) {
				messageMap.put("message", "取消成功");
			} else {
				messageMap.put("message", "取消失败");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return messageMap;
	}

	@RequestMapping(value = "/teacherChooseStudentAdd")
	@ResponseBody
	public Map<String, Object> teacherChooseStudentAdd(HttpServletRequest request, HttpServletResponse response) {
		String IDs = request.getParameter("studentID");
		List<String> lists= new ArrayList<String>();
		String[] strs = IDs.split(",");
		for (String string : strs) {
			lists.add(string);
		}
		String graduateProgramID = request.getParameter("graduateProgramID");
		Map<Object, Object> map = new HashMap<>();
		map.put("list", lists);
		map.put("graduateProgramID", graduateProgramID);
		Map<String, Object> messageMap = new HashMap<>();
		try {
			int status = graduateProgramService.teacherChooseStudentAdd(map);
			if (status == 1) {
				messageMap.put("message", "添加成功");
			} else {
				messageMap.put("message", "添加失败");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return messageMap;
	}
	
	@RequestMapping(value = "/teacherChooseStudent")
	@ResponseBody
	public Map<String, Object> teacherChooseStudent(HttpServletRequest request, HttpServletResponse response) {
		String IDs = request.getParameter("studentID");
		List<String> lists= new ArrayList<String>();
		String[] strs = IDs.split(",");
		for (String string : strs) {
			lists.add(string);
		}
		String graduateProgramID = request.getParameter("graduateProgramID");
		Map<Object, Object> map = new HashMap<>();
		map.put("list", lists);
		map.put("graduateProgramID", graduateProgramID);
		Map<String, Object> messageMap = new HashMap<>();
		try {
			int status = graduateProgramService.teacherChooseStudent(map);
			if (status == 1) {
				messageMap.put("message", "保存成功");
			} else {
				messageMap.put("message", "保存失败");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return messageMap;
	}

	@RequestMapping(value = "/getGraduateInfoStudent")
	@ResponseBody
	public List<Map<String, Object>> getGraduateInfoStudent(HttpServletRequest request, 
			HttpServletResponse response,HttpSession session) {
		//String studentID = (String) session.getAttribute("userID");
		User user = (User) session.getAttribute("loginuser");
		
		String studentID = user.getUserID();
		//String studentID = request.getParameter("studentID");
		List<Map<String, Object>> list = new ArrayList<>();
		try {
			list = graduateProgramService.getGraduateInfoStudent(studentID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@RequestMapping(value = "/getStudentInGraduate")
	@ResponseBody
	public List<Map<String, Object>> getStudentInGraduate(HttpServletRequest request, HttpServletResponse response){
		String graduateProgramID = request.getParameter("graduateProgramID");
		List<Map<String, Object>> list = new ArrayList<>();
		try {
			list = graduateProgramService.getStudentInGraduate(graduateProgramID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@RequestMapping(value = "/getStudentInfo")
	@ResponseBody
	public List<Map<String, Object>> getStudentInfo(HttpServletRequest request, HttpServletResponse response){
		String graduateProgramID = request.getParameter("graduateProgramID");
		List<Map<String, Object>> list = new ArrayList<>();
		try {
			list = graduateProgramService.getStudentInfo(graduateProgramID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	@RequestMapping(value = "/getGraduateInfoTeacher")
	@ResponseBody
	public Map<String, Object> getGraduateInfoTeacher(@RequestParam(value="page" ,required=false) int page,@RequestParam(value="pageSize" ,required=false) int pageSize,
			HttpServletRequest request, HttpServletResponse response,HttpSession session) {
		int start = (page-1)*pageSize;
		User user = (User) session.getAttribute("loginuser");
		
		String teacherID = user.getUserID();
		//String teacherID =request.getParameter("teacherID");
		String academicYear = request.getParameter("academicYear");
		Map<String,Object> map = new HashMap<>();
		map.put("teacherID", teacherID);
		map.put("academicYear", academicYear);
		map.put("start", start);
		map.put("pageSize", pageSize);
		List<Map<String, Object>> list = new ArrayList<>();
		Map<String,Object> resultMap = new HashMap<>();
		try {
			list = graduateProgramService.getGraduateInfoTeacher(map);
			resultMap.put("totalNum", graduateProgramService.getGraduateInfoTeacherNum(map));
			resultMap.put("program", list);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/getGraduateInfoStaff")
	@ResponseBody
	public Map<String, Object> getGraduateInfoStaff(@RequestParam(value="page" ,required=false) int page,@RequestParam(value="pageSize" ,required=false) int pageSize,
			HttpServletRequest request, HttpServletResponse response) {
		int start = (page-1)*pageSize;
		String majorID = request.getParameter("majorID");
		String academicYear = request.getParameter("academicYear");
		Map<String,Object> map = new HashMap<>();
		map.put("majorID", majorID);
		map.put("academicYear", academicYear);
		map.put("start", start);
		map.put("pageSize", pageSize);
		List<Map<String, Object>> list = new ArrayList<>();
		Map<String,Object> resultMap = new HashMap<>();
		try {
			list = graduateProgramService.getGraduateInfoStaff(map);
			resultMap.put("totalNum", graduateProgramService.getGraduateInfoStaffNum(map));
			resultMap.put("program", list);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultMap;
	}
	
	//========================================
	@RequestMapping(value = "/getOneGraduateProgram")
	@ResponseBody
	public List<Map<String, Object>> getOneGraduateProgram(HttpServletRequest request, HttpServletResponse response) {
		String graduateProgramID = request.getParameter("graduateProgramID");
		Map<String,Object> map = new HashMap<>();
		map.put("graduateProgramID", graduateProgramID);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = graduateProgramService.getOneGraduateProgram(map);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	 @RequestMapping(value="exportProgram")
	 @ResponseBody
	 public Map<String,Object> exportMajor(HttpServletRequest request) throws FileNotFoundException, IOException{
		  
		     String majorID = request.getParameter("majorID");
		     String term = request.getParameter("term");
		     String academicYear = request.getParameter("academicYear");
			String basePath = this.getClass().getResource("/").getPath();
			if(System.getProperty("os.name").toLowerCase().indexOf("linux")>=0){
				basePath = basePath.substring(0, basePath.indexOf("/WEB-INF"));
			}else{
				basePath = basePath.substring(1, basePath.indexOf("/WEB-INF"));
			}
			
			String fileName = "graduateProgram.xls";		
			String templatePath = basePath + "/file/export/" + fileName;
			// 先读取模板 
		    POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(templatePath));
		    HSSFWorkbook wb = new HSSFWorkbook(fs);  
		    //在webbook中添加一个sheet,对应Excel文件中的sheet  
		    HSSFSheet sheet = wb.getSheetAt(0);  
		    //在sheet中添加表头第0行 
		    HSSFRow row;
		    HSSFCellStyle cellStyle = ExcelUtil.setCellStyle(wb);
		    //写入实体数据 ,数据从数据库得到，  
		    List<Map<String,Object>> list = null;
		    Map<String,Object> map2 = new HashMap<String, Object>();
		    map2.put("majorID", majorID);
		    map2.put("term", term);
		    map2.put("academicYear", academicYear);
		    try {
				list = graduateProgramService.getGraduateProgram(map2);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    if(list!=null){
		    	for (int i = 0; i < list.size(); i++) {
		    		 row = sheet.createRow( i + 1); 		    				                
		            row.createCell(0).setCellValue(list.get(i).get("graduateTitle")+""); 
		            row.getCell(0).setCellStyle(cellStyle);
		            row.createCell(1).setCellValue(list.get(i).get("teacherName")+""); 
		            row.getCell(1).setCellStyle(cellStyle);
		            row.createCell(2).setCellValue(list.get(i).get("majorName")+"");
		            row.getCell(2).setCellStyle(cellStyle);
		            row.createCell(3).setCellValue(list.get(i).get("num")+""); 
		            row.getCell(3).setCellStyle(cellStyle);
		            row.createCell(4).setCellValue(list.get(i).get("titleSource")+""); 
		            row.getCell(4).setCellStyle(cellStyle);
		            row.createCell(5).setCellValue(list.get(i).get("itemCategoryName")+""); 
		            row.getCell(5).setCellStyle(cellStyle);
		            row.createCell(6).setCellValue(list.get(i).get("itemCategoryID")+""); 
		            row.getCell(6).setCellStyle(cellStyle);
			                  		     	     
			    }  
		    }
		Map<String,Object> map = new HashMap<String, Object>(); 	
	    try{  
	    	String path = "file/export";
	    	String realPath = request.getSession().getServletContext().getRealPath("/"+path);
	    	String pathtrue = realPath + "/"+fileName;
	        FileOutputStream fout = new FileOutputStream(pathtrue);  
	        wb.write(fout);  
	        fout.close();  
	        String uri =  request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
	       
	        map.put("message", uri+"/"+"TeachingMIS/"+path+"/"+fileName);
	    }  
	    catch (Exception e){ 
	    
	    	map.put("message", "导出失败！");
	        e.printStackTrace();  
	    }  
		 
		 return map;
	 }
	 
	 @RequestMapping(value="exportGraduateInfoStaff")
	 @ResponseBody
	 public Map<String,Object> exportGraduateInfoStaff(HttpServletRequest request) throws FileNotFoundException, IOException{
		  
		     String majorID = request.getParameter("majorID");
		     String academicYear = request.getParameter("academicYear");
			String basePath = this.getClass().getResource("/").getPath();
			if(System.getProperty("os.name").toLowerCase().indexOf("linux")>=0){
				basePath = basePath.substring(0, basePath.indexOf("/WEB-INF"));
			}else{
				basePath = basePath.substring(1, basePath.indexOf("/WEB-INF"));
			}
			
			String fileName = "graduateStaff.xls";		
			String templatePath = basePath + "/file/export/" + fileName;
			// 先读取模板 
		    POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(templatePath));
		    HSSFWorkbook wb = new HSSFWorkbook(fs);  
		    //在webbook中添加一个sheet,对应Excel文件中的sheet  
		    HSSFSheet sheet = wb.getSheetAt(0);  
		    //在sheet中添加表头第0行 
		    HSSFRow row = null;
		    HSSFCellStyle cellStyle = ExcelUtil.setCellStyle(wb);
		    //写入实体数据 ,数据从数据库得到，  
		    List<Map<String,Object>> list = null;
		    Map<String,Object> map2 = new HashMap<String, Object>();
		    map2.put("majorID", majorID);
		    map2.put("academicYear", academicYear);
		    try {
				list = graduateProgramService.exportGraduateInfoStaff(map2);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    if(list!=null){
		    	for (int i = 0; i < list.size(); i++) {
		    		 row = sheet.createRow( i + 1); 	
		            row.createCell(0).setCellValue(list.get(i).get("graduateTitle")+""); 
		            row.getCell(0).setCellStyle(cellStyle);
		            row.createCell(1).setCellValue(list.get(i).get("teacherName")+""); 
		            row.getCell(1).setCellStyle(cellStyle);
		            row.createCell(2).setCellValue(list.get(i).get("majorName")+"");
		            row.getCell(2).setCellStyle(cellStyle);
		            row.createCell(3).setCellValue(list.get(i).get("num")+""); 
		            row.getCell(3).setCellStyle(cellStyle);
		            row.createCell(4).setCellValue(list.get(i).get("number")+""); 
		            row.getCell(4).setCellStyle(cellStyle);
		            row.createCell(5).setCellValue(list.get(i).get("titleSource")+""); 
		            row.getCell(5).setCellStyle(cellStyle);
		            row.createCell(6).setCellValue(list.get(i).get("itemCategoryName")+""); 
		            row.getCell(6).setCellStyle(cellStyle);
		            row.createCell(7).setCellValue(list.get(i).get("itemCategoryID")+""); 
		            row.getCell(7).setCellStyle(cellStyle);
		            row.createCell(8).setCellValue(list.get(i).get("studentName")+""); 
		            row.getCell(8).setCellStyle(cellStyle);
		            row.createCell(9).setCellValue(list.get(i).get("className")+""); 
		            row.getCell(9).setCellStyle(cellStyle);	     	     
			    }  
		    }
		   
		    
		Map<String,Object> map = new HashMap<String, Object>(); 	
	    try{  
	    	String path = "file/export";
	    	String realPath = request.getSession().getServletContext().getRealPath("/"+path);
	    	String pathtrue = realPath + "/"+fileName;
	        FileOutputStream fout = new FileOutputStream(pathtrue);  
	        wb.write(fout);  
	        fout.close();  
	        String uri =  request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
	       
	        map.put("message", uri+"/"+"TeachingMIS/"+path+"/"+fileName);
	    }  
	    catch (Exception e){ 
	    
	    	map.put("message", "导出失败！");
	        e.printStackTrace();  
	    }  
		 
		 return map;
	 }
	 
	 @RequestMapping(value="getPlanTimeWithID")
	 @ResponseBody
	 public Map<String,Object> getPlanTimeWithID(HttpServletResponse response,HttpServletRequest request){
		 
		 String graduateProPlanID = request.getParameter("graduateProPlanID");
		 Map<String,Object> map = new HashMap<>();
		 try {
			map = graduateProgramService.getPlanTimeWithID(graduateProPlanID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	 }
	 
}

package teaching.controller.designManage.graduteProgram;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import teaching.service.designManage.graduateProgram.GraduateResultService;
import teaching.util.file.word2pdf.WordToPDFUtil;

/**
*          
*/
@Controller
@RequestMapping(value = "graduateResult")
public class GraduateResultController {

	@Autowired
	private GraduateResultService graduateResultService;
	
	@RequestMapping(value = "getStudentGraduate")
	@ResponseBody
	public Map<String,Object> getStudentGraduate(HttpServletRequest request,
			HttpSession session,HttpServletResponse response){
		String studentID = request.getParameter("studentID");
		Map<String,Object> map = new HashMap<String, Object>();
		try {
			map = graduateResultService.getStudentGraduate(studentID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/addGraduateResult")
	@ResponseBody
	public Map<String,Object> addGraduateResult (HttpServletRequest request,
			HttpSession session,HttpServletResponse response){
		String graduateResultID = UUID.randomUUID().toString().replace("-", "");
		String graduateProgramID =request.getParameter("graduateProgramID");
		String studentID = request.getParameter("studentID");
		String remark1 = request.getParameter("remark1");
		String graduateResultName = request.getParameter("graduateResultName");
		String graduateResultStore = request.getParameter("graduateResultStore");
		String graduateResult = request.getParameter("graduateResult");
		String dissertation = request.getParameter("dissertation");
		String dissertationName = request.getParameter("dissertationName");
		String dissertationStore = request.getParameter("dissertationStore");
		String uploadTime = request.getParameter("uploadTime");
		Map<String,Object> map = new HashMap<>();
		map.put("graduateResultID", graduateResultID);
		map.put("graduateProgramID", graduateProgramID);
		map.put("studentID", studentID);
		map.put("graduateResultName", graduateResultName);
		map.put("graduateResultStore", graduateResultStore);
		map.put("graduateResult", graduateResult);
		map.put("dissertation", dissertation);
		map.put("dissertationName", dissertationName);
		map.put("dissertationStore", dissertationStore);
		map.put("uploadTime", uploadTime);
		map.put("remark1", remark1);
		Map<String,Object> mesageMap = new HashMap<String, Object>();
		try {
			Map<String,Object> check = graduateResultService.getCheck(studentID);
			if(check.get("flag").equals(0)){
				int status = graduateResultService.addGraduateResult(map);
				if (status == 1) {
					mesageMap.put("message", "添加成功");
				} else {
					mesageMap.put("message", "添加失败");
				}
			}else{
				int status = graduateResultService.updateGraduateResult(map);
				if (status == 1) {
					mesageMap.put("message", "添加成功");
				} else {
					mesageMap.put("message", "添加失败");
				}
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mesageMap;
	}
	
	@RequestMapping(value = "/uploadDissertation")
	@ResponseBody
	public Map<String,Object>uploadDissertation (@RequestParam("dissertationName") MultipartFile file,HttpServletRequest request) {
		String fileName = file.getOriginalFilename();
		String prefix=fileName.substring(fileName.lastIndexOf(".")+1);
		String saveFileName = UUID.randomUUID().toString().replaceAll("-", "") + "." + prefix;
		String path = "file/designFileCS/graduateProgram";
		String pathtrue = path + saveFileName;
		String realPath = request.getSession().getServletContext().getRealPath("/")+"/"+pathtrue;
		
		
		Map<String,Object> map = new HashMap<>();
		
		if(prefix.equals("docx") || prefix.equals("doc")){
			String saveFileName1 = null ;
			if((prefix.equals("docx"))){
				saveFileName1 = saveFileName.replace("docx", "pdf");
				fileName = fileName.replace("docx", "pdf");
			}else if(prefix.equals("doc")){
				saveFileName1 = saveFileName.replace("doc", "pdf");
				fileName = fileName.replace("doc", "pdf");
			}
			String pathtrue1 = path + saveFileName1;
			String realPath1 = request.getSession().getServletContext().getRealPath("/")+""+pathtrue1;	
			try {
				file.transferTo(new File(realPath));
				if(new WordToPDFUtil().word2pdf(realPath, realPath1)){
					System.out.println("转换成功");
				}else{
					System.out.println("转换失败");
				}
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			map.put("dissertationName", fileName);
			map.put("dissertationStore", saveFileName1);
			map.put("dissertation", pathtrue1);
		}else{
			try {
				file.transferTo(new File(realPath));
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			map.put("dissertationName", fileName);
			map.put("dissertationStore", saveFileName);
			map.put("dissertation", pathtrue);
		}
		return map;
	}
	
	@RequestMapping(value = "/uploadGraduateResult")
	@ResponseBody
	public Map<String,Object>uploadGraduateResult (@RequestParam("graduateResultName") MultipartFile file,HttpServletRequest request){
		String fileName = file.getOriginalFilename();
		String prefix=fileName.substring(fileName.lastIndexOf(".")+1);
		String saveFileName = UUID.randomUUID().toString().replaceAll("-", "") + "." + prefix;
		String path = "file/designFileCS/graduateProgram";
		String pathtrue = path + saveFileName;
		String realPath = request.getSession().getServletContext().getRealPath("/")+"/"+pathtrue;
		
		
		Map<String,Object> map = new HashMap<>();
		
		if(prefix.equals("docx") || prefix.equals("doc")){
			String saveFileName1 = null ;
			if((prefix.equals("docx"))){
				saveFileName1 = saveFileName.replace("docx", "pdf");
				fileName = fileName.replace("docx", "pdf");
			}else if(prefix.equals("doc")){
				saveFileName1 = saveFileName.replace("doc", "pdf");
				fileName = fileName.replace("doc", "pdf");
			}
			String pathtrue1 = path + saveFileName1;
			String realPath1 = request.getSession().getServletContext().getRealPath("/")+""+pathtrue1;	
			try {
				file.transferTo(new File(realPath));
				if(new WordToPDFUtil().word2pdf(realPath, realPath1)){
					System.out.println("转换成功");
				}else{
					System.out.println("转换失败");
				}
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			map.put("graduateResultName", fileName);
			map.put("graduateResultStore", saveFileName1);
			map.put("graduateResult", pathtrue1);
		}else{
			try {
				file.transferTo(new File(realPath));
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			map.put("graduateResultName", fileName);
			map.put("graduateResultStore", saveFileName);
			map.put("graduateResult", pathtrue);
		}
		return map;
	}
	
	@RequestMapping(value = "/getGraduateResult")
	@ResponseBody
	public Map<String,Object>getGraduateResult (@RequestParam(value="page" ,required=false) Integer page,@RequestParam(value="pageSize" ,required=false) Integer pageSize,
			HttpServletRequest request, HttpServletResponse response){
		int start = (page-1)*pageSize;
		String majorID = request.getParameter("majorID");
		String academicYear = request.getParameter("academicYear");
		Map<String,Object> map = new HashMap<>();
		map.put("majorID", majorID);
		map.put("academicYear", academicYear);
		map.put("start", start);
		map.put("pageSize", pageSize);
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		Map<String,Object> resultMap = new HashMap<>();
		try {
			list = graduateResultService.getGraduateResult(map);
			resultMap.put("totalNum", graduateResultService.getGraduateResultNum(map));
			resultMap.put("result", list);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultMap;
	}
	
	@RequestMapping(value = "/updateCheck")
	@ResponseBody
	public Map<String,Object>updateCheck (HttpServletRequest request,HttpServletResponse response
			,HttpSession session){
		String studentID = request.getParameter("studentID");
		Map<String,Object> resultMap = new HashMap<>();
		
		try {
			int status = graduateResultService.updateCheck(studentID);
			if (status == 1) {
				resultMap.put("message", "提交成功");
			} else {
				resultMap.put("message", "提交失败");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultMap;
	}
	
}

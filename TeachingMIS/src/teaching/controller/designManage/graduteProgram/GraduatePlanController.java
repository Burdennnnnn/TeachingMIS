package teaching.controller.designManage.graduteProgram;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import teaching.entity.Major;
import teaching.service.designManage.graduateProgram.GraduatePlanService;
import teaching.util.file.word2pdf.WordToPDFUtil;

/**
 * 张哲 创建时间：2017年4月14日 下午5:18:19
 */
@Controller
@RequestMapping(value = "graduatePlan")
public class GraduatePlanController {

	@Autowired
	private GraduatePlanService graduatePlanService;
	// graduateProPlanID term academicYear
	// graduatePlanName graduatePlanStore graduatePlanContent uploader upTime
	// majorID

	@RequestMapping(value = "/addGraduatePlan")
	@ResponseBody
	public Map<String, Object> addGraduatePlan(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String graduateProPlanID = UUID.randomUUID().toString().replace("-", "");
		String term = request.getParameter("term");
		String academicYear = request.getParameter("academicYear");
		String graduatePlanName = request.getParameter("graduatePlanName");
		String graduatePlanStore = request.getParameter("graduatePlanStore");
		String graduatePlanContent = request.getParameter("graduatePlanContent");
		// 需要与前台商量获取当前登录人员姓名
		String uploader = request.getParameter("uploader");
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String upTime = format.format(date);
		String remark = request.getParameter("remark");
		String majorID = request.getParameter("majorID");
		String grade = request.getParameter("grade");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("graduateProPlanID", graduateProPlanID);
		map.put("term", term);
		map.put("academicYear", academicYear);
		map.put("graduatePlanName", graduatePlanName);
		map.put("graduatePlanStore", graduatePlanStore);
		map.put("graduatePlanContent", graduatePlanContent);
		map.put("uploader", uploader);
		map.put("upTime", upTime);
		map.put("majorID", majorID);
		map.put("grade", grade);
		map.put("remark", remark);
		Map<String, Object> mesageMap = new HashMap<String, Object>();
		try {
			int status = graduatePlanService.addGraduatePlan(map);
			if (status == 1) {
				mesageMap.put("message", "添加成功");
			} else {
				mesageMap.put("message", "添加失败");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mesageMap;
	}

	@RequestMapping(value = "/updateGraduatePlan")
	@ResponseBody
	public Map<String, Object> updateGraduatePlan(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String graduateProPlanID = request.getParameter("graduateProPlanID");
		String term = request.getParameter("term");
		String academicYear = request.getParameter("academicYear");
		String graduatePlanName = request.getParameter("graduatePlanName");
		String graduatePlanStore = request.getParameter("graduatePlanStore");
		String graduatePlanContent = request.getParameter("graduatePlanContent");
		// 需要与前台商量获取当前登录人员姓名
		String uploader = request.getParameter("uploader");

		String grade = request.getParameter("grade");
		//String upTime = request.getParameter("upTime");
		String remark = request.getParameter("remark");
		String majorID = request.getParameter("majorID");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("graduateProPlanID", graduateProPlanID);
		map.put("term", term);
		map.put("academicYear", academicYear);
		map.put("graduatePlanName", graduatePlanName);
		map.put("graduatePlanStore", graduatePlanStore);
		map.put("graduatePlanContent", graduatePlanContent);
		map.put("uploader", uploader);
		//map.put("upTime", upTime);
		map.put("majorID", majorID);
		map.put("remark", remark);
		map.put("grade", grade);
		Map<String, Object> mesageMap = new HashMap<String, Object>();
		mesageMap.put("message", "修改失败");
		map.put("graduateProPlanID", graduateProPlanID);
		try {
			int status = graduatePlanService.updateGraduatePlan(map);
			if (status == 1) {
				mesageMap.put("message", "修改成功");
			} else {
				mesageMap.put("message", "修改失败");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mesageMap;
	}

	@RequestMapping(value = "/uploadGraduatePlan")
	@ResponseBody
	public Map<String, Object> uploadGraduatePlan(@RequestParam("graduatePlanName") MultipartFile file,
			HttpServletRequest request) {
		String fileName = file.getOriginalFilename();
		String prefix=fileName.substring(fileName.lastIndexOf(".")+1);
		String saveFileName = UUID.randomUUID().toString().replaceAll("-", "") + "." + prefix;
		String path = "file/designFileCS/graduateProgram";
		String pathtrue = path + saveFileName;
		String realPath = request.getSession().getServletContext().getRealPath("/")+"/"+pathtrue;
		
		
		
		Map<String,Object> map = new HashMap<>();
		
		if(prefix.equals("docx") || prefix.equals("doc")){
			String saveFileName1 = null ;
			if((prefix.equals("docx"))){
				saveFileName1 = saveFileName.replace("docx", "pdf");
				fileName = fileName.replace("docx", "pdf");
			}else if(prefix.equals("doc")){
				saveFileName1 = saveFileName.replace("doc", "pdf");
				fileName = fileName.replace("doc", "pdf");
			}
			String pathtrue1 = path + saveFileName1;
			String realPath1 = request.getSession().getServletContext().getRealPath("/")+""+pathtrue1;	
			try {
				file.transferTo(new File(realPath));
				if(new WordToPDFUtil().word2pdf(realPath, realPath1)){
					System.out.println("转换成功");
				}else{
					System.out.println("转换失败");
				}
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			map.put("graduatePlanName", fileName);
			map.put("graduatePlanStore", saveFileName1);
			map.put("graduatePlanContent", pathtrue1);
		}else{
			try {
				file.transferTo(new File(realPath));
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			map.put("graduatePlanName", fileName);
			map.put("graduatePlanStore", saveFileName);
			map.put("graduatePlanContent", pathtrue);
		}
		return map;
	}

	@RequestMapping(value = "/getGraduatePlan")
	@ResponseBody
	public Map<String, Object> getGraduatePlan(@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "pageSize", required = false) Integer pageSize, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		// int pageSize = 10;
		int start = (page - 1) * pageSize;
		String majorID = request.getParameter("majorID");
		String term = request.getParameter("term");
		String academicYear = request.getParameter("academicYear");
		String grade = request.getParameter("grade");
		Map<String, Object> map = new HashMap<>();
		map.put("majorID", majorID);
		map.put("term", term);
		map.put("academicYear", academicYear);
		map.put("grade", grade);
		map.put("start", start);
		map.put("pageSize", pageSize);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> resultMap = new HashMap<>();
		try {
			list = graduatePlanService.getGraduatePlan(map);
			resultMap = graduatePlanService.getGraduatePlanNum(map);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		resultMap.put("Plan", list);
		return resultMap;
	}

	@RequestMapping(value = "/getMajorInPlan")
	@ResponseBody
	public List<Map<String, Object>> getMajorInPlan(HttpServletRequest request, HttpServletResponse response) {
		List<Map<String, Object>> list = new ArrayList<>();
		try {
			list = graduatePlanService.getMajorInPlan();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@RequestMapping(value = "/deleteGraduatePlan")
	@ResponseBody
	public Map<String, Object> deleteGraduatePlan(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String graduateProPlanID = request.getParameter("graduateProPlanID");
		Map<String, Object> resultMap = new HashMap<>();

		try {
			int status = graduatePlanService.deleteGraduatePlan(graduateProPlanID);
			if (status == 1) {
				resultMap.put("message", "删除成功");
			} else {
				resultMap.put("message", "删除失败");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultMap;
	}

	@RequestMapping(value = "/getPlanInfoWithID")
	@ResponseBody
	public Map<String, Object> getPlanInfoWithID(HttpServletRequest request, HttpServletResponse response) {
		String graduateProPlanID = request.getParameter("graduateProPlanID");
		Map<String, Object> map = new HashMap<>();
		try {
			map = graduatePlanService.getPlanInfoWithID(graduateProPlanID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	}

}

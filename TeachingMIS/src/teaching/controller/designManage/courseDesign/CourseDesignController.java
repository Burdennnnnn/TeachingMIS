package teaching.controller.designManage.courseDesign;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import teaching.entity.User;
import teaching.service.designManage.courseDesign.CourseDesignService;
import teaching.util.file.word2pdf.WordToPDFUtil;

/**
 * 张哲 创建时间：2017年4月9日 下午8:04:32
 */

@Controller
@RequestMapping(value = "courseDesign")
public class CourseDesignController {

	
	@Autowired
	private CourseDesignService courseDesignService;
	
	@RequestMapping(value = "/insertDesignInfo")
	@ResponseBody
	public Map<String, Object> insertDesignInfo(HttpServletResponse response, HttpServletRequest request) throws IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		Map<String,Object> map1 = new HashMap<String, Object>();
		Map<String,Object> map2 = new HashMap<String, Object>();
		Map<String,Object> messageMap = new HashMap<String, Object>(); 
		String courseDesignID = UUID.randomUUID().toString().replace("-", "");
		String majorID = request.getParameter("majorID");
		String trainingCourseID = request.getParameter("trainingCourseID");
		String courseDesignName = request.getParameter("courseDesignName");
		String startTime = request.getParameter("startTime");
		String endTime = request.getParameter("endTime");
		String teacherID = request.getParameter("teacherID");
		String classID = request.getParameter("classID");
		String remark1 = request.getParameter("remark1");
		//获取用户名称
		String upload = request.getParameter("user");
		String courseDesignInfoName = request.getParameter("courseDesignInfoName");
		String courseDesignInfoStore = request.getParameter("courseDesignInfoStore");
		String courseDesignInfo = request.getParameter("courseDesignInfo");
		String courseArrangeInfoName = request.getParameter("courseArrangeInfoName");
		String courseArrangeInfoStore = request.getParameter("courseArrangeInfoStore");
		String courseArrangeInfo = request.getParameter("courseArrangeInfo");
		System.out.println(courseArrangeInfoName);
		map1.put("courseDesignInfoName", courseDesignInfoName);
		map1.put("courseDesignInfoStore", courseDesignInfoStore);
		map1.put("courseDesignInfo", courseDesignInfo);
		map1.put("courseArrangeInfoName", courseArrangeInfoName);
		map1.put("courseArrangeInfoStore", courseArrangeInfoStore);
		map1.put("courseArrangeInfo", courseArrangeInfo);
		map1.put("courseDesignID", courseDesignID);
		map1.put("majorID", majorID);
		map1.put("trainingCourseID",trainingCourseID);
		map1.put("courseDesignName", courseDesignName);
		map1.put("startTime", startTime);
		map1.put("endTime", endTime);
		map1.put("upload", upload);
		map1.put("remark1", remark1);
		
		
		map2.put("courseDesignID", courseDesignID);
		map2.put("teacherID", teacherID);
		map2.put("classID", classID);
		
		
		try {
			int status = courseDesignService.insertDesignInfo(map1);
			status += courseDesignService.insertDesignTeacherClass(map2);
			if(status == 2){
				messageMap.put("message", "添加成功！");
			}else{
				messageMap.put("message", "添加失败！");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return messageMap;
	}

	@RequestMapping(value = "/getDesignBaseInfo")
	@ResponseBody
	public Map<String, Object> getDesignBaseInfo(@RequestParam(value="page",required=false) int  page,@RequestParam(value="pageSize",required=false) int  pageSize,
			HttpServletResponse response, HttpServletRequest request) {
		int start = (page-1)*pageSize;
		String majorID = request.getParameter("majorID");
		String term = request.getParameter("term");
		String grade = request.getParameter("grade");
		String academicYear = request.getParameter("academicYear");
		Map<String,Object> map = new HashMap<>();
		map.put("majorID", majorID);
		map.put("term", term);
		map.put("grade", grade);
		map.put("academicYear", academicYear);
		map.put("start", start);
		map.put("pageSize", pageSize);
		Map<String,Object> returnMap = new HashMap<>();
		List<Map<String, Object>> list = new ArrayList<>();
		try {
			list = courseDesignService.getDesignBaseInfo(map);
			for (Map<String, Object> map2 : list) {
				String[] teacherIDs = map2.get("teacherID").toString().split(",");
				String teacherName = "";
				for (int i = 0; i < teacherIDs.length; i++) {
					teacherName += courseDesignService.getTeacherByTeacherID(teacherIDs[i])+",";
				}
				teacherName = teacherName.substring(0,teacherName.length()-1);
				map2.put("teacherName", teacherName);
			}
			
			
			returnMap.put("num", courseDesignService.getDesignBaseInfoNum(map));
			returnMap.put("info", list);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println(list);
		return returnMap;
	}
	
	@RequestMapping(value = "/getCourseDesignT")
	@ResponseBody
	public Map<String, Object> getCourseDesignT(@RequestParam(value="page",required=false) int  page,@RequestParam(value="pageSize",required=false) int  pageSize,
			HttpServletResponse response, HttpServletRequest request,HttpSession session) {
		int start = (page-1)*pageSize;
		
		User user = (User) session.getAttribute("loginuser");
		
		String teacherID = user.getUserID();
		//String teacherID = request.getParameter("teacherID");
		
		//String teacherID = (String) session.getAttribute("userID");
		String majorID = request.getParameter("majorID");
		String term = request.getParameter("term");
		String grade = request.getParameter("grade");
		String academicYear = request.getParameter("academicYear");
		Map<String,Object> map = new HashMap<>();
		map.put("teacherID", teacherID);
		map.put("majorID", majorID);
		map.put("term", term);
		map.put("grade", grade);
		map.put("academicYear", academicYear);
		map.put("start", start);
		map.put("pageSize", pageSize);
		Map<String,Object> returnMap = new HashMap<>();
		List<Map<String, Object>> list = new ArrayList<>();
		try {
			list = courseDesignService.getCourseDesignT(map);
			for (Map<String, Object> map2 : list) {
				String[] teacherIDs = map2.get("teacherID").toString().split(",");
				String teacherName = "";
				for (int i = 0; i < teacherIDs.length; i++) {
					teacherName += courseDesignService.getTeacherByTeacherID(teacherIDs[i])+",";
				}
				teacherName = teacherName.substring(0,teacherName.length()-1);
				map2.put("teacherName", teacherName);
			}	
			returnMap.put("num", courseDesignService.getCourseDesignTN(map));
			returnMap.put("info", list);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println(list);
		return returnMap;
	}
	
	@RequestMapping(value = "/getMajorDesign")
	@ResponseBody
	public List<Map<String,Object>> getMajorDesign(HttpServletRequest request,HttpServletResponse response){
		List<Map<String, Object>> list = null;
		try {
			list=courseDesignService.getMajorDesign();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@RequestMapping(value = "/getCourseWithID")
	@ResponseBody
	public Map<String,Object> getCourseWithID(HttpServletRequest request,HttpServletResponse response){
		String trainingCourseID = request.getParameter("trainingCourseID");
		String classID = request.getParameter("classID");
		System.out.println(classID+"******************");
		Map<String, Object>map1 = new HashMap<>();
		map1.put("trainingCourseID", trainingCourseID);
		map1.put("classID", classID);
		
		Map<String, Object>map = new HashMap<>();
		try {
			map=courseDesignService.getCourseWithID(map1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	}
	
//	@RequestMapping(value = "/getCourseInfoDesign")
//	@ResponseBody
//	public List<Map<String,Object>> getCourseInfoDesign(HttpServletRequest request,HttpServletResponse response){
//		List<Map<String, Object>> list = null;
//		Map<String,Object> map = new HashMap<String, Object>();
//		String majorID = request.getParameter("majorID");
//		String term = request.getParameter("term");
//		String academicYear = request.getParameter("academicYear");
//		map.put(majorID, "majorID");
//		map.put(term, "term");
//		map.put(academicYear, "academicYear");
//		try {
//			list = courseDesignService.getCourseInfoDesign(map);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return list;
//	}

	@RequestMapping(value = "/getTeacherDesign")
	@ResponseBody
	public List<Map<String, Object>> getTeacherDesign(HttpServletResponse response, HttpServletRequest request) {
		List<Map<String, Object>> teacherList = new ArrayList<>();
		try {
			teacherList = courseDesignService.getTeacherDesign();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teacherList;
	}
	
	@RequestMapping(value = "/getAllCourseWithMajor")
	@ResponseBody
	public List<Map<String, Object>> getAllCourseWithMajor(HttpServletResponse response, HttpServletRequest request) {
		List<Map<String, Object>> majorList = new ArrayList<>();
		List<Map<String, Object>> returnList = new ArrayList<>();
		try {
			majorList = courseDesignService.getMajorDesign();
			for (Map<String, Object> map : majorList) {
				String majorID = (String) map.get("majorID");
				String majorName = (String) map.get("majorName");
				List<Map<String, Object>> courseList = new ArrayList<>();
				courseList = courseDesignService.getAllCourseWithMajor(majorID);
				Map<String, Object> map1 = new HashMap<>();
				map1.put("value", majorID);
				map1.put("label", majorName);
				map1.put("children", courseList);
				returnList.add(map1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;
	}

	@RequestMapping(value = "/getClassDesign")
	@ResponseBody
	public List<Map<String, Object>> getClassDesign(HttpServletResponse response, HttpServletRequest request) {
		String majorID = request.getParameter("majorID");
		List<Map<String, Object>> list = new ArrayList<>();
		try {
			list = courseDesignService.getClassDesign(majorID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@RequestMapping(value = "/deleteDesignOne")
	@ResponseBody
	public Map<String, Object> deleteDesignOne(HttpServletResponse response, HttpServletRequest request) {
		String courseDesignID = request.getParameter("courseDesignID");
		Map<String,Object> messageMap = new HashMap<String, Object>(); 
		try {
			int status = courseDesignService.deleteDesignOne(courseDesignID);
			if(status == 1){
				messageMap.put("message", "删除成功！");
			}else{
				messageMap.put("message", "删除失败！");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return messageMap;
	}

	@RequestMapping(value = "/deleteDesignBatch")
	@ResponseBody
	public Map<String, Object> deleteDesignBatch(HttpServletResponse response, HttpServletRequest request) {
		String IDs = request.getParameter("courseDesignIDs");
		List<String> lists= new ArrayList<String>();
		String[] strs = IDs.split(",");
		for (String string : strs) {
			lists.add(string);
		}
		Map<String,Object> messageMap = new HashMap<String, Object>(); 
		try {
			int status = courseDesignService.deleteDesignBatch(lists);
			if(status == lists.size()){
				messageMap.put("message", "删除成功！");
			}else{
				messageMap.put("message", "删除失败！");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return messageMap;
	}

	@RequestMapping(value = "/getOneDesignInfo")
	@ResponseBody
	public Map<String, Object> getOneDesignInfo(HttpServletResponse response, HttpServletRequest request){
		String courseDesignID = request.getParameter("courseDesignID");
		Map<String, Object> map = new HashMap<>();
		try {
			map = courseDesignService.getOneDesignInfo(courseDesignID);
			String[] teacherIDs = map.get("teacherID").toString().split(",");
			map.put("teacherID", teacherIDs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	}
	@RequestMapping(value = "/updateDesignInfo")
	@ResponseBody
	public Map<String,Object> updateDesignInfo(HttpServletResponse response, HttpServletRequest request) throws IllegalStateException, IOException{
		Map<String,Object> map1 = new HashMap<String, Object>();
		List<Map<String,Object>> list = new ArrayList<>();
		Map<String,Object> messageMap = new HashMap<String, Object>(); 
		String courseDesignID = request.getParameter("courseDesignID");
		String majorID = request.getParameter("majorID");
		String trainingCourseID = request.getParameter("trainingCourseID");
		String courseDesignName = request.getParameter("courseDesignName");
		String startTime = request.getParameter("startTime");
		String endTime = request.getParameter("endTime");
		String teacherID = request.getParameter("teacherID");
		String classID = request.getParameter("classID");
		String remark1 = request.getParameter("remark1");
		//获取用户名称
		String upload = request.getParameter("user");
		map1.put("courseDesignID", courseDesignID);
		map1.put("majorID", majorID);
		map1.put("trainingCourseID",trainingCourseID);
		map1.put("courseDesignName", courseDesignName);
		map1.put("startTime", startTime);
		map1.put("endTime", endTime);
		map1.put("remark1", remark1);
		map1.put("upload", upload);
		
		String courseDesignInfoName = request.getParameter("courseDesignInfoName");
		
		String courseDesignInfoStore = request.getParameter("courseDesignInfoStore");
		String courseDesignInfo = request.getParameter("courseDesignInfo");
		String courseArrangeInfoName = request.getParameter("courseArrangeInfoName");
		String courseArrangeInfoStore = request.getParameter("courseArrangeInfoStore");
		String courseArrangeInfo = request.getParameter("courseArrangeInfo");
		//System.out.println(courseArrangeInfoName);
		map1.put("courseDesignInfoName", courseDesignInfoName);
		map1.put("courseDesignInfoStore", courseDesignInfoStore);
		map1.put("courseDesignInfo", courseDesignInfo);
		map1.put("courseArrangeInfoName", courseArrangeInfoName);
		map1.put("courseArrangeInfoStore", courseArrangeInfoStore);
		map1.put("courseArrangeInfo", courseArrangeInfo);

		Map<String,Object> map = new HashMap<>();
		map.put("courseDesignID", courseDesignID);
		map.put("teacherID", teacherID);
		map.put("classID", classID);

		try {
			int status = courseDesignService.updateDesignInfo(map1);
			status += courseDesignService.updateDesignTeachetClass(map);
			if(status == 2){
				messageMap.put("message", "修改成功！");
			}else{
				messageMap.put("message", "修改失败！");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return messageMap;
	}
	
	@RequestMapping(value = "/uploadCourseDesignInfo")
	@ResponseBody
	public Map<String,Object>uploadCourseDesignInfo (@RequestParam("courseDesignInfoName") MultipartFile file,HttpServletRequest request,
			HttpServletResponse response) {
		response.setContentType("text/html;charset=utf-8");
		String fileName = file.getOriginalFilename();
		String prefix=fileName.substring(fileName.lastIndexOf(".")+1);
		String saveFileName = UUID.randomUUID().toString().replaceAll("-", "") + "." + prefix;
		String path = "file\\designFileCS\\courseDesign\\";
		String pathtrue = path + saveFileName;
		String realPath = request.getSession().getServletContext().getRealPath("/")+""+pathtrue;

		Map<String,Object> map = new HashMap<>();
		
		if(prefix.equals("docx") || prefix.equals("doc")){
			String saveFileName1 = null ;
			if((prefix.equals("docx"))){
				saveFileName1 = saveFileName.replace("docx", "pdf");
				fileName = fileName.replace("docx", "pdf");
			}else if(prefix.equals("doc")){
				saveFileName1 = saveFileName.replace("doc", "pdf");
				fileName = fileName.replace("doc", "pdf");
			}
			String pathtrue1 = path + saveFileName1;
			String realPath1 = request.getSession().getServletContext().getRealPath("/")+""+pathtrue1;	
			try {
				file.transferTo(new File(realPath));
				if(new WordToPDFUtil().word2pdf(realPath, realPath1)){
					System.out.println("转换成功");
				}else{
					System.out.println("转换失败");
				}
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			map.put("courseDesignInfoName", fileName);
			map.put("courseDesignInfoStore", saveFileName1);
			map.put("courseDesignInfo", pathtrue1);
		}else{
			try {
				file.transferTo(new File(realPath));
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			map.put("courseDesignInfoName", fileName);
			map.put("courseDesignInfoStore", saveFileName);
			map.put("courseDesignInfo", pathtrue);
		}
		return map;
	}
	
	@RequestMapping(value = "/uploadCourseArrangeInfo")
	@ResponseBody
	public Map<String,Object>uploadCourseArrangeInfo (@RequestParam("courseArrangeInfoName") MultipartFile file,HttpServletRequest request,
			HttpServletResponse response) {
		 response.setContentType("text/html;charset=utf-8");
		String fileName = file.getOriginalFilename();
		String saveFileName = UUID.randomUUID().toString().replaceAll("-", "") + "-" + fileName;
		String path = "file/designFileCS/courseDesign/";
		String pathtrue = path + saveFileName;
		String realPath = request.getSession().getServletContext().getRealPath("/")+"/"+pathtrue;
		
		String prefix=fileName.substring(fileName.lastIndexOf(".")+1);
		Map<String,Object> map = new HashMap<>();
		
		if(prefix.equals("docx") || prefix.equals("doc")){
			String saveFileName1 = null ;
			if((prefix.equals("docx"))){
				saveFileName1 = saveFileName.replace("docx", "pdf");
				fileName = fileName.replace("docx", "pdf");
			}else if(prefix.equals("doc")){
				saveFileName1 = saveFileName.replace("doc", "pdf");
				fileName = fileName.replace("doc", "pdf");
			}
			String pathtrue1 = path + saveFileName1;
			String realPath1 = request.getSession().getServletContext().getRealPath("/")+""+pathtrue1;	
			try {
				file.transferTo(new File(realPath));
				if(new WordToPDFUtil().word2pdf(realPath, realPath1)){
					System.out.println("转换成功");
				}else{
					System.out.println("转换失败");
				}
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			map.put("courseArrangeInfoName", fileName);
			map.put("courseArrangeInfoStore", saveFileName1);
			map.put("courseArrangeInfo", pathtrue1);
		}else{
			try {
				file.transferTo(new File(realPath));
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			map.put("courseArrangeInfoName", fileName);
			map.put("courseArrangeInfoStore", saveFileName);
			map.put("courseArrangeInfo", pathtrue);
		}
		return map;
	}
	
	@RequestMapping(value = "/getCourseDesign")
	@ResponseBody
	public Map<String,Object>getCourseDesign(@RequestParam(value="page",required=false) int  page,@RequestParam(value="pageSize",required=false) int  pageSize,
			HttpServletResponse response, HttpServletRequest request){
		int start = (page-1)*pageSize;
		String term = request.getParameter("term");
		String academicYear = request.getParameter("academicYear");
		String majorID = request.getParameter("majorID");
		String grade = request.getParameter("grade");
		Map<String ,Object>map= new HashMap<>();
		map.put("start", start);
		map.put("pageSize", pageSize);
		map.put("term", term);
		map.put("academicYear", academicYear);
		map.put("majorID", majorID);
		map.put("grade", grade);
		Map<String,Object> resultMap = new HashMap<>();
		try {
			List<Map<String,Object>> list = new ArrayList<>();
			resultMap.put("num", courseDesignService.getCourseDesignNum(map));
			list = courseDesignService.getCourseDesign(map);
			resultMap.put("course", list);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultMap;
	}
	
}

package teaching.controller.CourseManage;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.entity.Course;
import teaching.entity.User;
import teaching.service.CourseManage.CourseService;

@Controller
@RequestMapping("course")
public class CourseController {

	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CourseService courseService;
	
	@RequestMapping("getCourseForTeacher")
	@ResponseBody
	public Map<String, Object> getCoursesForTeacher(HttpServletRequest request,@RequestBody Map<String,String> condition ){
		Map<String,Object> result = new HashMap<String,Object>();
		String yearStr = condition.get("academicYear");
        String term = condition.get("term");
		if(!validateStr(yearStr)){
			Calendar now = Calendar.getInstance();
	        int year = now.get(Calendar.YEAR);
	        int month = now.get(Calendar.MONTH) + 1;
	        
	        if(month>=9 || month <=2){
	            yearStr = String.valueOf(year)+"-"+String.valueOf(year+1);
	            term = "01";
	        }else{
	            yearStr = String.valueOf(year-1)+"-"+String.valueOf(year);
	            term = "02";
	        }
		}
		
        condition.put("academicYear", yearStr);
        condition.put("term", term);
		/*String teacherID = (String) request.getSession().getAttribute("userID");*/
        String teacherID = "T00003";
		condition.put("teacherID", teacherID);
		List<Map<String, Object>> courseList = new ArrayList<Map<String, Object>>();
		try {
			courseList = courseService.getCoursesForTeacher(condition);
			result.put("courseList", courseList);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			result.put("error", e.getMessage());
			return result;
		}
		return result;
	}
	
	@RequestMapping("/getCourseById")
	public @ResponseBody Map<String,Object> getCourseById(@RequestBody Map<String,Object> ids){
		Map<String,Object> result = new HashMap<String,Object>();
		Course course;
		try {
			course = courseService.getCourseById((String) ids.get("trainingCourseID"));
			result.put("course", course);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			result.put("error", e.getMessage());
			return result;
		}
		return result;
	}
	
	@RequestMapping("getCoursesForStudent")
	public @ResponseBody Map<String, Object> getCoursesForStudent(@RequestBody Map<String,String> condition ,HttpSession session) {
		Map<String,Object> result = new HashMap<String,Object>();
		String yearStr = condition.get("academicYear");
        String term = condition.get("term");
		if(!validateStr(yearStr)){
			Calendar now = Calendar.getInstance();
	        int year = now.get(Calendar.YEAR);
	        int month = now.get(Calendar.MONTH) + 1;
	        
	        if(month>=9 || month <=2){
	            yearStr = String.valueOf(year)+"-"+String.valueOf(year+1);
	            term = "01";
	        }else{
	            yearStr = String.valueOf(year-1)+"-"+String.valueOf(year);
	            term = "02";
	        }
		}
		
        condition.put("academicYear", yearStr);
        condition.put("term", term);
		User user = (User) session.getAttribute("loginuser");
		String studentID = user.getUserID();
		String classID;
		try {
			classID = courseService.getClassID(studentID);
			condition.put("classID", classID);
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<Map<String, Object>> courseList = new ArrayList<Map<String, Object>>();
		courseList = courseService.getCoursesForStudent(condition);
		result.put("courseList", courseList);
		return result;
	}
	
	
	private boolean validateStr(String str){
		boolean result = false;
		if(str != null && !str.equals("") && !str.equals("null")){
			result = true;
		}
		return result;
	}
}

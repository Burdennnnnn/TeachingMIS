package teaching.controller.CourseManage;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import teaching.entity.FileIndex;
import teaching.entity.TeachingProgram;
import teaching.service.CourseManage.TeachingProgramService;
import teaching.service.file.FileService;
import teaching.util.file.word2pdf.WordToPDFUtil;

@Controller()
@RequestMapping("teachingProgram")
public class TeachingProgramController {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private TeachingProgramService teachingProgramService;
    @Autowired
    private FileService fileService;
    
    @InitBinder
    public void initBinder(WebDataBinder binder){
    	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    	dateFormat.setLenient(false);
    	binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }
    
    @RequestMapping(value="/uploadProgram")
    @ResponseBody
    public String uploadSchema(@RequestParam("file") MultipartFile file,HttpServletRequest request){
    	String fileID;
    	if(!file.isEmpty()){
    		logger.debug(file.getOriginalFilename());
    		String path = "file/teachingProgram/word";
    		String realPath = request.getSession().getServletContext().getRealPath("/"+path);
    		File targetFile = new File(realPath,System.currentTimeMillis()+file.getOriginalFilename());
    		logger.debug(targetFile.getAbsolutePath());
    		/*if(!targetFile.exists()){
    			targetFile.mkdirs();
    		}*/
    		try {
				FileUtils.copyInputStreamToFile(file.getInputStream(), targetFile);
				String source = targetFile.getAbsolutePath();
				String pdfPath = "file/teachingProgram/PDF";
				String pdfName = targetFile.getName().replace("docx", "pdf");
				File temp =new File(request.getSession().getServletContext().getRealPath("/"+pdfPath));    
				if(!temp.exists())      
				{       
				    temp.mkdir();    
				} 

				String target = request.getSession().getServletContext().getRealPath("/"+pdfPath) +"\\"+pdfName;
				logger.debug(source);
				logger.debug(target);
				if(new WordToPDFUtil().word2pdf(source, target)){
					logger.debug("转换成功");
				}else{
					logger.debug("转换失败");
				}
				FileIndex fileIndex = new FileIndex();
				fileIndex.setOriginalName(pdfName);
				fileIndex.setCurrentName(System.currentTimeMillis()+pdfName);
				fileIndex.setFileUrl(target);
				if(fileService.addFile(fileIndex)){
					fileID = fileIndex.getFileID();
					return fileID;
				}
				
			} catch (IOException e) {
				logger.debug(e.getMessage());
			}
    	}
    	return null;
    }
    
    @RequestMapping("/teachingProgramList")
    @ResponseBody
    public Map<String,Object> teachingProgramList(@RequestBody Map<String,Object> condition){
    	List<TeachingProgram> programList = new ArrayList<TeachingProgram>();
    	Map<String,Object> result = new HashMap<String,Object>();
    	try {
			programList = teachingProgramService.getProgramList(condition);
			result.put("programList", programList);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			result.put("error", e.getMessage());
			return result;
		}
    	return result;
    }
    
    @RequestMapping("/addProgram")
    @ResponseBody
    public Map<String,Object> addProgram(@RequestBody TeachingProgram teachingProgram){
    	Map<String,Object> result = new HashMap<String,Object>();
    	try {
    		
			if(teachingProgramService.addProgram(teachingProgram)){
				result.put("success", "添加课程教学大纲成功!");
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			result.put("error", e.getMessage());
			return result;
		}
    	return result;
    }
    
    @RequestMapping("/deleteProgram")
    @ResponseBody
    public Map<String,Object> deleteProgram(@RequestBody Map<String,Object> id){
    	Map<String,Object> result = new HashMap<String,Object>();
    	try {
			if(teachingProgramService.deleteProgram((String) id.get("teachingProgramID"))){
				result.put("success", "删除课程教学大纲成功!");
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			result.put("error", e.getMessage());
			return result;
		}
    	return result;
    }
}

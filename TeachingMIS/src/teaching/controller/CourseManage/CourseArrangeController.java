package teaching.controller.CourseManage;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.entity.CourseArrange;
import teaching.entity.TeacherCourse;
import teaching.service.CourseManage.CourseArrangeService;

@Controller
@RequestMapping("courseArrange")
public class CourseArrangeController {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CourseArrangeService courseArrangeService;
	
	@ResponseBody
	@RequestMapping("getCourseArrList")
	public Map<String,Object> courseArrList(@RequestBody Map<String,String> condition) {
		Map<String,Object> result = new HashMap<String,Object>();
		List<Map<String, Object>> arrangeList = new ArrayList<Map<String, Object>>();
		try {
			arrangeList = courseArrangeService.getCourseArrList(condition);
			result.put("arrangeList", arrangeList);
		} catch (SQLException e) {
			result.put("message", e.getMessage());
		}
		return result;
	}
	
	@ResponseBody
	@RequestMapping("getCourseList")
	public Map<String,Object> getCourseList(@RequestBody Map<String,String> condition) {
		Map<String,Object> result = new HashMap<String,Object>();
		
		String[] year = condition.get("academicYear").split("-");
		int yearNumber = 0;
		String academicYear = null;
		//根据专业获取班级
		try {
			Map<String,String> majorIDMap = new HashMap<String,String>();
			majorIDMap.put("majorID", condition.get("majorID"));
			List<Map<String,String>> classList = courseArrangeService.getClassYear(majorIDMap);
			List<Map<String,String>> courseList = new ArrayList<Map<String,String>>();
			//根据学期和班级的入学年份判断是那一学年的课程
			Map<String,String> courseCon = new HashMap<String,String>();
			for(Map<String,String> clazz:classList){
				int y = Integer.valueOf(year[1]);
				String e = String.valueOf(clazz.get("enrollmentTime"));
				int en = Integer.valueOf(e);
				yearNumber = y-en;
				if(yearNumber >0&& yearNumber<5){
					switch (yearNumber) {
					case 1:
						academicYear = "第一学年";
						break;
					case 2:
						academicYear = "第二学年";
						break;

					case 3:
						academicYear = "第三学年";
						break;
					case 4:
						academicYear = "第四学年";
						break;
					default:
						break;
					}
					courseCon.put("majorID", condition.get("majorID"));
					courseCon.put("classID", clazz.get("classID"));
					courseCon.put("academicYear", academicYear);
					courseCon.put("trainingSchemeID", clazz.get("trainingSchemeID"));
					List<Map<String,String>> courseWillList = courseArrangeService.getWillCourse(courseCon);
					courseList.addAll(courseWillList);
				}
			}
			result.put("arrangeList", courseList);
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
		//根据获取到的学年和专业培养方案id到培养课程里获取课程信息
		//获取往年的排课信息
		//4组装新的排课信息，并保存
		return result;
	}
	@ResponseBody
	@RequestMapping("addArrange")
	public Map<String,Object> addArrange(@RequestBody Map<String,String> condition) {
		Map<String,Object> result = new HashMap<String,Object>();
		
		/*
		 * 根据专业和学年查询以往的排课情况
		 */
		String[] year = condition.get("academicYear").split("-");
		String preYear = String.valueOf(Integer.valueOf(year[0])-1) + "-" +  String.valueOf(Integer.valueOf(year[1])-1);
		Map<String,String> preCon = new HashMap<String,String>();
		preCon.put("majorID", condition.get("majorID"));
		preCon.put("academicYear", preYear);
		List<Map<String,String>> preArrList = courseArrangeService.getPreArrange(preCon);
		/*
		 * 保存新的排课安排，并获取排课ID
		 */
		CourseArrange courseArrange = new CourseArrange();
		courseArrange.setAcademicYear(condition.get("academicYear"));
		courseArrange.setMajorID(condition.get("majorID"));
		try {
			courseArrangeService.addArrange(courseArrange);
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
	    String courseArrangeID = courseArrange.getCoursesArrangeID();
	    
		int yearNumber = 0;
		String academicYear = null;
		//根据专业获取班级
		try {
			Map<String,String> majorIDMap = new HashMap<String,String>();
			majorIDMap.put("majorID", condition.get("majorID"));
			List<Map<String,String>> classList = courseArrangeService.getClassYear(majorIDMap);
			//根据学期和班级的入学年份判断是那一学年的课程
			Map<String,String> courseCon = new HashMap<String,String>();
			for(Map<String,String> clazz:classList){
				int y = Integer.valueOf(year[1]);
				String e = String.valueOf(clazz.get("enrollmentTime"));
				int en = Integer.valueOf(e);
				yearNumber = y-en;
				if(yearNumber >0&& yearNumber<5){
					switch (yearNumber) {
					case 1:
						academicYear = "第一学年";
						break;
					case 2:
						academicYear = "第二学年";
						break;

					case 3:
						academicYear = "第三学年";
						break;
					case 4:
						academicYear = "第四学年";
						break;
					default:
						break;
					}
					courseCon.put("majorID", condition.get("majorID"));
					courseCon.put("classID", clazz.get("classID"));
					courseCon.put("academicYear", academicYear);
					courseCon.put("trainingSchemeID", clazz.get("trainingSchemeID"));
					List<Map<String,String>> courseWillList = courseArrangeService.getWillCourse(courseCon);
					TeacherCourse tc = new TeacherCourse();
					for(Map<String, String> course : courseWillList){
						tc.setCourseArrangeID(courseArrangeID);
						tc.setCourseID(course.get("courseID"));
						tc.setTrainingCourseID(course.get("trainingCourseID"));
						tc.setClassID(clazz.get("classID"));
						for(Map<String, String> arr:preArrList){
							if(course.get("courseID").equals(arr.get("courseID"))){
								tc.setIsUseMedia(arr.get("isUseMedia"));
								tc.setTeacherID(arr.get("teacherID"));
							}
						}
						courseArrangeService.addTeacherCourse(tc);
					}
				}
			}
			result.put("courseArrangeID", courseArrangeID);
		} catch (SQLException e) {
			logger.info(e.getMessage());
			result.put("reslut", false);
		}
		//根据获取到的学年和专业培养方案id到培养课程里获取课程信息
		//获取往年的排课信息
		//4组装新的排课信息，并保存
		return result;
	}

	@ResponseBody
	@RequestMapping("getArrangeList")
	public Map<String,Object> getArrangeList(@RequestBody Map<String,String> condition) {
		Map<String,Object> result = new HashMap<String,Object>();
		List<Map<String, Object>> arrangeList = new ArrayList<Map<String, Object>>();
		try {
			arrangeList = courseArrangeService.getCourseArrList(condition);
			result.put("arrangeList", arrangeList);
		} catch (SQLException e) {
			result.put("message", e.getMessage());
		}
		return result;
	}
	
	@ResponseBody
	@RequestMapping("getArrangeDetailList")
	public Map<String,Object> getArrangeDetailList(@RequestBody Map<String,String> condition) {
		Map<String,Object> result = new HashMap<String,Object>();
		List<Map<String, Object>> arrangeList = new ArrayList<Map<String, Object>>();
		try {
			arrangeList = courseArrangeService.getArrangeDetailList(condition);
			result.put("arrangeList", arrangeList);
		} catch (SQLException e) {
			result.put("message", e.getMessage());
		}
		return result;
	}
	
	@ResponseBody
	@RequestMapping("arrangeAgree")
	public Map<String,Object> arrangeAgree(@RequestBody Map<String,String> checkInfo) {
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			if(courseArrangeService.updateChenck(checkInfo)){
				result.put("message", "审核通过");
			}
		} catch (SQLException e) {
			result.put("message", e.getMessage());
		}
		return result;
	}
	
	@ResponseBody
	@RequestMapping("arrangeDisAgree")
	public Map<String,Object> arrangeDisAgree(@RequestBody Map<String,String> checkInfo) {
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			if(courseArrangeService.updateChenck(checkInfo)){
				courseArrangeService.addCheckOpinion(checkInfo);
				result.put("message", "审核不通过");
			}
		} catch (SQLException e) {
			result.put("message", e.getMessage());
		}
		return result;
	}
	
	@ResponseBody
	@RequestMapping("getCheckOpinion")
	public Map<String,Object> getCheckOpinion(@RequestBody Map<String,String> checkInfo) {
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			String checkOpinion = courseArrangeService.getCheckOpinion(checkInfo.get("coursesArrangeID"));
			result.put("checkOpinion", checkOpinion);
		} catch (SQLException e) {
			result.put("message", e.getMessage());
		}
		return result;
	}
	
	@ResponseBody
	@RequestMapping("getTeachers")
	public Map<String,Object> getTeachers(@RequestBody Map<String,String> condition) {
		Map<String,Object> result = new HashMap<String,Object>();
		List<Map<String, String>> teachers = new ArrayList<Map<String, String>>();
		try {
			teachers = courseArrangeService.getTeachers(condition);
			result.put("teachers", teachers);
		} catch (SQLException e) {
			result.put("message", e.getMessage());
		}
		return result;
	}
	
	@ResponseBody
	@RequestMapping("updateTeacher")
	public Map<String,Object> updateTeacher(@RequestBody Map<String,String> condition) {
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			courseArrangeService.updateTeacher(condition);
		} catch (SQLException e) {
			result.put("message", e.getMessage());
		}
		return result;
	}
}

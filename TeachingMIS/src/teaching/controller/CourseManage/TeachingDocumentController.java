package teaching.controller.CourseManage;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import teaching.entity.FileIndex;
import teaching.entity.TeachingDocument;
import teaching.service.CourseManage.TeachingDocumentService;
import teaching.service.file.FileService;
import teaching.util.file.word2pdf.WordToPDFUtil;

@Controller
@RequestMapping("teachingDocument")
public class TeachingDocumentController {

	Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private TeachingDocumentService teachingDocumentService;
    @Autowired
    private FileService fileService;
    
    @RequestMapping(value="/uploadDocument")
    @ResponseBody
    public String uploadDocument(@RequestParam("file") MultipartFile file,HttpServletRequest request){
    	String fileID;
    	if(!file.isEmpty()){
    		logger.debug(file.getOriginalFilename());
    		String path = "file/teachingDocument/word";
    		String realPath = request.getSession().getServletContext().getRealPath("/"+path);
    		File targetFile = new File(realPath,System.currentTimeMillis()+file.getOriginalFilename());
    		logger.debug(targetFile.getAbsolutePath());
    		/*if(!targetFile.exists()){
    			targetFile.mkdirs();
    		}*/
    		try {
				FileUtils.copyInputStreamToFile(file.getInputStream(), targetFile);
				String source = targetFile.getAbsolutePath();
				String pdfPath = "file/teachingDocument/PDF";
				String pdfName = targetFile.getName().replace("docx", "pdf");
				File temp =new File(request.getSession().getServletContext().getRealPath("/"+pdfPath));    
				if(!temp.exists())      
				{       
				    temp.mkdir();    
				} 

				String target = request.getSession().getServletContext().getRealPath("/"+pdfPath) +"\\"+pdfName;
				logger.debug(source);
				logger.debug(target);
				if(new WordToPDFUtil().word2pdf(source, target)){
					logger.debug("转换成功");
				}else{
					logger.debug("转换失败");
				}
				FileIndex fileIndex = new FileIndex();
				fileIndex.setOriginalName(pdfName);
				fileIndex.setCurrentName(System.currentTimeMillis()+pdfName);
				fileIndex.setFileUrl(target);
				if(fileService.addFile(fileIndex)){
					fileID = fileIndex.getFileID();
					return fileID;
				}
				
			} catch (IOException e) {
				logger.debug(e.getMessage());
			}
    	}
    	return null;
    }
    
    @RequestMapping("/documentList")
    @ResponseBody
    public Map<String,Object> documentList(@RequestBody Map<String,String> condition){
    	List<TeachingDocument> documentList = new ArrayList<TeachingDocument>();
    	Map<String,Object> result = new HashMap<String,Object>();
    	try {
    		documentList = teachingDocumentService.documentList(condition);
			result.put("documentList", documentList);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			result.put("error", e.getMessage());
			return result;
		}
    	return result;
    }
    
    @RequestMapping("/addDocument")
    @ResponseBody
    public Map<String,Object> addDocument(@RequestBody TeachingDocument teachingDocument){
    	Map<String,Object> result = new HashMap<String,Object>();
    	try {
    		
			if(teachingDocumentService.addDocument(teachingDocument)>0){
				result.put("success", "添加教学资料成功!");
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			result.put("error", e.getMessage());
			return result;
		}
    	return result;
    }
    
    @RequestMapping("/deleteDocument")
    @ResponseBody
    public Map<String,Object> deleteDocument(@RequestBody Map<String,Object> id){
    	Map<String,Object> result = new HashMap<String,Object>();
    	try {
			if(teachingDocumentService.deleteDocument((String) id.get("documentID"))>0){
				result.put("success", "删除教学资料成功!");
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			result.put("error", e.getMessage());
			return result;
		}
    	return result;
    }
}

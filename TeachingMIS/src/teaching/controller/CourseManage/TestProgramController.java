package teaching.controller.CourseManage;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import teaching.entity.FileIndex;
import teaching.entity.TestProgram;
import teaching.service.CourseManage.TestProgramService;
import teaching.service.file.FileService;
import teaching.util.file.word2pdf.WordToPDFUtil;

@Controller
@RequestMapping("testProgram")
public class TestProgramController {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private TestProgramService testProgramService;
    @Autowired
    private FileService fileService;
    
    @InitBinder
    public void initBinder(WebDataBinder binder){
    	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    	dateFormat.setLenient(false);
    	binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }
    
    @RequestMapping(value="/uploadTestProgram")
    @ResponseBody
    public String uploadTestProgram(@RequestParam("file") MultipartFile file,HttpServletRequest request){
    
    	String fileID;
    	if(!file.isEmpty()){
    		logger.debug(file.getOriginalFilename());
    		String path = "file/testProgram/word";
    		String realPath = request.getSession().getServletContext().getRealPath("/"+path);
    		File targetFile = new File(realPath,System.currentTimeMillis()+file.getOriginalFilename());
    		logger.debug(targetFile.getAbsolutePath());
    		/*if(!targetFile.exists()){
    			targetFile.mkdirs();
    		}*/
    		try {
				FileUtils.copyInputStreamToFile(file.getInputStream(), targetFile);
				String source = targetFile.getAbsolutePath();
				String pdfPath = "file/testProgram/PDF";
				String pdfName = targetFile.getName().replace("docx", "pdf");
				File temp =new File(request.getSession().getServletContext().getRealPath("/"+pdfPath));    
				if(!temp.exists())      
				{       
				    temp.mkdir();    
				} 

				String target = request.getSession().getServletContext().getRealPath("/"+pdfPath) +"\\"+pdfName;
				logger.debug(source);
				logger.debug(target);
				if(new WordToPDFUtil().word2pdf(source, target)){
					logger.debug("转换成功");
				}else{
					logger.debug("转换失败");
				}
				FileIndex fileIndex = new FileIndex();
				fileIndex.setOriginalName(pdfName);
				fileIndex.setCurrentName(System.currentTimeMillis()+pdfName);
				fileIndex.setFileUrl(target);
				if(fileService.addFile(fileIndex)){
					fileID = fileIndex.getFileID();
					return fileID;
				}
				
			} catch (IOException e) {
				logger.debug(e.getMessage());
			}
    	}
    	return null;
    }
    
    @RequestMapping("/testProgramList")
    @ResponseBody
    public Map<String,Object> testProgramList(@RequestBody Map<String,Object> condition){
    	List<TestProgram> programList = new ArrayList<TestProgram>();
    	Map<String,Object> result = new HashMap<String,Object>();
    	try {
    		programList = testProgramService.getTestProgramList(condition);
			result.put("scheduleList", programList);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			result.put("error", e.getMessage());
			return result;
		}
    	return result;
    }
    
    @RequestMapping("/addTestProgram")
    @ResponseBody
    public Map<String,Object> addTestProgram(@RequestBody TestProgram testProgram){
    	Map<String,Object> result = new HashMap<String,Object>();
    	try {
    		
			if(testProgramService.addTestProgram(testProgram)){
				result.put("success", "添加课程进度表成功!");
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			result.put("error", e.getMessage());
			return result;
		}
    	return result;
    }
    
    @RequestMapping("/deleteTestProgram")
    @ResponseBody
    public Map<String,Object> deleteTestProgram(@RequestBody Map<String,Object> id){
    	Map<String,Object> result = new HashMap<String,Object>();
    	try {
			if(testProgramService.deleteTestProgram((String) id.get("testProgramID"))){
				result.put("success", "删除课程进度表成功!");
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			result.put("error", e.getMessage());
			return result;
		}
    	return result;
    }
}

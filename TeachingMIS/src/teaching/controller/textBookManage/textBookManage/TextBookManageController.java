package teaching.controller.textBookManage.textBookManage;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.service.textBookManage.textBookManage.TextBookManageService;

@Controller
@RequestMapping(value="textBookManage")
public class TextBookManageController {
	
	@Autowired
	private TextBookManageService textBookManageService;
	
	
	/**
	 * 查询教材信息map(page,pageSize,textbookId,courseNameCN,textbookName)
	 * @author GuoFei
	 * @param page
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="getTextBook")
	 @ResponseBody
	public Map<String,Object> getTextBook(@RequestParam(value="page",required=false) Integer  page,@RequestParam(value="pageSize",required=false) Integer  pageSize,HttpServletRequest request,HttpServletResponse response){
		
		List<Map<String,Object>> list = null;
		Map<String,Object> map = new HashMap<String, Object>();
		Map<String,Object> map2 = new HashMap<String, Object>();
		 int start = 0; 
		 if(page!=null&&pageSize!=null){
			 start = (page-1)*pageSize;
		 }
		map.put("start",start);
		map.put("pageSize",pageSize);
		map.put("textbookId",request.getParameter("textbookId"));
		map.put("courseNameCN",request.getParameter("courseNameCN"));
		map.put("textbookName",request.getParameter("textbookName"));
		map.put("courseId",request.getParameter("courseId"));
		try {
			list = textBookManageService.getTextBook2(map);
			int countNum = textBookManageService.getTextBookCountNum(map);
			map2.put("textBookList", list);
			map2.put("countNum", countNum);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return map2;
	}
	
	
	/**
	 * 增加教材信息map(textbookId,textbookName,publishingHouse,author,isbn,price,teacherCourseId,remark1)
	 * @author GuoFei
	 * @param request
	 * @param response
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping(value="addTextBook")
	@ResponseBody
	public Map<String,Object> addTextBook(HttpServletRequest request,HttpServletResponse response) throws ParseException{
		
		Map<String,Object> map = new HashMap<String, Object>();
		Map<String,Object> resultMap = new HashMap<String, Object>();
		map.put("textbookId",request.getParameter("textbookId"));
		map.put("textbookName",request.getParameter("textbookName"));
		map.put("publishingHouse",request.getParameter("publishingHouse"));
		map.put("author",request.getParameter("author"));
		map.put("isbn",request.getParameter("isbn"));
		map.put("price",request.getParameter("price"));
		map.put("teacherCourseId",request.getParameter("teacherCourseId"));
		map.put("remark1",request.getParameter("remark1"));
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");		
		 map.put("createTime", sdf.parse(sdf.format(new Date())));
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		try {
			list.add(map);
			textBookManageService.addTextBook2(list);
		    resultMap.put("message", "添加教材信息成功");
					
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			resultMap.put("message", "添加教材信息失败");
			e.printStackTrace();
		}
		return resultMap;
	
	}
	
	/**
	 * 增加教材信息界面的课程信息的初始化map(courseNameCN,teacherCourseID)
	 * @author GuoFei
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="getNameAndId")
	 @ResponseBody
	public Map<String,Object> getNameAndId(HttpServletRequest request,HttpServletResponse response){
		List<Map<String,Object>> list = null ;
		Map<String,Object> map = new HashMap<String, Object>();
		try {
			list = textBookManageService.getNameAndId2();
			map.put("courseToteacher", list);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 修改教材信息map(oldTextbookId,textbookId,textbookName,publishingHouse,author,isbn,price,teacherCourseId,remark1)
	 * @author GuoFei
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="updateTextBook")
	 @ResponseBody
	public Map<String,Object> updateTextBook(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> resultMap = new HashMap<String, Object>();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("oldTextbookId",request.getParameter("oldTextbookId"));
		map.put("textbookId",request.getParameter("textbookId"));
		map.put("textbookName",request.getParameter("textbookName"));
		map.put("publishingHouse",request.getParameter("publishingHouse"));
		map.put("author",request.getParameter("author"));
		map.put("isbn",request.getParameter("isbn"));
		map.put("price",request.getParameter("price"));
		map.put("teacherCourseId",request.getParameter("teacherCourseId"));
		map.put("remark1",request.getParameter("remark1"));
		try {
			int status = textBookManageService.updateTextBook2(map);
			if(status == 1){
				resultMap.put("message", "修改教材信息成功");
			}else{
				resultMap.put("message", "修改教材信息失败");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return resultMap;
	}
	
	/**
	 *  删除教材信息
	 * @author GuoFei
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="deleteTextBook")
	 @ResponseBody
	public Map<String,Object> deleteTextBook(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> map =  new HashMap<String, Object>();
		String textbookId = request.getParameter("textbookId");
		try {
			int status = textBookManageService.deleteTextBook2(textbookId);
			if(status == 1){
				map.put("message", "删除教材信息成功");
			}else{
				map.put("message", "删除教材信息失败");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 查询教材名称及其id
	 * @return
	 */
	@RequestMapping(value="getTextBookNameAndId")
    @ResponseBody
	public List<Map<String,Object>> getTextBookNameAndId(){
		 List<Map<String,Object>> list = null;
		 try {
			list = textBookManageService.getTextBookNameAndId();
		 } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
		return list;
	}
}

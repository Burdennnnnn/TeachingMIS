package teaching.controller.trainingCourse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.entity.Course;
import teaching.service.trainingCourse.TrainingCourseService;

@Controller
@RequestMapping(value="trainingCourse")
public class TrainingCourseController {
    
    Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private TrainingCourseService trainingCourseService;
    
    /**
     * 
     * @param condition  培训方案ID 学年 学期 分页
     * @return
     */
    @RequestMapping("/getCourseList")
    public @ResponseBody List<Course> getTrainCourseList(@RequestBody Map<String,String> condition ){
    	
    	List<Course> courseList = new ArrayList<Course>();
    	try {
    		courseList = trainingCourseService.getTrainCourseList(condition);
    		return courseList;
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
    	return null;
    }
    
    @RequestMapping("/getCourseByID")
    public @ResponseBody Course getTrainCourseByID(@RequestBody Map<String,Object> request ){
    		String trainingCourseID = (String) request.get("trainingCourseID");
    		Course course = null;
    	try {
    		if(trainingCourseID!=null && !trainingCourseID.equals("")){
    			course = trainingCourseService.getTrainCourseByID(trainingCourseID);
    		}else{
    			course = new Course();
    		}
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
    	return course;
    }
    @RequestMapping("/saveCourse")
    public @ResponseBody boolean saveCourse(@RequestBody Course course ){
    	boolean result;
    	try {
    		if(course.getTrainingCourseID()!=null && !course.getTrainingCourseID().equals("")){
    			result = trainingCourseService.updateCourse(course);
    			return result;
    		}else{
    			result = trainingCourseService.addCourse(course);
    			return result;
    		}
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
    	return false;
    }
    
    @RequestMapping("/deleteCourse")
    public @ResponseBody boolean deleteCourse(@RequestBody Map<String,Object> request  ){
    	boolean result;
    	String trainingCourseID = (String) request.get("trainingCourseID");
    	try {
			result = trainingCourseService.deleteCourse(trainingCourseID);
			return result;
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
    	return false;
    }
   
}

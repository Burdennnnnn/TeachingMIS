package teaching.controller.personInfo;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.entity.User;
import teaching.service.baseInfoManage.studentBaseInfo.StudentBaseInfoService;
import teaching.service.baseInfoManage.teacherBaseInfo.TeacherBaseInfoService;
import teaching.service.personInfoService.PersonInfoService;

@Controller
@RequestMapping("personInfo")
public class PersonInfoController {

	@Autowired
	private PersonInfoService personInfoService;
	@Autowired
	private StudentBaseInfoService studentBaseInfoService;
	@Autowired
	private TeacherBaseInfoService teacherBaseInfoService;
	
	/**
	 * 得到个人信息
	 * @author GuoFei
	 * @param userId
	 * @param request
	 * @return
	 */
	@RequestMapping("getStudentInfo")
	@ResponseBody
	public Map<String,Object> getStudentInfo(HttpServletRequest request,HttpSession session){
		User user = (User) session.getAttribute("loginuser");
		String studentID = user.getUserID();
		List<Map<String, Object>> list = null;
		Map<String,Object> map = new HashMap<String, Object>();
		Map<String,Object> map2 = new HashMap<String, Object>();
		map.put("studentId", studentID);
		try {
			list = studentBaseInfoService.getStudent(map);
			int countNum = studentBaseInfoService.getStudentCountNum(map);
			map2.put("stuList", list);
			map2.put("countNum", countNum);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map2;
	}
	
	@RequestMapping("getTeacherInfo")
	@ResponseBody
	public Map<String,Object> getTeacherInfo(HttpServletRequest request,HttpSession session){
		User user = (User) session.getAttribute("loginuser");
		String teacherID = user.getUserID();
		List<Map<String, Object>> list = null;
		Map<String,Object> map = new HashMap<String, Object>();
		Map<String,Object> map2= new HashMap<String, Object>();
		map.put("teacherId", teacherID);
		try {
			list = teacherBaseInfoService.getTeacher(map);
			int countNum = teacherBaseInfoService.getTeacherCountNum(map);
			map2.put("teacherList", list);
			map2.put("countNum", countNum);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map2;
	}
	
	/**
	 * 修改密码
	 * @author GuoFei
	 * @param request
	 * @return
	 */
	@RequestMapping("updatePersonInfo")
	@ResponseBody
	public Map<String,Object> updatePersonInfo(HttpServletRequest request){
		Map<String,Object> map = new HashMap<String, Object>();
		Map<String,Object> map2 = new HashMap<String, Object>();
		String userId = request.getParameter("userId");
		String password = request.getParameter("password");
	    map2.put("userId", userId);
	    map2.put("password", password);
	    try {
			int status = personInfoService.updatePersonInfo(map2);
			if(status == 1){
				map.put("message", "修改成功");
			}else{
				map.put("message", "修改失败");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}
	
}

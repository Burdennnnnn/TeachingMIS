package teaching.controller.trainingSchema;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import teaching.entity.FileIndex;
import teaching.entity.TrainingSchema;
import teaching.service.file.FileService;
import teaching.service.trainingSchema.TrainingSchameService;
import teaching.util.file.word2pdf.WordToPDFUtil;

@Controller
@RequestMapping(value="trainingSchema")
public class TrainingSchemaController {
    
    Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private TrainingSchameService trainingShemaService;
    
    @Autowired
    private FileService fileService;
    
    @InitBinder
    public void initBinder(WebDataBinder binder){
    	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    	dateFormat.setLenient(false);
    	binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }
    @RequestMapping(value="/uploadSchema")
    @ResponseBody
    public String uploadSchema(@RequestParam("file") MultipartFile file,HttpServletRequest request){
    	String fileID;
    	if(!file.isEmpty()){
    		logger.debug(file.getOriginalFilename());
    		String path = "file/trainSchema/word";
    		String realPath = request.getSession().getServletContext().getRealPath("/"+path);
    		File targetFile = new File(realPath,System.currentTimeMillis()+file.getOriginalFilename());
    		logger.debug(targetFile.getAbsolutePath());
    		/*if(!targetFile.exists()){
    			targetFile.mkdirs();
    		}*/
    		try {
				FileUtils.copyInputStreamToFile(file.getInputStream(), targetFile);
				String source = targetFile.getAbsolutePath();
				String pdfPath = "file/trainSchema/PDF";
				String pdfName = targetFile.getName().replace("docx", "pdf");
				File temp =new File(request.getSession().getServletContext().getRealPath("/"+pdfPath));    
				if(!temp.exists())      
				{       
				    temp.mkdir();    
				} 

				String target = request.getSession().getServletContext().getRealPath("/"+pdfPath) +"\\"+pdfName;
				logger.debug(source);
				logger.debug(target);
				if(new WordToPDFUtil().word2pdf(source, target)){
					logger.debug("转换成功");
				}else{
					logger.debug("转换失败");
				}
				FileIndex fileIndex = new FileIndex();
				fileIndex.setOriginalName(pdfName);
				fileIndex.setCurrentName(System.currentTimeMillis()+pdfName);
				fileIndex.setFileUrl(target);
				if(fileService.addFile(fileIndex)){
					fileID = fileIndex.getFileID();
					return fileID;
				}
				
			} catch (IOException e) {
				logger.debug(e.getMessage());
			}
    	}
    	return null;
    }
   
    @RequestMapping(value="/addSchema")
    @ResponseBody
    public boolean addSchema(@RequestBody TrainingSchema trainingSchema){
    	boolean result = false;
    	try {
    		trainingSchema.setCreateTime(trainingSchema.getCreateTime().substring(0, 10));
			if(trainingShemaService.addSchema(trainingSchema)){
				result = true;
			}
		} catch (SQLException e) {
			logger.debug(e.getMessage());
		}
    	return result;
    }
    
    @RequestMapping(value="/updateSchema")
    @ResponseBody
    public boolean updateSchema(@RequestBody TrainingSchema trainingSchema){
    	boolean result = false;
    	try {
    		if(trainingShemaService.updateSchema(trainingSchema)){
				result = true;
			}
		} catch (SQLException e) {
			logger.debug(e.getMessage());
		}
    	return result;
    }
    
    @RequestMapping(value="/deleteSchema")
    @ResponseBody
    public boolean deleteSchema(@RequestBody Map<String,Object> request){
    	boolean result = false;
    	String trainingSchemeID = (String) request.get("trainingSchemeID");
    	try {
    		if(trainingShemaService.deleteSchema(trainingSchemeID)){
				result = true;
			}
		} catch (SQLException e) {
			logger.debug(e.getMessage());
		}
    	return result;
    }
    
    @RequestMapping(value="/getSchemaList")
    @ResponseBody
    public Map<String,Object> getSchemaList(@RequestBody Map<String,Object> condition){
    	Map<String,Object> result = new HashMap<String,Object>();
    	int page = (int) condition.get("currentPage");
    	int pageSize = (int) condition.get("pageSize");
    	int start = 0;
    	if(page >-1){
    		start = (page - 1)*pageSize;
    	}
    	condition.put("start",start);
    	condition.put("start",start);
    	List<TrainingSchema> schemaList = new ArrayList<TrainingSchema>();
    	int total = 0;
    	try {
			schemaList = trainingShemaService.getSchemaList(condition);
			total = trainingShemaService.getSchemaCount(condition);
			result.put("schemaList", schemaList);
			result.put("total", total);
		} catch (SQLException e) {
			logger.debug(e.getMessage());
		}
    	return result;
    }
    
    @RequestMapping(value="/getSchemaByID")
    @ResponseBody
    public TrainingSchema getSchemaByID(@RequestBody Map<String,Object> request){
    	TrainingSchema trainingSchema = new TrainingSchema();
    	String trainingSchemeID = (String) request.get("trainingSchemeID");
    	try {
			trainingSchema = trainingShemaService.getSchemaByID(trainingSchemeID);
		} catch (SQLException e) {
			logger.debug(e.getMessage());
		}
    	return trainingSchema;
    }
    
    
}

package teaching.controller.weixin;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.service.wexin.NewsService;

@Controller
@RequestMapping(value="weixin/news")
public class NewsController {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private NewsService newsService;
	
    @RequestMapping(value="newsList")
    public @ResponseBody List<Map<String,String>> getNewsList(){
    	List<Map<String,String>> newsList = new ArrayList<Map<String,String>>();
        try {
			newsList = newsService.getNewsList();
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
        return newsList;
    }
    
    @RequestMapping(value="addNews")
    public @ResponseBody Map<String,Object> addNews(HttpServletRequest request){
        Map<String,Object> result = new HashMap<String,Object>();
        Map<String,String> news = new HashMap<String,String>();
        String newTitle = request.getParameter("newTitle");
        String newContent = request.getParameter("newContent");
        String userId = request.getParameter("userId");
        news.put("newTitle", newTitle);
        news.put("newContent", newContent);
        news.put("userId", userId);
        try {
			if(newsService.addNews(news)){
				result.put("result", true);
	        }else{
	            result.put("result", false);
	        }
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
        return result;
    }
    
    @RequestMapping(value="getNews")
    public @ResponseBody Map<String,String> getNews(HttpServletRequest request){
    	Map<String,String> news = new HashMap<String,String>();
        String newId = request.getParameter("newId");
        Map<String,String> newIdMap = new HashMap<String,String>();
        newIdMap.put("newId", newId);
        try {
			news = newsService.getNewsById(newIdMap);
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
        return news;
    }
    
    @RequestMapping(value="deleteNews")
    public @ResponseBody Map<String,Object> deleteNews(HttpServletRequest request){
        Map<String,Object> result = new HashMap<String,Object>();
        String newId = request.getParameter("newId");
        Map<String,String> newIdMap = new HashMap<String,String>();
        newIdMap.put("newId", newId);
        try {
			if(newsService.deleteNews(newIdMap)){
			    result.put("result", true);
			}else{
			    result.put("result", false);
			}
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
        return result;
    }
    
    
    @RequestMapping(value="reviewList")
    public @ResponseBody List<Map<String,String>> getReviewList(HttpServletRequest request){
        String newId = request.getParameter("newId");
        List<Map<String,String>> reviewList = new ArrayList<Map<String,String>>();
        Map<String,String> newIdMap = new HashMap<String,String>();
        newIdMap.put("newId", newId);
        if(newId != null && !newId.equals("")){
        	try {
				reviewList = newsService.getReviewByNewsId(newIdMap);
			} catch (SQLException e) {
				logger.info(e.getMessage());
			}
        }
        return reviewList;
    }
    
    @RequestMapping(value="addReview")
    public @ResponseBody Map<String,Object> addReview(HttpServletRequest request){
        Map<String,Object> result = new HashMap<String,Object>();
        String userId = request.getParameter("userId");
        String reviewContent = request.getParameter("reviewContent");
        String newId = request.getParameter("newId");
        Map<String,String> review = new HashMap<String,String>();
        review.put("newId", newId);
        review.put("reviewContent", reviewContent);
        review.put("userId", userId);
        try {
			if(newsService.addReview(review)){
			    result.put("result", true);
			}else{
			    result.put("result", false);
			}
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
        return result;
    }
}

package teaching.controller.weixin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller

public class BookOrderController {
	@RequestMapping(value="bookorder")
	 @ResponseBody
	public List<Map<String,Object>>bookOrder(HttpServletRequest request){
		String userId = request.getParameter("userid");
		String orderYear = request.getParameter("orderyear");
		String orderTerm = request.getParameter("orderterm");
		Map<String,Object> map1 = new HashMap<String, Object>();
		map1.put("ocoursename", "数据库");
		map1.put("ocheckflag", 0);
		map1.put("bookname", "数据库书");
		map1.put("publisher", "出版社a");
		map1.put("author", "a");
		map1.put("isbn", "12313");
		map1.put("price", "20");
		map1.put("orderid", "01");
		Map<String,Object> map2 = new HashMap<String, Object>();
		map1.put("ocoursename", "Java");
		map1.put("ocheckflag", 1);
		map1.put("bookname", "java书");
		map1.put("publisher", "出版社b");
		map1.put("author", "b");
		map1.put("isbn", "146464");
		map1.put("price", "23");
		map1.put("orderid", "02");
		Map<String,Object> map3 = new HashMap<String, Object>();
		map1.put("ocoursename", "计算机网络");
		map1.put("ocheckflag", 0);
		map1.put("bookname", "计算机网络书");
		map1.put("publisher", "出版社c");
		map1.put("author", "c");
		map1.put("isbn", "6646");
		map1.put("price", "15");
		map1.put("orderid", "03");
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		list.add(map1);
		list.add(map2);
		list.add(map3);
		return list;
	}
	
}

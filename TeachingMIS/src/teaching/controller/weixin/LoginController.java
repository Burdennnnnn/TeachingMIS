package teaching.controller.weixin;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.service.wexin.CheckService;

@Controller
@RequestMapping(value="weixin/login")
public class LoginController {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CheckService checkService;
	
	
	@RequestMapping(value="login")
	@ResponseBody
	public Map<String,Object> Login(HttpServletRequest request){
		String userId = request.getParameter("userId");
		String userPs = request.getParameter("userPs");
		Map<String,Object> map =  new HashMap<String, Object>();
		if(userId.equals("admin") && userPs.equals("admin")){
			map.put("flag", 0);
			map.put("result", true);
			map.put("name", "管理者");
			map.put("picture", "D:\\");  
		}else if(userId.equals("admin") && userPs.equals("admin")){
			map.put("flag", 1);
			map.put("result", true);
			map.put("name", "aa");
			map.put("picture", "D:\\");  
		}else{
			map.put("result", false);
		}
		return map;
	}
	
	@RequestMapping(value="checkIn")
    @ResponseBody
    public Map<String,String> checkIn(HttpServletRequest request){
       String userId = request.getParameter("userId");
       String userPs = request.getParameter("userPs");
       String flag = "1";
       Map<String,String> loginInfo =  new HashMap<String, String>();
       loginInfo.put("userId",userId );
       loginInfo.put("userPs",userPs );
       Map<String,String> userInfo =  new HashMap<String, String>();
       try {
		String userID = checkService.wxlogin(loginInfo);
		if(userID != null && !userID.equals("")&& !userID.equals("null")){
			Map<String,String> userIdMap =  new HashMap<String, String>();
			userIdMap.put("userId", userID);
			List<Map<String,String>> roles = checkService.wxGetRoles(userIdMap);
			for(Map<String,String> role:roles){
				if(role.get("roleName").equals("教研室主任")){
					flag = "0";
				}
			}
			userInfo = checkService.wxGetUinfos(userIdMap);
			userInfo.put("flag", flag);
		}
	} catch (SQLException e) {
		logger.info(e.getMessage());
	}
       return userInfo;
   }
	
	@RequestMapping(value="updateInfo")
    public @ResponseBody Map<String,Object> updateInfo(HttpServletRequest request){
        Map<String,Object> result = new HashMap<String,Object>();
        String userId = request.getParameter("userId");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        System.out.println(userId+"  * "+phone+" * " + email);
        Map<String,String> userInfo =  new HashMap<String, String>();
        userInfo.put("userId", userId);
        userInfo.put("phone", phone);
        userInfo.put("email", email);
        try {
			if(checkService.wxUpdateUserInfo(userInfo)>0){
				result.put("result", true);
	        }else{
	            result.put("result", false);
	        }
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
        return result;
    }
	
	@RequestMapping(value="updatePassword")
    public @ResponseBody Map<String,Object> updatePassword(HttpServletRequest request){
        Map<String,Object> result = new HashMap<String,Object>();
        String userId = request.getParameter("userId");
        String userPs = request.getParameter("userPs");
        Map<String,String> userInfo =  new HashMap<String, String>();
        userInfo.put("userId", userId);
        userInfo.put("userPs", userPs);
        try {
			if(checkService.wxUpdatePassword(userInfo)>0){
				result.put("result", true);
	        }else{
	            result.put("result", false);
	        }
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
        return result;
    }
}

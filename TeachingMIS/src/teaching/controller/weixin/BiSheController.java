package teaching.controller.weixin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller

public class BiSheController {
	@RequestMapping(value="bishe")
    @ResponseBody
	public List<Map<String,Object>>biShe(HttpServletRequest request){
		String userId = request.getParameter("userid");
		String biSheYear = request.getParameter("bisheyear");
		String biSheDepar = request.getParameter("bishedepar");
		Map<String,Object> map1 = new HashMap<String, Object>();
		map1.put("bsname", "软件工程毕设");
		map1.put("bsteacher", "z");
		map1.put("bsdepar", "软件工程");
		map1.put("bscontents", "这是软件工程毕设");
		map1.put("bsid", "01");
		Map<String,Object> map2 = new HashMap<String, Object>();
		map2.put("bsname", "网络工程毕设");
		map2.put("bsteacher", "x");
		map2.put("bsdepar", "网络工程");
		map2.put("bscontents", "这是网络工程毕设");
		map2.put("bsid", "02");
		Map<String,Object> map3 = new HashMap<String, Object>();
		map3.put("bsname", "计算机科学与技术毕设");
		map3.put("bsteacher", "w");
		map3.put("bsdepar", "计算机科学与技术");
		map3.put("bscontents", "这是计算机毕设");
		map3.put("bsid", "03");
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		list.add(map1);
		list.add(map2);
		list.add(map3);
		return list;
	}
}

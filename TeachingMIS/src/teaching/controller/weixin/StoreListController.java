package teaching.controller.weixin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller

public class StoreListController {
	@RequestMapping(value = "storelist")
	@ResponseBody
	public List<Map<String, Object>> storeList(HttpServletRequest request) {
		String userId = request.getParameter("userid");
		String classYear = request.getParameter("classyear");
		String classTerm = request.getParameter("classterm");
		String classDepar = request.getParameter("classdepar");

		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("classname", "数据库");
		map1.put("classteacher", "z");
		map1.put("classdepar", "软件工程");
		map1.put("classcontents", "zzzz");
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("classname", "Java");
		map2.put("classteacher", "z");
		map2.put("classdepar", "软件工程");
		map2.put("classcontents", "zzzz");
		Map<String, Object> map3 = new HashMap<String, Object>();
		map3.put("classname", "计算机网络");
		map3.put("classteacher", "x");
		map3.put("classdepar", "网络工程");
		map3.put("classcontents", "xxxx");
		Map<String, Object> map4 = new HashMap<String, Object>();
		map4.put("classname", "电路");
		map4.put("classteacher", "x");
		map4.put("classdepar", "网络工程");
		map4.put("classcontents", "xxxx");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		list.add(map1);
		list.add(map2);
		list.add(map3);
		list.add(map4);

		return list;
	}
}

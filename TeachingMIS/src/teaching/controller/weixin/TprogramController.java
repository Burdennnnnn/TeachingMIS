package teaching.controller.weixin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
//@RequestMapping(value="weixin")
public class TprogramController {
     @RequestMapping(value="tprogram")
     @ResponseBody
     public List<Map<String,Object>> tprogram(HttpServletRequest request,HttpServletResponse response){
    	 Map<String,Object> map = new HashMap<String, Object>();
    	 String programyear = request.getParameter("programyear");
    	 String programterm = request.getParameter("programterm");
    	 String coursedepar = request.getParameter("coursedepar");
    	 map.put("programname", "大学物理");
    	 map.put("programteacher", "张兰");
    	 map.put("programdepar","计算机");
    	 map.put("programfileurl", "127.0.0.1");
    	 List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
    	 list.add(map);
    	 return list;
     }
}

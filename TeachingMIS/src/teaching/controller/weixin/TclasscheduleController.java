package teaching.controller.weixin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
//@RequestMapping(value="weixin")
public class TclasscheduleController {
	@RequestMapping(value="tclasschedule")
	 @ResponseBody
   public List<Map<String,Object>> tclasschedule(HttpServletRequest request,HttpServletResponse response){
	   Map<String,Object> map = new HashMap<String, Object>();
	   String userid = request.getParameter("userid");
	   String courseyear = request.getParameter("courseyear");
	   String courseterm = request.getParameter("courseterm");
	   String coursedepar = request.getParameter("coursedepar");
	   
	   map.put("coursename", "数据结构");
	   map.put("coursetime", "9.30");
	   map.put("courseplace", "综合楼");
	   List<Map<String,Object>> list = new  ArrayList<Map<String,Object>>();
	   list.add(map);
	   return list;
   }
}

package teaching.controller.weixin;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.service.designManage.courseDesign.CourseDesignService;
import teaching.service.wexin.KesheService;

@Controller
@RequestMapping(value="weixin/courseDesign")
public class KesheController {
	
	@Autowired
	private KesheService kesheService;
	@Autowired
	private CourseDesignService courseDesignService;
	
	@RequestMapping(value="designList")
	public @ResponseBody List<Map<String,String>> list(HttpServletRequest request){
		List<Map<String,String>> list = new ArrayList<Map<String,String>>();
		String teacherID = request.getParameter("userId");
		String flag = request.getParameter("flag");
		String majorID = request.getParameter("kesheMajor");
		String term = request.getParameter("kesheSemester");
		String academicYear = request.getParameter("kesheYear");
		Map<String,String> condition = new HashMap<String,String>();
		condition.put("major", majorID);
		condition.put("term", term);
		condition.put("academicYear", academicYear);
		if(flag.equals("1")){
			condition.put("teacherID", teacherID);
		}
		try {
			list = kesheService.getCourseDesign(condition);
			for (Map<String, String> map : list) {
				String[] teacherIDs = map.get("teacherID").toString().split(",");
				String teacherName = "";
				for (int i = 0; i < teacherIDs.length; i++) {
					teacherName += courseDesignService.getTeacherByTeacherID(teacherIDs[i])+",";
				}
				teacherName = teacherName.substring(0,teacherName.length()-1);
				map.put("teacherName", teacherName);
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		}
        return list;
	}
	
	@RequestMapping(value="getDesign")
    public @ResponseBody Map<String,String> design(HttpServletRequest request){
    	String kesheId = request.getParameter("kesheId");
    	Map<String,String> idMap = new HashMap<String,String>();
    	idMap.put("kesheId", kesheId);
    	Map<String,String> design = new HashMap<String,String>();
    	if(kesheId!=null && !kesheId.equals("")){
    		try {
				design = kesheService.getDesignBaseInfo(idMap);
				String[] teacherIDs = design.get("teacherID").toString().split(",");
				String teacherName = "";
				for (int i = 0; i < teacherIDs.length; i++) {
					teacherName += courseDesignService.getTeacherByTeacherID(teacherIDs[i])+",";
				}
				teacherName = teacherName.substring(0,teacherName.length()-1);
				design.put("teacherName", teacherName);
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    	return design;
    }
}


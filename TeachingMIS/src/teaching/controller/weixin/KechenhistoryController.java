package teaching.controller.weixin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
//@RequestMapping(value="weixin")
public class KechenhistoryController {
	@RequestMapping(value="kechenhistory")
	 @ResponseBody
    public List<Map<String,Object>> kechenhistory(HttpServletRequest request,HttpServletResponse response){
    	Map<String,Object> map = new HashMap<String, Object>();
    	String classyear = request.getParameter("classyear");
    	String classterm = request.getParameter("classterm");
    	String classdepar = request.getParameter("classdepar");
    	map.put("classname", "软件工程");
    	map.put("classteacher", "朱峰");
    	map.put("classdepar", "计算机");
    	map.put("classcontents", "良好");
    	List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
    	list.add(map);
    	return list;
    }
}

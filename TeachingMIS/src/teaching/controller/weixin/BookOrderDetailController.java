package teaching.controller.weixin;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
//@RequestMapping(value="weixin")
public class BookOrderDetailController {

	@RequestMapping(value="bookOrderdetail")
	 @ResponseBody
	public Map<String,Object> bookOrderDetail(HttpServletRequest request,HttpServletResponse response){
		
		Map<String,Object> map =  new HashMap<String, Object>();
		String orderid = request.getParameter("orderid");
		String bookname = request.getParameter("bookname");
		String publisher = request.getParameter("publisher");
		String author = request.getParameter("author");
		String isbn = request.getParameter("isbn");
		String price = request.getParameter("price");
		String ocheckflag = request.getParameter("ocheckflag");
		map.put("message", "成功");
		return map;
	}
	
}

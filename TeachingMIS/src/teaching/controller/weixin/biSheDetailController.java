package teaching.controller.weixin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller

public class biSheDetailController {
	@RequestMapping(value="bishedetail")
	 @ResponseBody
	public List<Map<String,Object>> biSheDetail(HttpServletRequest request){
		String biSheId = request.getParameter("bisheid");
		Map<String,Object> map1 = new HashMap<String, Object>();
		map1.put("bsname", "软件工程毕设");
		map1.put("bsstudents", "a");
		map1.put("bscontents", "这是软件工程毕设");
		map1.put("bsyear", "2017");
		Map<String,Object> map2 = new HashMap<String, Object>();
		map2.put("bsname", "网络工程毕设");
		map2.put("bsstudents", "s");
		map2.put("bscontents", "这是网络工程毕设");
		map2.put("bsyear", "2015");
		Map<String,Object> map3 = new HashMap<String, Object>();
		map3.put("bsname", "计算机科学与技术毕设");
		map3.put("bsstudents", "d");
		map3.put("bscontents", "这是计算机毕设");
		map3.put("bsyear", "2016");
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		list.add(map1);
		list.add(map2);
		list.add(map3);
		return list;
	}
	
}

package teaching.controller.baseInfoManage.common;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import teaching.service.baseInfoManage.common.BaseInfoCommonService;
import teaching.service.baseInfoManage.common.UploadFile;

@Controller
@RequestMapping(value="baseInfoCommon")
public class BaseInfoCommonController {
	
	@Autowired
    private BaseInfoCommonService baseInfoCommonService;
	
	/**
	 * 查询专业名称及对应的id+查询培养方案名称及对应的id
	 * @author GuoFei
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="initData")
	@ResponseBody
	public Map<String,Object> initData(HttpServletResponse response,HttpServletRequest request){
		
		Map<String,Object> map = new HashMap<String, Object>();
		try {
			List<Map<String,Object>> major = baseInfoCommonService.getAllMajorAndId();
			List<Map<String,Object>> trainingScheme = baseInfoCommonService.getAllTrainingScheme();
			map.put("majorAndId", major);
			map.put("trainingSchemeAndId", trainingScheme);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value="uploadFiles")
	 @ResponseBody
	public List<String> uploadFiles(HttpServletResponse response,HttpServletRequest request){
		List<String> list = new ArrayList<String>();
		UploadFile uploadFile = new UploadFile();
		String imageFolderName = "images/personImages";
		list = uploadFile.uploadFile(request,imageFolderName);
		return list;
	}
	
	/**
	 * 增加学生页面和修改学生信息页面的初始化班级信息
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="getClassNameAndId")
	@ResponseBody
	public List<Map<String, Object>> getClassNameAndId(HttpServletResponse response,HttpServletRequest request){
		List<Map<String, Object>> list = null;
		try {
			list = baseInfoCommonService.getClassNameAndId();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return list;
	}
	
	/**
	 * 上传头像
	 * @param file
	 * @param request
	 * @return
	 */
	@RequestMapping(value="uploadPhoto")
	@ResponseBody
	public  Map<String,Object> uploadStuPhoto(@RequestParam("photo") MultipartFile file,HttpServletRequest request){
		String fileName = file.getOriginalFilename();
		String saveFileName = UUID.randomUUID().toString().replaceAll("-", "") + "-" + fileName;
		String path = "file/images";
		String realPath = request.getSession().getServletContext().getRealPath("/"+path);
		String pathtrue = realPath + "/"+saveFileName;
		try {
			file.transferTo(new File(pathtrue));
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Map<String,Object> map = new HashMap<>();
		map.put("picturePath", path+"/"+saveFileName);
		return map;
	}
	/**
	 * 查询所有学院
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="getXy")
	@ResponseBody
	public Map<String,Object> getXy(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> map = new HashMap<>();
		List<Map<String,Object>> list;
		try {
			list = baseInfoCommonService.getXy();
			map.put("xylist", list);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 按学院查询专业
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="getMajorByXy")
	@ResponseBody
	public Map<String,Object> getMajorByXy(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> map = new HashMap<>();
		List<Map<String,Object>> list;
		String collegeId = request.getParameter("collegeId");
		try {
			list = baseInfoCommonService.getMajorByXy(collegeId);
			map.put("majorlist", list);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 按专业查询入学年份
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="getJoinTime")
	@ResponseBody
	public Map<String,Object> getJoinTime(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> map = new HashMap<>();
		List<Map<String,Object>> list;
		String majorId = request.getParameter("majorId");
		try {
			list = baseInfoCommonService.getJoinTime(majorId);
			map.put("joinTime", list);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 按入学份查询班级
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="getClassByJoinTime")
	@ResponseBody
	public Map<String,Object> getClassByJoinTime(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> map = new HashMap<>();
		List<Map<String,Object>> list;
		String enrollmentTime = request.getParameter("enrollmentTime");
		Map<String,Object> map2 = new HashMap<String,Object>(); 
		map2.put("enrollmentTime", enrollmentTime);
		try {
			list = baseInfoCommonService.getClassByJoinTime(map2);
			map.put("class", list);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 直接初始化全部树
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="getAllTree")
	@ResponseBody
	public Map<String,Object> getAllTree(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> map = new HashMap<>();
		List<Map<String,Object>> xyList = null;
		try {
		    xyList = baseInfoCommonService.getXy();
			for(Map<String,Object> map2:xyList){	
				List<Map<String,Object>>  majorList = baseInfoCommonService.getMajorByXy(map2.get("id").toString());		
				for(Map<String,Object> map3 :majorList){
					String majorId = map3.get("id").toString();
					List<Map<String,Object>> timeList = baseInfoCommonService.getJoinTime(map3.get("id").toString());									
					for(Map<String,Object> map4 : timeList){
						String joinTime = map4.get("label").toString();
						Map<String,Object> map5 = new HashMap<String, Object>();
						map5.put("enrollmentTime", joinTime);
						map5.put("majorID", majorId);
						map4.put("children", baseInfoCommonService.getClassByJoinTime(map5));
					}
					map3.put("children", timeList);
				}
				map2.put("children",majorList);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		map.put("xyList", xyList);
		return map;
	}
	
	/**
	 * 班级初始化树
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="getTwoTree")
	@ResponseBody
	public Map<String,Object> getTwoTree(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> map = new HashMap<>();
		List<Map<String,Object>> xyList = null;
		try {
		    xyList = baseInfoCommonService.getXy();
			for(Map<String,Object> map2:xyList){	
				List<Map<String,Object>>  majorList = baseInfoCommonService.getMajorByXy(map2.get("id").toString());		
				for(Map<String,Object> map3 :majorList){
					List<Map<String,Object>> timeList = baseInfoCommonService.getJoinTime(map3.get("id").toString());												
					map3.put("children", timeList);
				}
				map2.put("children",majorList);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		map.put("xyList", xyList);
		return map;
	}
	/**
	 *教师界面初始化树
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="getOneTree")
	@ResponseBody
	public Map<String,Object> getOneTree(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> map = new HashMap<>();
		List<Map<String,Object>> xyList = null;
		try {
		    xyList = baseInfoCommonService.getXy();
			for(Map<String,Object> map2:xyList){	
				List<Map<String,Object>>  majorList = baseInfoCommonService.getMajorByXy(map2.get("id").toString());		
				map2.put("children",majorList);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		map.put("xyList", xyList);
		return map;
	}

  
	
}

package teaching.controller.baseInfoManage.teacherBaseInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import teaching.service.baseInfoManage.common.BaseInfoCommonService;
import teaching.service.baseInfoManage.common.ExcelUtil;
import teaching.service.baseInfoManage.common.ExcelUtil.ExcelReader;
import teaching.service.baseInfoManage.teacherBaseInfo.TeacherBaseInfoService;

@Controller
@RequestMapping(value="teacherBaseInfo")
public class TeacherBaseInfoController {
     
	@Autowired
	private TeacherBaseInfoService teacherBaseInfoService;
    @Autowired
    private BaseInfoCommonService baseInfoCommonService;
	/**
	 * 查询教师map(start,pageSize,teacherId,teacherName,teacherSex,degree,positionalTitle)
	 * @author GuoFei
	 * @param page
	 * @param response
	 * @param request
	 * @return
	 * @throws ParseException 
	 * @throws SQLException 
	 */
	@RequestMapping(value="getTeacher")
	 @ResponseBody
	public Map<String, Object> getTeacher(@RequestParam(value="page",required=false) Integer  page,@RequestParam(value="pageSize",required=false) Integer  pageSize,HttpServletResponse response,HttpServletRequest request) throws ParseException, SQLException{
		
		List<Map<String, Object>> list = null;
		Map<String,Object> map = new HashMap<String, Object>();
		Map<String,Object> map2= new HashMap<String, Object>();
		int start = 0; 
		 if(page!=null&&pageSize!=null){
			 start = (page-1)*pageSize;
		 }
		map.put("start",start);
		map.put("pageSize",pageSize);
		map.put("teacherId", request.getParameter("teacherId"));
		map.put("teacherName", request.getParameter("teacherName"));
		map.put("teacherSex", request.getParameter("teacherSex"));
		map.put("degree", request.getParameter("degree"));
		map.put("positionalTitle", request.getParameter("positionalTitle"));
		map.put("majorId", request.getParameter("majorId"));
	    String id = request.getParameter("id");	   
	    Map<String,Object> map3 = this.findId(id);
	    Iterator  i=map3.entrySet().iterator();
	    String str = "";
	    while(i.hasNext()){//只遍历一次,速度快
          Map.Entry  e=(Map.Entry)i.next();      
          str=(String) e.getKey();
	    }
	    switch (str) {
			case "collegeId":
				  map.put("collegeId", map3.get("collegeId"));
				  break;
			case "majorId":
				  map.put("majorId2", map3.get("majorId"));
				  break;
		
			default:
				break;
		}
		
//		map.put("collegeId", request.getParameter("collegeId"));
//		map.put("majorId", request.getParameter("majorId"));
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");						
		 map.put("createTime", sdf.parse(sdf.format(new Date())));
		try {
			list = teacherBaseInfoService.getTeacher(map);
			int countNum = teacherBaseInfoService.getTeacherCountNum(map);
			map2.put("teacherList", list);
			map2.put("countNum", countNum);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return map2;
	}
	
	/**
	 * 修改教师信息map(oldTeacherId,teacherId,teacherName,teacherSex,positionalTitle,
	 * teacherPosition,teacherTel,majorId,education,graduateSchool,teacherPhoto,
	 * joinSchoolTime,graduateMajor,degree,teacherBirth)
	 * @author GuoFei
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="updateTeacher")
	 @ResponseBody
	public Map<String,Object> updateTeacher(HttpServletResponse response,HttpServletRequest request){
		
		Map<String,Object> resultMap = new HashMap<String, Object>();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("oldTeacherId",request.getParameter("oldTeacherId"));
		map.put("teacherId",request.getParameter("teacherId"));
		map.put("collegeID",request.getParameter("collegeId"));
		map.put("teacherName",request.getParameter("teacherName"));
		map.put("teacherSex",request.getParameter("teacherSex"));
		map.put("positionalTitle",request.getParameter("positionalTitle"));
		map.put("teacherPosition",request.getParameter("teacherPosition"));
		map.put("teacherTel",request.getParameter("teacherTel"));
		map.put("majorId",request.getParameter("majorId"));
		map.put("graduateSchool",request.getParameter("graduateSchool"));
		map.put("education",request.getParameter("education"));
		map.put("teacherPhoto",request.getParameter("teacherPhoto"));
		map.put("joinSchoolTime",request.getParameter("joinSchoolTime"));
		map.put("graduateMajor",request.getParameter("graduateMajor"));
		map.put("degree",request.getParameter("degree"));
		map.put("teacherBirth",request.getParameter("teacherBirth"));
		try {
			int status = teacherBaseInfoService.updateTeacher(map);
			if(status == 1){
				resultMap.put("message", "修改教师信息成功");
			}else{
				resultMap.put("message", "修改教师信息失败");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultMap;
	}
	
	/**
	 * 增加教师，map(teacherId,teacherName,teacherSex,positionalTitle,
	 * teacherPosition,teacherTel,majorId,education,graduateSchool,teacherPhoto,
	 * joinSchoolTime,graduateMajor,degree,teacherBirth)
	 * @author GuoFei
	 * @param response
	 * @param request
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping(value="addTeacher")
	 @ResponseBody
	public Map<String,Object> addTeacher(HttpServletResponse response,HttpServletRequest request) throws ParseException{
	 
	    Map<String,Object> resultMap = new HashMap<String, Object>();
		Map<String,Object> map = new HashMap<String, Object>();
		
		map.put("teacherId",request.getParameter("teacherId"));
		map.put("collegeID",request.getParameter("collegeId"));
		map.put("teacherName",request.getParameter("teacherName"));
		map.put("teacherSex",request.getParameter("teacherSex"));
		map.put("positionalTitle",request.getParameter("positionalTitle"));
		map.put("teacherPosition",request.getParameter("teacherPosition"));
		map.put("teacherTel",request.getParameter("teacherTel"));
		map.put("majorId",request.getParameter("majorId"));
		map.put("graduateSchool",request.getParameter("graduateSchool"));
		map.put("education",request.getParameter("education"));
		map.put("teacherPhoto",request.getParameter("teacherPhoto"));
		map.put("joinSchoolTime",request.getParameter("joinSchoolTime"));
		map.put("graduateMajor",request.getParameter("graduateMajor"));
		map.put("degree",request.getParameter("degree"));
		map.put("teacherBirth",request.getParameter("teacherBirth"));
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");		
		 map.put("createTime", sdf.parse(sdf.format(new Date())));
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		try {
			list.add(map);
			int status = teacherBaseInfoService.addTeacher(list);
			if(status == 1){
				resultMap.put("message", "添加老师成功");
			}else{
				resultMap.put("message", "添加老师失败");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultMap;
	}
	/**
	 * 删除教师
	 * @author GuoFei
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="deleteTeacher")
	 @ResponseBody
	public  Map<String,Object> deleteTeacher(HttpServletResponse response,HttpServletRequest request){
		 Map<String,Object> resultMap = new HashMap<String, Object>();
		 String teacherId = request.getParameter("teacherId");
		 try {
				int status = teacherBaseInfoService.deleteTeacher(teacherId);
				if(status == 1){
					resultMap.put("message", "删除老师成功");
				}else{
					resultMap.put("message", "删除老师失败");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 return resultMap;
	}
	
	 /**
	  * 导出教师
	  * @param response
	  * @param request
	  * @return
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws SQLException 
	  */
	 @RequestMapping(value="exportTeacher")
	 @ResponseBody
	 public Map<String,Object> exportTeacher(HttpServletRequest request) throws FileNotFoundException, IOException, SQLException{
		  
		    Map<String,Object> map = new HashMap<String, Object>(); 	    
			String basePath = this.getClass().getResource("/").getPath();
			if(System.getProperty("os.name").toLowerCase().indexOf("linux")>=0){
				basePath = basePath.substring(0, basePath.indexOf("/WEB-INF"));
			}else{
				basePath = basePath.substring(1, basePath.indexOf("/WEB-INF"));
			}
			
			String fileName = "template_teacher.xls";		
			String templatePath = basePath + "/template/baseInfoManage/" + fileName;
			// 先读取模板 
		    POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(templatePath));
		    HSSFWorkbook wb = new HSSFWorkbook(fs);  
		    //在webbook中添加一个sheet,对应Excel文件中的sheet  
		    HSSFSheet sheet = wb.getSheetAt(0);  
		    //在sheet中添加表头第0行 
		    HSSFRow row;
		    HSSFCellStyle cellStyle = ExcelUtil.setCellStyle(wb);
		    //写入实体数据 ,数据从数据库得到，  
		    List< Map<String,Object>> list = null;
		    Map<String,Object> map2 = new HashMap<String, Object>();	  
			map2.put("teacherName",request.getParameter("teacherName"));
			map2.put("teacherSex",request.getParameter("teacherSex"));
			map2.put("degree", request.getParameter("degree"));
			map2.put("positionalTitle", request.getParameter("positionalTitle"));
			map2.put("majorId", request.getParameter("majorId"));
			
			    String id = request.getParameter("id");	   
			    Map<String,Object> map3 = this.findId(id);
			    Iterator  it=map3.entrySet().iterator();
			    String str = "";
			    while(it.hasNext()){//只遍历一次,速度快
		          Map.Entry  e=(Map.Entry)it.next();      
		          str=(String) e.getKey();
			    }
			    switch (str) {
					case "collegeId":
						  map2.put("collegeId", map3.get("collegeId"));
						  break;
					case "majorId":
						  map2.put("majorId2", map3.get("majorId"));
						  break;
				
					default:
						break;
				}
			
		    try {
				list = teacherBaseInfoService.getTeacher(map2);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    if(list!=null){
		    	for (int i = 0; i < list.size(); i++) {
		    		 row = sheet.createRow( i + 1); 		    				                
		            row.createCell(0).setCellValue(list.get(i).get("teacherId").toString()); 
		            row.getCell(0).setCellStyle(cellStyle);
		            row.createCell(1).setCellValue(list.get(i).get("teacherName").toString()); 
		            row.getCell(1).setCellStyle(cellStyle);
		            row.createCell(2).setCellValue(list.get(i).get("teacherSex").toString());
		            row.getCell(2).setCellStyle(cellStyle);
		            row.createCell(3).setCellValue(list.get(i).get("teacherBirth").toString()); 
		            row.getCell(3).setCellStyle(cellStyle);
		            row.createCell(4).setCellValue(list.get(i).get("positionalTitle").toString()); 
		            row.getCell(4).setCellStyle(cellStyle);
		            row.createCell(5).setCellValue(list.get(i).get("teacherPosition").toString()); 
		            row.getCell(5).setCellStyle(cellStyle);
		            row.createCell(6).setCellValue(list.get(i).get("teacherTel").toString()); 
		            row.getCell(6).setCellStyle(cellStyle);
		            row.createCell(7).setCellValue(list.get(i).get("majorName").toString()); 
		            row.getCell(7).setCellStyle(cellStyle);
		            row.createCell(8).setCellValue(list.get(i).get("education").toString()); 
		            row.getCell(8).setCellStyle(cellStyle);
		            row.createCell(9).setCellValue(list.get(i).get("degree").toString()); 
		            row.getCell(9).setCellStyle(cellStyle);
		            row.createCell(10).setCellValue(list.get(i).get("graduateSchool").toString()); 
		            row.getCell(10).setCellStyle(cellStyle);
		            row.createCell(11).setCellValue(list.get(i).get("joinSchoolTime").toString()); 
		            row.getCell(11).setCellStyle(cellStyle);
		            row.createCell(12).setCellValue(list.get(i).get("graduateMajor").toString()); 
		            row.getCell(12).setCellStyle(cellStyle);
			                  		     	     
			    }  
		    }
		  
		    
	    try{  
	    	String path = "file/export";
	    	String realPath = request.getSession().getServletContext().getRealPath("/"+path);
	    	String pathtrue = realPath + "/"+fileName;
	        FileOutputStream fout = new FileOutputStream(pathtrue);  
	        wb.write(fout);  
	        fout.close();  
	        String uri =  request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
	       
	        map.put("message", uri+"/"+"TeachingMIS/"+path+"/"+fileName);
	    }  
	    catch (Exception e){ 
	    
	    	map.put("message", "导出失败！");
	        e.printStackTrace();  
	    }  
		 
		 return map;
	 }
	 
	 /**
		 * 导入教师
		 * @param fileName
		 * @return
		 * @throws SQLException 
	 * @throws ParseException 
		 */
		 @RequestMapping(value="importTeacher")
		 @ResponseBody
		 public Map<String,Object> importTeacher(@RequestParam("file") MultipartFile file,HttpServletRequest request) throws SQLException, ParseException{
			    String fileName = file.getOriginalFilename();
				String saveFileName = UUID.randomUUID().toString().replaceAll("-", "") + "-" + fileName;
				String path = "file/import";
				String realPath = request.getSession().getServletContext().getRealPath("/"+path);
				String pathtrue = realPath + "/"+saveFileName;
				try {
					file.transferTo(new File(pathtrue));
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			
			 Map<String,Object> map = new HashMap<String, Object>();
			 ExcelUtil excelUtil = new ExcelUtil();
			 InputStream is = null;
			
			 try {
				is = new FileInputStream(pathtrue);
			 } catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			 }
			 ExcelReader excelReader = excelUtil.getExcelReader();
			 List<Map<Integer, String>>  list = excelReader.readExcelContent(is);	
			 List<Map<String,Object>>  list2 = new ArrayList<Map<String,Object>>();
			 if(list!=null){
				 for(int i = 0;i<list.size();i++){	
					 if(list.get(i).get(0) != null&&!"".equals(list.get(i).get(0))){
						 Map<String,Object> map2 = new HashMap<String, Object>();
						 map2.put("teacherId",list.get(i).get(0));
						 map2.put("teacherName", list.get(i).get(1));									
						 map2.put("teacherSex", list.get(i).get(2));
						 map2.put("teacherBirth", list.get(i).get(3));
						 map2.put("positionalTitle", list.get(i).get(4));
						 map2.put("teacherPosition", list.get(i).get(5));	
						 map2.put("teacherTel", list.get(i).get(6));
						 map2.put("majorId",  baseInfoCommonService.getMajorIdByName(list.get(i).get(7)));					
						 map2.put("education", list.get(i).get(8));	
						 map2.put("degree", list.get(i).get(9));	
						 map2.put("graduateSchool", list.get(i).get(10));	
						 map2.put("joinSchoolTime", list.get(i).get(11));	
						 map2.put("graduateMajor", list.get(i).get(12));
						 map2.put("collegeID", baseInfoCommonService.getCollegeIdByName(list.get(i).get(13)));
						 map2.put("teacherPhoto", null);
						 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");						
						 map2.put("createTime", sdf.parse(sdf.format(new Date())));
						 list2.add(map2);					
					 }		 
				 }
			 }	
			 teacherBaseInfoService.addTeacher(list2); 
			 File file2 = new File(pathtrue);
			 if(file2.exists()){
				 file2.delete();
			 }
		     map.put("message", "导入成功！");
			
			 return map;
		 
		 }
		 /**
		     * 查询树的时候判断ID是属于哪一学院，专业，班级
		     * @param id
		     * @return
		     * @throws SQLException
		     */
		    public  Map<String,Object>  findId(String id) throws SQLException{
		    	Map<String,Object> map = new  HashMap<String, Object>();
		    	List<Map<String,Object>> xyList = baseInfoCommonService.getXy();
		    	List<Map<String,Object>> majorList = baseInfoCommonService.getAllMajorAndId();
		    	List<Map<String,Object>> classList = baseInfoCommonService.getClassNameAndId();
		    	for(Map<String,Object> obj :xyList){
		    		if(obj.get("id").equals(id)){
		    			map.put("collegeId", id);
		    			return map;
		    		}
		    	}
		    	for(Map<String,Object> obj :majorList){
		    		if(obj.get("majorID").equals(id)){
		    			map.put("majorId", id);
		    			return map;
		    		}
		    	}
		    	for(Map<String,Object> obj :classList){
		    		if(obj.get("classId").equals(id)){
		    			map.put("classId", id);
		    			return map;
		    		}
		    	}
		    	return map;
		   
		    }
			 
	 
}

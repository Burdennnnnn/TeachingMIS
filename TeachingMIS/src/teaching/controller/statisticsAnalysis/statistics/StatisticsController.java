package teaching.controller.statisticsAnalysis.statistics;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;





import teaching.service.statisticsAnalysis.statistics.StatisticsService;

@Controller
@RequestMapping(value="statistics")
public class StatisticsController {
	@Autowired
    private StatisticsService statisticsService;
	
	/**
	 * 查看具体工作
	 * @param request
	 * @param response
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value="getDetail")
	public Map<String,Object> getDetail(HttpServletRequest request,HttpServletResponse response) throws SQLException{
		 Map<String,Object> map = new HashMap<String, Object>();
		 String teacherId = request.getParameter("teacherId");
		 List<Map<String,Object>> list1 = statisticsService.getCourse(teacherId);
		 if(list1!=null){
			 for(Map<String,Object> obj:list1){			
				 obj.put("class", statisticsService.getCourseToClass(obj.get("trainingCourseID").toString()));			 
			 }
		 }
		 List<Map<String,Object>> list2 = statisticsService.getCourseDesign(teacherId);
		 List<Map<String,Object>> list3 = statisticsService.getGraduateProgram(teacherId);
		 map.put("course", list1);
		 map.put("courseDesign", list2);
		 map.put("graduateProgram", list3);
		 return map;
	
	}
	
	
	
}

package teaching.controller.wym.common;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.service.wym.commom.CommonService;

@Controller
@RequestMapping(value="common")
public class CommonController {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private CommonService commonService;
    
    @RequestMapping(value="/getAcademicYear")
    @ResponseBody
    public List<Map<String,String>> getAcademicYear(){
    	List<Map<String,String>> yearList = new ArrayList<Map<String,String>>();
    	try {
			yearList = commonService.getAcademicYear();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
        return yearList;
    }
    
    @RequestMapping(value="/getMajorList")
    @ResponseBody
    public List<Map<String,String>> getMajorList(){
    	List<Map<String,String>> majorList = new ArrayList<Map<String,String>>();
    	try {
    		majorList = commonService.getMajorList();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
        return majorList;
    }
    
    @RequestMapping(value="/getScoringWay")
    @ResponseBody
    public List<Map<String,String>> getScoringWay(){
    	List<Map<String,String>> wayList = new ArrayList<Map<String,String>>();
    	try {
    		wayList = commonService.getScoringWay();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
        return wayList;
    }
}

package teaching.controller.login;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import teaching.entity.User;
import teaching.service.SystemManage.LoginService;

@Controller
@RequestMapping("PClogin")
public class PCLoginController {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private LoginService loginService;
	@Autowired
	HttpServletRequest httpServletRequest;
	
	@RequestMapping("pclogin")
	public @ResponseBody Map<String,Object> login(@RequestBody Map<String,String> loginInfo,HttpSession session){
		
		Map<String,Object> result = new HashMap<String,Object>();
		
		try {
			String userID = loginService.login(loginInfo);
			if(userID != null && !userID.equals("")){
				Map<String,String> ids = new HashMap<String,String>();
				ids.put("userID", userID);
				User user = new User();
				user.setUserID(userID);
				session.setAttribute("loginuser", user);
				List<String> roleID = loginService.getRoles(loginInfo);
				result.put("isLogin", "true");
			}else{
				result.put("isLogin", "false");
				result.put("message", "账号或密码错误!");
			}
		} catch (SQLException e) {
			result.put("error", e.getMessage());
		}
		return result;
	}
}

package teaching.mapper.textBookManage.textBookOrder;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import teaching.entity.TextBookOrder;

public interface TextBookOrderMapper {

	/**
	 * 查询订单map(start,pageSize,grade,majorName,academicYear,term)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String,Object>> getTextBookOrder(Map<String,Object> map)throws SQLException;
   
	/**
	 * 新增订单map(textbookId,courseId,orderNum,term,academicYear,grade,createTime)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public int addTextBookOrder(List<TextBookOrder> textBookOrderList)throws SQLException;
	 /**
     * 添加班级-订单关系
     *  @param map(orderId,classId)
     * @return
     * @throws SQLException
     */
    public int addOrderAndClass(List<Map<String,Object>> list)throws SQLException;
    /**
     * 修改班级-订单关系
     * @param classId
     * @return
     * @throws SQLException
     */
    public int updateOrderAndClass(List<Map<String,Object>> list)throws SQLException;
    /**
     * 确认订单
     * @param textBookOrder
     * @return
     * @throws SQLException
     */
    public int updateOrder(List<TextBookOrder> textBookOrderList)throws SQLException;
    
    /**
     * 教务处提交订单更改状态码为2
     * @param map(orderId)
     * @return
     * @throws SQLException
     */
    public int updateOrderByJW(List<Map<String,Object>> list)throws SQLException;
	
    /**
     * 统计订单数量map(majorName,grade,academicYear,term)
     * @param map
     * @return
     * @throws SQLException
     */
    public int getOrderCountNum(Map<String,Object> map)throws SQLException;
    /**
     * 教材名称及其id
     * @return
     * @throws SQLException
     */
    public List<Map<String,Object>> getCourseNameAndId()throws SQLException;
    public int updateOrder2(TextBookOrder textBookOrder)throws SQLException;
    public int updateOrderAndClass2(Map<String,Object> map)throws SQLException;
    public int delOrder(@Param("orderId")String orderId )throws SQLException;
    public List<Map<String,Object>> getArrangeCourse(Map<String,Object> map)throws SQLException;
    public int getCountCourse(Map<String,Object> map)throws SQLException;

}

package teaching.mapper.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import teaching.entity.TeachingDocument;

public interface TeachingDocumentMapper {
	
	List<TeachingDocument> documentList(Map<String,String> condition) throws SQLException;
	
	int addDocument(TeachingDocument teachingDocument)  throws SQLException;
	
	int deleteDocument(String documentID) throws SQLException;
}

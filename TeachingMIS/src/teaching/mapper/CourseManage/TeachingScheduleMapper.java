package teaching.mapper.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import teaching.entity.TeachingSchedule;

public interface TeachingScheduleMapper {

	List<TeachingSchedule> getScheduleList(Map<String, Object> condition) throws SQLException;

	int updateUseState(String teachercourseid)throws SQLException;

	int addSchedule(TeachingSchedule teachingSchedule)throws SQLException;

	int deleteSchedule(String teachingScheduleID)throws SQLException;

	
}

package teaching.mapper.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import teaching.entity.TestProgram;

public interface TestProgramMapper {

	List<TestProgram> getTestProgramList(Map<String, Object> condition) throws SQLException;

	int updateUseState(String testProgramID)throws SQLException;

	int addTestProgram(TestProgram testProgram)throws SQLException;

	int deleteTestProgram(String testProgramID)throws SQLException;

	
}

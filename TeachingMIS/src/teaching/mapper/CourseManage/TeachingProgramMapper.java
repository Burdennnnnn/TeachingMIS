package teaching.mapper.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import teaching.entity.TeachingProgram;

public interface TeachingProgramMapper {

	int addProgram(TeachingProgram teachingProgram) throws SQLException;
	int updateUseState(String courseID)throws SQLException;
	int deleteProgram(String teachingProgramID)throws SQLException;
	List<TeachingProgram> getProgramList(Map<String,Object> condition) throws SQLException;
	
}

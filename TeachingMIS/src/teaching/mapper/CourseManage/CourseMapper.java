package teaching.mapper.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import teaching.entity.Course;

public interface CourseMapper {

	List<String> getSchemaID(Map<String, String> condition)throws SQLException;

	String getArrangeID(Map<String, String> condition)throws SQLException;

	List<String> getClassNameList(Map<String, String> condition)throws SQLException;

	List<Map<String,Object>> getCoursesForTeacher(Map<String,String> condition)throws SQLException;

	Course getCourseById(String trainingCourseID);

	List<Map<String, Object>> getCoursesForStudent(Map<String, String> condition);
	
	String getClassID(String studentID)throws SQLException;
}

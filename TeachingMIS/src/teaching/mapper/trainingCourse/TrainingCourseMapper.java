package teaching.mapper.trainingCourse;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import teaching.entity.Course;

public interface TrainingCourseMapper {

	List<Course> getTrainCourseList(Map<String, String> condition) throws SQLException;

	int addCourse(Course course)throws SQLException;

	int updateCourse(Course course)throws SQLException;

	int deleteCourse(String trainingCourseID)throws SQLException;

	Course getTrainCourseByID(String trainingCourseID)throws SQLException;
}

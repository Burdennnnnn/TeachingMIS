package teaching.mapper.file;

import teaching.entity.FileIndex;

public interface FileMapper {

	int deleteFile(String filedId);

    int addFile(FileIndex fileIndex);

    FileIndex getFileByID(String filedId);

    int updateFile(FileIndex fileIndex);
    
}

package teaching.mapper.baseInfoManage.majorBaseInfo;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import teaching.entity.Major;

public interface MajorBaseInfoMapper {
	/**
	 * 添加专业信息
	 * @author GuoFei
	 * @param list
	 * @return
	 * @throws SQLException
	 */
    public int addMajor(List<Major> list)throws SQLException;
    
    /**
     * 修改专业信息(根据专业id修改)
     * @author GuoFei
     * @param map
     * @return
     * @throws SQLException
     */
    public int updateMajor(Map<String,Object> map)throws SQLException;
    
    /**
     * 分页查询专业(条件pageSize+page)
     * @author GuoFei
     * @param map
     * @return
     * @throws SQLException
     */
    public List<Major> getMajor(Map<String,Object> map)throws SQLException;
    
    /**
     * 根据专业id删除专业+班级+学生+毕设结果
     * @author GuoFei
     * @param majorId
     * @return
     * @throws SQLException
     */
    public int deleteMajor(@Param("majorId")String majorId)throws SQLException;
    
    /**
     * 统计专业数量
     * @return
     * @throws SQLException
     */
    public int getMajorCountNum()throws SQLException;







}

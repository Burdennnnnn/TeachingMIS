package teaching.mapper.baseInfoManage.teacherBaseInfo;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface TeacherBaseInfoMapper {
    
	/**
	 * 查询教师map(start,pageSize,teacherId,teacherName,teacherSex,degree,positionalTitle)
	 * @author GuoFei
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String,Object>> getTeacher(Map<String,Object> map)throws SQLException;
	
	/**
	 * 修改教师信息map(oldTeacherId,teacherId,teacherName,teacherSex,positionalTitle,
	 * teacherPosition,teacherTel,majorId,education,graduateSchool,teacherPhoto,
	 * joinSchoolTime,graduateMajor,degree,teacherBirth)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public int updateTeacher(Map<String,Object> map)throws SQLException;
	
	/**
	 * 增加教师，map(teacherId,teacherName,teacherSex,positionalTitle,
	 * teacherPosition,teacherTel,majorId,education,graduateSchool,teacherPhoto,
	 * joinSchoolTime,graduateMajor,degree,teacherBirth)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public int addTeacher(List<Map<String,Object>> list)throws SQLException;
	
	/**
	 * 删除教师
	 * @author GuoFei
	 * @param teacherId
	 * @return
	 * @throws SQLException
	 */
	public int deleteTeacher(@Param("teacherId")String teacherId)throws SQLException;
	/**
	 * 统计教师数量map(teacherName,teacherSex,degree,positionalTitle)
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public int getTeacherCountNum(Map<String,Object> map)throws SQLException;
}

package teaching.mapper.baseInfoManage.studentBaseInfo;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface StudentBaseInfoMapper {
   
	/**
	 * 分页查询学生信息map(start,pageSize,className,studentId,studentName)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String,Object>> getStudent(Map<String,Object> map)throws SQLException;
	
	/**
	 * 添加素学生信息
	 * map(studentId,studentName,studentSex,studentBirth,classId,studentJob,bankCard,IdNum,studentPhoto,phone,studentEmail)
	 * @author GuoFei
	 * @param list
	 * @return
	 * @throws SQLException
	 */
	public int addStudent(List<Map<String,Object>> list)throws SQLException;
	
	/**
	 * 删除学生信息
	 * @author GuoFei
	 * @param studentId
	 * @return
	 * @throws SQLException
	 */
	public int deleteStudent(@Param("studentId")String studentId)throws SQLException;
	
	/**
	 * 修改学生信息
	 * map(oldStudentId,studentId,studentName,studentSex,studentBirth,
	 * classId,studentJob,bankCard,IdNum,studentPhoto,phone,studentEmail)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public int updateStudent(Map<String,Object> map)throws SQLException;
	/**
	 * 统计学生总数量map(className,studentName)
	 * @return
	 * @throws SQLException
	 */
	public int getStudentCountNum(Map<String,Object> map)throws SQLException;
}

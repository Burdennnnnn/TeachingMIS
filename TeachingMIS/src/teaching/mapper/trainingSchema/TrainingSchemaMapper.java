package teaching.mapper.trainingSchema;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import teaching.entity.TrainingSchema;

public interface TrainingSchemaMapper {

    public int addSchema(TrainingSchema trainingSchema) throws SQLException;
    public int updateSchema(TrainingSchema trainingSchema)throws SQLException;
    public int deleteSchema(String trainingSchemeID)throws SQLException;
    public List<TrainingSchema> getSchemaList(Map<String,Object> condition)throws SQLException;
    public TrainingSchema getSchemaByID(String trainingSchemeID)throws SQLException;
    int getSchemaCount(Map<String,Object> condition)throws SQLException;
}

package teaching.mapper.SystemManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface UserManageMapper {

	int addUser(Map<String,String> userInfo)throws SQLException;
	int deleteUser(String userID)throws SQLException;
	List<String> getStudentIds(Map<String,String> condition)throws SQLException;
	int addUserRole(Map<String, String> userInfo)throws SQLException;
}

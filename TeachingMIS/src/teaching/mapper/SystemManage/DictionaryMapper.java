package teaching.mapper.SystemManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface DictionaryMapper {

	List<Map<String,Object>> getOneDictionary()throws SQLException;
	List<Map<String,Object>> getDictionnaryByUpId(Map<String,String> upId)throws SQLException;
	List<Map<String,Object>> getOneForTree()throws SQLException;
	List<Map<String,Object>> getTwoForTree(String upId)throws SQLException;
	int addDictionary(Map<String,String> dictionary)throws SQLException;
	int updateDictionary(Map<String,String> dictionary)throws SQLException;
	int deleteDictionary(String dictionaryID)throws SQLException;
	Map<String,String> getDictionry(String dictionaryID)throws SQLException;
}

package teaching.mapper.SystemManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;


public interface LoginMapper {

	String login(Map<String,String> login) throws SQLException;
	List<String> getRoles(Map<String,String> userID)throws SQLException;
	List<Map<String,String>> getOneMenu(Map<String,String> condition)throws SQLException;
	List<Map<String,String>> getTwoMenu(Map<String,String> condition)throws SQLException;
	
}

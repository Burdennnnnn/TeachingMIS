package teaching.mapper.weixin;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface NewsMapper {

	List<Map<String,String>> getNewsList()throws SQLException;
	int addNews(Map<String,String> news)throws SQLException;
	Map<String,String> getNewsById(Map<String,String> newId)throws SQLException;
	int deleteNews(Map<String,String> newId)throws SQLException;
	List<Map<String,String>> getReviewByNewsId(Map<String,String> newId)throws SQLException;
	int addReview(Map<String,String> review)throws SQLException;
}

package teaching.mapper.weixin;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface KesheMapper {

	List<Map<String,String>> getCourseDesign(Map<String,String> condition)throws SQLException;
	Map<String,String> getDesignBaseInfo(Map<String,String> kesheId)throws SQLException;
}

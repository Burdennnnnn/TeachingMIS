package teaching.mapper.designManage.graduateProgram;
/**
*   
*/

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface GraduateResultMapper {
	
	public Map<String,Object> getStudentGraduate(@Param("studentID")String studentID)throws SQLException;
	
	public int addGraduateResult (Map<String,Object>map) throws SQLException;
	
	public List<Map<String,Object>>getGraduateResult (Map<String,Object>map) throws SQLException;
	
	public Map<String,Object>getGraduateResultNum (Map<String,Object>map) throws SQLException;
	
	public Map<String,Object>getCheck (@Param("studentID")String studentID) throws SQLException;
	
	public int updateGraduateResult (Map<String,Object>map) throws SQLException;
	
	public int updateCheck (@Param("studentID")String studentID) throws SQLException;
	
}

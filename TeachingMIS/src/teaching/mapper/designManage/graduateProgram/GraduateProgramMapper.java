package teaching.mapper.designManage.graduateProgram;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
*                
*/
public interface GraduateProgramMapper {
	
	public int addGraduateProgram(Map<String,Object> map)throws SQLException;
	
	public List<Map<String,Object>> getGraduateProgram(Map<String,Object> map)throws SQLException;
	
	public Map<String,Object> getGraduateProgramNum(Map<String,Object> map)throws SQLException;

	public List<Map<String,Object>> getGraduateProgramStudent(Map<String,Object> map)throws SQLException;
	
	public List<Map<String,Object>> getGraduateInStudent(@Param("studentID")String studentID)throws SQLException;
	
	public int addStudentGraduate(Map<String,Object> map)throws SQLException;
	
	public int deleteStudentGraduate(@Param("studentID")String studentID)throws SQLException;
	
	public List<Map<String,Object>> getStudentNumByTeacher(Map<String,Object> map)throws SQLException;
	
	public Map<String,Object> getStudentByTeacherNum(Map<String,Object> map)throws SQLException;
	
	public List<Map<String,Object>> getStudentNum(Map<String,Object> map)throws SQLException;
	
	public Map<String,Object> getStudentTotalNum(Map<String,Object> map)throws SQLException;
	
	public int teacherChooseStudentDelete(Map<Object,Object> map)throws SQLException;
	
	public int teacherChooseStudentAdd(Map<Object,Object> map)throws SQLException;
	
	public int teacherChooseStudent(Map<Object,Object> map)throws SQLException;
	
	public List<Map<String,Object>> getGraduateInfoStudent(@Param("studentID")String studentID)throws SQLException;
	
	public List<Map<String,Object>> getStudentNOBelong(Map<String,Object> map)throws SQLException;
	
	public List<Map<String,Object>> getStudentInGraduate(@Param("graduateProgramID")String graduateProgramID)throws SQLException;
	
	public List<Map<String,Object>> getStudentInfo(@Param("graduateProgramID")String graduateProgramID)throws SQLException;
	
	public List<Map<String,Object>> getGraduateInfoTeacher(Map<String,Object> map)throws SQLException;
	
	public Map<String,Object> getGraduateInfoTeacherNum(Map<String,Object> map)throws SQLException;
	
	public List<Map<String,Object>> getGraduateInfoStaff(Map<String,Object> map)throws SQLException;
	
	public Map<String,Object> getGraduateInfoStaffNum(Map<String,Object> map)throws SQLException;
	
	public List<Map<String,Object>> exportGraduateInfoStaff(Map<String,Object> map)throws SQLException;
	
	public Map<String,Object> getPlanTimeWithID(@Param("graduateProPlanID")String graduateProPlanID)throws SQLException;
	
	//=================================================================
	public List<Map<String,Object>> getOneGraduateProgram(Map<String,Object> map)throws SQLException;
	
	
}

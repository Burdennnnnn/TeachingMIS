package teaching.service.trainingSchema;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import teaching.entity.TrainingSchema;

public interface TrainingSchameService {

    public boolean addSchema(TrainingSchema trainingSchema) throws SQLException;
    public boolean updateSchema(TrainingSchema trainingSchema)throws SQLException;
    public boolean deleteSchema(String trainingSchemaID)throws SQLException;
    public List<TrainingSchema> getSchemaList(Map<String,Object> condition)throws SQLException;
    public TrainingSchema getSchemaByID(String trainingSchemaID)throws SQLException;
    int getSchemaCount(Map<String,Object> condition)throws SQLException;
}

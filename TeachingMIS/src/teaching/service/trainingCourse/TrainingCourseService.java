package teaching.service.trainingCourse;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import teaching.entity.Course;

public interface TrainingCourseService {

	List<Course> getTrainCourseList(Map<String, String> condition) throws SQLException;

	boolean addCourse(Course course)throws SQLException;

	boolean updateCourse(Course course)throws SQLException;

	boolean deleteCourse(String trainingCourseID)throws SQLException;

	Course getTrainCourseByID(String courseID)throws SQLException;

}

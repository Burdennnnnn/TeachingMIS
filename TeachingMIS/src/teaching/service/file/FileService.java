package teaching.service.file;

import teaching.entity.FileIndex;

public interface FileService {

	boolean deleteFile(String filedId);

	boolean addFile(FileIndex fileIndex);

    FileIndex getFileByID(String filedId);

    boolean updateFile(FileIndex fileIndex);
}

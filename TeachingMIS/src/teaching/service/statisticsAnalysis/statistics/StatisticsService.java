package teaching.service.statisticsAnalysis.statistics;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;



public interface StatisticsService {
	/**
	 * 通过教师id得到课程信息
	 * @param teacherId
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String,Object>> getCourse(@Param("teacherId")String teacherId)throws SQLException;
    /**
     * 通过教师id得到课设信息
     * @param teacherId
     * @return
     * @throws SQLException
     */
	public List<Map<String,Object>> getCourseDesign(@Param("teacherId")String teacherId)throws SQLException;
   /**
    * 通过教师id得到毕设信息
    * @param teacherId
    * @return
    * @throws SQLException
    */
	public List<Map<String,Object>> getGraduateProgram(@Param("teacherId")String teacherId)throws SQLException;

	/**
	 * 根据培养课程编号得到教师所教授课程
	 * @param trainingCourseID
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String,Object>> getCourseToClass(@Param("trainingCourseID")String trainingCourseID)throws SQLException;
}

package teaching.service.impl.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.entity.TeachingProgram;
import teaching.mapper.CourseManage.TeachingProgramMapper;
import teaching.service.CourseManage.TeachingProgramService;

@Service
public class TeachingProgramServiceImpl implements TeachingProgramService{

	@Autowired
	private TeachingProgramMapper teachingProgramMapper;
	@Override
	public boolean addProgram(TeachingProgram teachingProgram) throws SQLException {
		if(teachingProgramMapper.addProgram(teachingProgram)>0 && teachingProgramMapper.updateUseState(teachingProgram.getCourseID())>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteProgram(String teachingProgramID) throws SQLException {
		return teachingProgramMapper.deleteProgram(teachingProgramID)>0?true:false;
	}

	@Override
	public List<TeachingProgram> getProgramList(Map<String,Object> condition) throws SQLException {
		return teachingProgramMapper.getProgramList(condition);
	}

}

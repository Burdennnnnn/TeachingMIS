package teaching.service.impl.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.entity.TeachingDocument;
import teaching.mapper.CourseManage.TeachingDocumentMapper;
import teaching.service.CourseManage.TeachingDocumentService;

@Service
public class TeachingDocumentServiceImpl implements TeachingDocumentService{

	@Autowired
	private TeachingDocumentMapper teachingDocumentMapper;
	@Override
	public List<TeachingDocument> documentList(Map<String, String> condition) throws SQLException {
		return teachingDocumentMapper.documentList(condition);
	}

	@Override
	public int addDocument(TeachingDocument teachingDocument) throws SQLException {
		return teachingDocumentMapper.addDocument(teachingDocument);
	}

	@Override
	public int deleteDocument(String documentID) throws SQLException {
		return teachingDocumentMapper.deleteDocument(documentID);
	}

	
}

package teaching.service.impl.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.entity.CourseArrange;
import teaching.entity.TeacherCourse;
import teaching.mapper.CourseManage.CourseArrangeMapper;
import teaching.service.CourseManage.CourseArrangeService;

@Service
public class CourseArrangeServiceImpl implements CourseArrangeService{

	@Autowired
	private CourseArrangeMapper courseArrangeMapper;


	@Override
	public boolean addArrange(CourseArrange courseArrange) throws SQLException {
		return courseArrangeMapper.addArrange(courseArrange)>0?true:false;
	}

	@Override
	public boolean addTeacherCourse(TeacherCourse teacherCourse) throws SQLException {
		return courseArrangeMapper.addTeacherCourse(teacherCourse)>0?true:false;
	}

	@Override
	public List<Map<String, Object>> getArrangeList(Map<String, String> condition) throws SQLException {
		return courseArrangeMapper.getArrangeList(condition);
	}

	@Override
	public String getClass(Map<String, String> condition) throws SQLException {
		return courseArrangeMapper.getClass(condition);
	}

	@Override
	public List<Map<String, Object>> getArrangeDetailList(Map<String, String> condition) throws SQLException {
		return courseArrangeMapper.getArrangeDetailList(condition);
	}

	@Override
	public boolean updateChenck(Map<String, String> checkInfo) throws SQLException {
		return courseArrangeMapper.updateChenck(checkInfo)>0?true:false;
	}

	@Override
	public boolean addCheckOpinion(Map<String, String> checkInfo) throws SQLException {
		return courseArrangeMapper.addCheckOpinion(checkInfo)>0?true:false;
	}

	@Override
	public String getCheckOpinion(String coursesArrangeID) throws SQLException {
		return courseArrangeMapper.getCheckOpinion(coursesArrangeID);
	}

	@Override
	public boolean updateTeacher(Map<String, String> checkInfo) throws SQLException {
		return courseArrangeMapper.updateTeacher(checkInfo)>0?true:false;
	}

	@Override
	public List<Map<String, String>> getTeachers(Map<String,String> majorID) throws SQLException {
		return courseArrangeMapper.getTeachers(majorID);
	}

	@Override
	public List<Map<String, Object>> getCourseList(Map<String, String> condition) throws SQLException {
		return courseArrangeMapper.getCourseList(condition);
	}

	@Override
	public List<Map<String, String>> getClassYear(Map<String, String> majorID) throws SQLException {
		return courseArrangeMapper.getClassYear(majorID);
	}

	@Override
	public List<Map<String, String>> getWillCourse(Map<String, String> courseInfo) throws SQLException {
		return courseArrangeMapper.getWillCourse(courseInfo);
	}

	@Override
	public List<Map<String, Object>> getCourseArrList(Map<String, String> condition) throws SQLException {
		return courseArrangeMapper.getCourseArrList(condition);
	}

	@Override
	public List<Map<String, String>> getPreArrange(Map<String, String> preCon) {
		return courseArrangeMapper.getPreArrange(preCon);
	}


}

package teaching.service.impl.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.entity.TeachingSchedule;
import teaching.mapper.CourseManage.TeachingScheduleMapper;
import teaching.service.CourseManage.TeachingScheduleService;

@Service
public class TeachingScheduleServiceImpl implements TeachingScheduleService{

	@Autowired
	private TeachingScheduleMapper teachingScheduleMapper;

	@Override
	public List<TeachingSchedule> getScheduleList(Map<String, Object> condition) throws SQLException {
		return teachingScheduleMapper.getScheduleList(condition);
	}

	@Override
	public boolean addSchedule(TeachingSchedule teachingSchedule) throws SQLException {
		if(teachingScheduleMapper.addSchedule(teachingSchedule)>0 && teachingScheduleMapper.updateUseState(teachingSchedule.getTeacherCourseID())>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteSchedule(String teachingScheduleID) throws SQLException {
		return teachingScheduleMapper.deleteSchedule(teachingScheduleID)>0?true:false;
	}

	

}

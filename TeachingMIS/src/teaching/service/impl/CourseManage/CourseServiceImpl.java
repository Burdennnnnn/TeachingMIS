package teaching.service.impl.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.entity.Course;
import teaching.mapper.CourseManage.CourseMapper;
import teaching.service.CourseManage.CourseService;

@Service
public class CourseServiceImpl implements CourseService{

    @Autowired
	private CourseMapper courseMapper;
	@Override
	public List<Map<String, Object>> getCoursesForTeacher(Map<String, String> condition) throws SQLException {
		return courseMapper.getCoursesForTeacher(condition);
	}
	
	@Override
	public Course getCourseById(String trainingCourseID) throws SQLException {
		return courseMapper.getCourseById(trainingCourseID);
	}

	@Override
	public List<Map<String, Object>> getCoursesForStudent(Map<String, String> condition) {
		return courseMapper.getCoursesForStudent(condition);
	}

	@Override
	public String getClassID(String studentID) throws SQLException {
		return courseMapper.getClassID(studentID);
	}

}

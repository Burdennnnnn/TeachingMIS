package teaching.service.impl.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.entity.TestProgram;
import teaching.mapper.CourseManage.TestProgramMapper;
import teaching.service.CourseManage.TestProgramService;

@Service
public class TestProgramServiceImpl implements TestProgramService{

	@Autowired
	private TestProgramMapper testProgramMapper;

	@Override
	public List<TestProgram> getTestProgramList(Map<String, Object> condition) throws SQLException {
		return testProgramMapper.getTestProgramList(condition);
	}

	@Override
	public boolean addTestProgram(TestProgram testProgram) throws SQLException {
		if(testProgramMapper.addTestProgram(testProgram)>0 && testProgramMapper.updateUseState(testProgram.getTeacherCourseID())>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteTestProgram(String testProgramID) throws SQLException {
		return testProgramMapper.deleteTestProgram(testProgramID)>0?true:false;
	}

	

}

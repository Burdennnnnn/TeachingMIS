package teaching.service.impl.textBookManage.textBookOrder;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.entity.TextBookOrder;
import teaching.mapper.textBookManage.textBookOrder.TextBookOrderMapper;
import teaching.service.textBookManage.textBookOrder.TextBookOrderService;

@Service
public class TextBookOrderImpl implements TextBookOrderService{
   
	@Autowired
	private TextBookOrderMapper textBookOrderMapper;
	
	/**
	 * 查询订单map(start,pageSize,majorName,academicYear,term)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	@Override
	public List<Map<String, Object>> getTextBookOrder(Map<String, Object> map)
			throws SQLException {
		// TODO Auto-generated method stub
		return textBookOrderMapper.getTextBookOrder(map);
	}

	/**
	 * 新增订单map(textbookId,courseId,orderNum,term,academicYear,grade,createTime)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int addTextBookOrder(List<TextBookOrder> textBookOrderList) throws SQLException {
		// TODO Auto-generated method stub
		return textBookOrderMapper.addTextBookOrder(textBookOrderList);
	}
	 /**
     * 添加班级-订单关系
     *  @param map(orderId,classId)
     * @return
     * @throws SQLException
     */
	@Override
	public int addOrderAndClass(List<Map<String,Object>> list) throws SQLException {
		// TODO Auto-generated method stub
		
		return textBookOrderMapper.addOrderAndClass(list);
	}
	  /**
     * 确认订单
     * @param textBookOrder
     * @return
     * @throws SQLException
     */
	@Override
	public int updateOrder(List<TextBookOrder> textBookOrderList) throws SQLException {
		// TODO Auto-generated method stub
		return textBookOrderMapper.updateOrder(textBookOrderList);
	}
	  /**
     * 修改班级-订单关系
     * @param classId
     * @return
     * @throws SQLException
     */
	@Override
	public int updateOrderAndClass(List<Map<String,Object>> list) throws SQLException {
		// TODO Auto-generated method stub
		return textBookOrderMapper.updateOrderAndClass(list);
	}
	 
    /**
     * 教务处提交订单更改状态码为2
     * @param map(orderId)
     * @return
     * @throws SQLException
     */
    public int updateOrderByJW(List<Map<String,Object>> list)throws SQLException{
    	// TODO Auto-generated method stub
		return textBookOrderMapper.updateOrderByJW(list);
    }
    /**
     * 统计订单数量map(majorName,grade,academicYear,term)
     * @param map
     * @return
     * @throws SQLException
     */
	@Override
	public int getOrderCountNum(Map<String, Object> map) throws SQLException {
		// TODO Auto-generated method stub
		return textBookOrderMapper.getOrderCountNum(map);
	}
	  /**
     * 教材名称及其id
     * @return
     * @throws SQLException
     */
	@Override
	public List<Map<String, Object>> getCourseNameAndId() throws SQLException {
		// TODO Auto-generated method stub
		return textBookOrderMapper.getCourseNameAndId();
	}

	@Override
	public int updateOrder2(TextBookOrder textBookOrder) throws SQLException {
		// TODO Auto-generated method stub
		return textBookOrderMapper.updateOrder2(textBookOrder);
	}

	@Override
	public int updateOrderAndClass2(Map<String, Object> map)
			throws SQLException {
		// TODO Auto-generated method stub
		return textBookOrderMapper.updateOrderAndClass2(map);
	}

	@Override
	public int delOrder(@Param("orderId")String orderId) throws SQLException {
		// TODO Auto-generated method stub
		return textBookOrderMapper.delOrder(orderId);
	}

	@Override
	public List<Map<String, Object>> getArrangeCourse(Map<String, Object> map)
			throws SQLException {
		// TODO Auto-generated method stub
		return textBookOrderMapper.getArrangeCourse(map);
	}

	@Override
	public int getCountCourse(Map<String, Object> map) throws SQLException {
		// TODO Auto-generated method stub
		return textBookOrderMapper.getCountCourse(map);
	}
	

}

package teaching.service.impl.textBookManage.textBookManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.textBookManage.textBookManage.TextBookManageMapper;
import teaching.service.textBookManage.textBookManage.TextBookManageService;

@Service
public class TextBookManageImpl  implements TextBookManageService{

	@Autowired
	private TextBookManageMapper textBookManageMapper;
	
	/**
	 * 查询教材信息map(start,pageSize,textbookId,courseNameCN,textbookName)
	 * @author GuoFei
	 * @return
	 * @throws SQLException
	 */
	@Override
	public List<Map<String, Object>> getTextBook2(Map<String,Object> map) throws SQLException {
		// TODO Auto-generated method stub
		return textBookManageMapper.getTextBook2(map);
	}

	/**
	 * 增加教材信息map(textbookId,textbookName,publishingHouse,author,isbn,price,teacherCourseId,remark1)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int addTextBook2(List<Map<String,Object>> list) throws SQLException {
		// TODO Auto-generated method stub
		return textBookManageMapper.addTextBook2(list);
	}

	/**
     * 增加教材信息界面的课程信息的初始化
     * @author GuoFei
     * @return
     * @throws SQLException
     */
	@Override
	public List<Map<String, Object>> getNameAndId2() throws SQLException {
		// TODO Auto-generated method stub
		return textBookManageMapper.getNameAndId2();
	}

	 /**
     * 修改教材信息map(oldTextbookId,textbookId,textbookName,publishingHouse,author,isbn,price,teacherCourseId,remark1)
     * @author GuoFei
     * @param map
     * @return
     * @throws SQLException
     */
	@Override
	public int updateTextBook2(Map<String, Object> map) throws SQLException {
		// TODO Auto-generated method stub
		return textBookManageMapper.updateTextBook2(map);
	}
	  /**
     * 删除教材信息
     * @author GuoFei
     * @param textbookId
     * @return
     * @throws SQLException
     */
	@Override
	public int deleteTextBook2(@Param("textbookId")String textbookId) throws SQLException {
		// TODO Auto-generated method stub
		return textBookManageMapper.deleteTextBook2(textbookId);
	}
	 /**
     * 统计教材数量map(courseNameCN,textbookName)
     * @param map
     * @return
     * @throws SQLException
     */
	@Override
	public int getTextBookCountNum(Map<String, Object> map) throws SQLException {
		// TODO Auto-generated method stub
		return textBookManageMapper.getTextBookCountNum(map);
	}
	 /**
     * 查询教材名称及其id
     * @return
     * @throws SQLException
     */
	@Override
	public List<Map<String, Object>> getTextBookNameAndId() throws SQLException {
		// TODO Auto-generated method stub
		return textBookManageMapper.getTextBookNameAndId();
	}

}

package teaching.service.impl.wym.commom;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.wym.commom.CommonMapper;
import teaching.service.wym.commom.CommonService;

@Service
public class CommomServiceImpl implements CommonService{

	@Autowired
	private CommonMapper commmonMapper;
	@Override
	public List<Map<String, String>> getAcademicYear() throws SQLException {
		return commmonMapper.getAcademicYear();
	}
	@Override
	public List<Map<String, String>> getMajorList() throws SQLException {
		return commmonMapper.getMajorList();
	}
	@Override
	public List<Map<String, String>> getScoringWay() throws SQLException {
		return commmonMapper.getScoringWay();
	}

	
}

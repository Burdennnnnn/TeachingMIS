package teaching.service.impl.trainingManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.entity.Course;
import teaching.mapper.trainingCourse.TrainingCourseMapper;
import teaching.service.trainingCourse.TrainingCourseService;

@Service
public class TrainingCourseServiceImpl implements TrainingCourseService{

	@Autowired
	private TrainingCourseMapper trainingCourseMapper;
	@Override
	public List<Course> getTrainCourseList(Map<String, String> condition) throws SQLException {
		return trainingCourseMapper.getTrainCourseList(condition);
	}

	@Override
	public boolean addCourse(Course course) throws SQLException {
		return trainingCourseMapper.addCourse(course)>0?true:false;
	}

	@Override
	public boolean updateCourse(Course course) throws SQLException {
		return trainingCourseMapper.updateCourse(course)>0?true:false;
	}

	@Override
	public boolean deleteCourse(String courseID) throws SQLException {
		return trainingCourseMapper.deleteCourse(courseID)>0?true:false;
	}

	@Override
	public Course getTrainCourseByID(String courseID) throws SQLException {
		return trainingCourseMapper.getTrainCourseByID(courseID);
	}

}

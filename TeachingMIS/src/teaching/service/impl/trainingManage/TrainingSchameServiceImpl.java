package teaching.service.impl.trainingManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.entity.TrainingSchema;
import teaching.mapper.trainingSchema.TrainingSchemaMapper;
import teaching.service.trainingSchema.TrainingSchameService;

@Service
public class TrainingSchameServiceImpl implements TrainingSchameService{

    @Autowired
    private TrainingSchemaMapper trainingSchemaMapper;
    @Override
    public boolean addSchema(TrainingSchema trainingSchema) throws SQLException {
        return trainingSchemaMapper.addSchema(trainingSchema) >0 ?true:false;
    }

    @Override
    public boolean updateSchema(TrainingSchema trainingSchema) throws SQLException {
        return trainingSchemaMapper.updateSchema(trainingSchema)>0?true:false;
    }

    @Override
    public boolean deleteSchema(String trainingSchemaID) throws SQLException {
        return trainingSchemaMapper.deleteSchema(trainingSchemaID)>0?true:false;
    }

    @Override
    public List<TrainingSchema> getSchemaList(Map<String,Object> condition) throws SQLException {
        return trainingSchemaMapper.getSchemaList(condition);
    }

    @Override
    public TrainingSchema getSchemaByID(String trainingSchemaID) throws SQLException {
        return trainingSchemaMapper.getSchemaByID(trainingSchemaID);
    }

	@Override
	public int getSchemaCount(Map<String,Object> condition) throws SQLException {
		return trainingSchemaMapper.getSchemaCount(condition);
	}

   

}

package teaching.service.impl.statisticsAnalysis.statistics;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




import teaching.mapper.designManage.graduateProgram.GraduatePlanMapper;
import teaching.mapper.statisticsAnalysis.statistics.StatisticsMapper;
import teaching.service.statisticsAnalysis.statistics.StatisticsService;

@Service
public class StatisticsImpl  implements StatisticsService{

	
	@Autowired
	private StatisticsMapper statisticsMapper;
	/**
	 * 通过教师id得到课程信息
	 * @param teacherId
	 * @return
	 * @throws SQLException
	 */
	@Override
	public List<Map<String, Object>> getCourse(String teacherId)
			throws SQLException {
		// TODO Auto-generated method stub
		return statisticsMapper.getCourse(teacherId);
	}
	 /**
     * 通过教师id得到课设信息
     * @param teacherId
     * @return
     * @throws SQLException
     */
	@Override
	public List<Map<String, Object>> getCourseDesign(String teacherId)
			throws SQLException {
		// TODO Auto-generated method stub
		return statisticsMapper.getCourseDesign(teacherId);
	}
	 /**
	    * 通过教师id得到毕设信息
	    * @param teacherId
	    * @return
	    * @throws SQLException
	    */
	@Override
	public List<Map<String, Object>> getGraduateProgram(String teacherId)
			throws SQLException {
		// TODO Auto-generated method stub
		return statisticsMapper.getGraduateProgram(teacherId);
	}
	/**
	 * 根据培养课程编号得到教师所教授课程
	 * @param trainingCourseID
	 * @return
	 * @throws SQLException
	 */
	@Override
	public List<Map<String, Object>> getCourseToClass(String trainingCourseID)
			throws SQLException {
		// TODO Auto-generated method stub
		return statisticsMapper.getCourseToClass(trainingCourseID);
	}


}

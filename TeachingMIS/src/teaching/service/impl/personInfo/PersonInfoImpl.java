package teaching.service.impl.personInfo;

import java.sql.SQLException;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.druid.filter.AutoLoad;

import teaching.mapper.personInfo.PersonInfoMapper;
import teaching.service.personInfoService.PersonInfoService;

@Service
public class PersonInfoImpl  implements PersonInfoService{
	
	@Autowired
	private PersonInfoMapper personInfoMapper;
	/**
	 * 得到个人信息
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	@Override
	public Map<String, Object> getPersonInfo(@Param("userId")String userId) throws SQLException {
		// TODO Auto-generated method stub
		return personInfoMapper.getPersonInfo(userId);
	}
	/**
	 * 修改密码map(userId,password)
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int updatePersonInfo(Map<String, Object> map)
			throws SQLException {
		// TODO Auto-generated method stub
		return personInfoMapper.updatePersonInfo(map);
	}

}

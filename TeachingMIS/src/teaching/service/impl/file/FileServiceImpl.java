package teaching.service.impl.file;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.entity.FileIndex;
import teaching.mapper.file.FileMapper;
import teaching.service.file.FileService;

@Service
public class FileServiceImpl implements FileService{

	@Autowired
	private FileMapper fileMapper;
	
	@Override
	public boolean deleteFile(String filedId) {
		return fileMapper.deleteFile(filedId)>0?true:false;
	}

	@Override
	public boolean addFile(FileIndex fileIndex) {
		return fileMapper.addFile(fileIndex)>0?true:false;
	}

	@Override
	public FileIndex getFileByID(String filedId) {
		return fileMapper.getFileByID(filedId);
	}

	@Override
	public boolean updateFile(FileIndex fileIndex) {
		return fileMapper.updateFile(fileIndex)>0?true:false;
	}

	

}

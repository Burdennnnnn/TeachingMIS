package teaching.service.impl.baseInfoManage.classBaseInfo;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.baseInfoManage.classBaseInfo.ClassBaseInfoMapper;
import teaching.service.baseInfoManage.classBaseInfo.ClassBaseInfoService;

@Service
public class ClassBaseInfoImpl implements ClassBaseInfoService{

	 @Autowired
	 private ClassBaseInfoMapper classBaseInfoMapper;
	/**
	 * 分页查询班级信息:共有四个条件Map(start+pageSize+majorName+enrollmentTime)
	 * 默认查询条件需设置(开始值start+pageSize);
	 * majorName专业名称和enrollmentTime入学年份可以为空
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	@Override
	public List<Map<String, Object>> getClass(Map<String, Object> map)
			throws SQLException {
		// TODO Auto-generated method stub
		return classBaseInfoMapper.getClass(map);
	}
	
	/**
	 * 增加班级信息map(classId,className,majorId,enrollmentTime,trainingSchemeId)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int addClass(List<Map<String,Object>> list) throws SQLException {
		// TODO Auto-generated method stub
		return classBaseInfoMapper.addClass(list);
	}

	/**
	 * 删除班级信息，级联删除学生和毕设结果
	 * @author GuoFei
	 * @param classId
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int deleteClass(@Param("classId")String classId) throws SQLException {
		// TODO Auto-generated method stub
		return classBaseInfoMapper.deleteClass(classId);
	}
	/**
	 * 修改班级信息map(oldClassId,classId,className,majorId,enrollmentTime,trainingSchemeId)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int updateClass(Map<String, Object> map) throws SQLException {
		// TODO Auto-generated method stub
		return classBaseInfoMapper.updateClass(map);
	}
	/**
	 * 得到班级总数量
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int getClassCountNum(Map<String,Object> map) throws SQLException {
		// TODO Auto-generated method stub
		return classBaseInfoMapper.getClassCountNum(map);
	}

}

package teaching.service.impl.baseInfoManage.teacherBaseInfo;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.baseInfoManage.teacherBaseInfo.TeacherBaseInfoMapper;
import teaching.service.baseInfoManage.teacherBaseInfo.TeacherBaseInfoService;

@Service
public class TeacherBaseInfoImpl implements TeacherBaseInfoService {

	@Autowired
	private TeacherBaseInfoMapper teacherBaseInfoMapper;
	/**
	 * 查询教师map(start,pageSize,teacherId,teacherName,teacherSex,degree,positionalTitle)
	 * @author GuoFei
	 * @return
	 * @throws SQLException
	 */
	@Override
	public List<Map<String, Object>> getTeacher(Map<String,Object> map) throws SQLException {
		// TODO Auto-generated method stub
		return teacherBaseInfoMapper.getTeacher(map);
	}
	/**
	 * 修改教师信息map(oldTeacherId,teacherId,teacherName,teacherSex,positionalTitle,
	 * teacherPosition,teacherTel,majorId,education,graduateSchool,teacherPhoto,
	 * joinSchoolTime,graduateMajor,degree,teacherBirth)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int updateTeacher(Map<String, Object> map) throws SQLException {
		// TODO Auto-generated method stub
		return teacherBaseInfoMapper.updateTeacher(map);
	}
	/**
	 * 增加教师，map(teacherId,teacherName,teacherSex,positionalTitle,
	 * teacherPosition,teacherTel,majorId,education,graduateSchool,teacherPhoto,
	 * joinSchoolTime,graduateMajor,degree,teacherBirth)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int addTeacher(List<Map<String,Object>> list) throws SQLException {
		// TODO Auto-generated method stub
		return teacherBaseInfoMapper.addTeacher(list);
	}
	
	/**
	 * 删除教师
	 * @author GuoFei
	 * @param teacherId
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int deleteTeacher(@Param("teacherId")String teacherId) throws SQLException {
		// TODO Auto-generated method stub
		return teacherBaseInfoMapper.deleteTeacher(teacherId);
	}
	/**
	 * 统计教师数量map(teacherName,teacherSex,degree,positionalTitle)
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int getTeacherCountNum(Map<String, Object> map) throws SQLException {
		// TODO Auto-generated method stub
		return teacherBaseInfoMapper.getTeacherCountNum(map);
	}

}

package teaching.service.impl.baseInfoManage.studentBaseInfo;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.baseInfoManage.studentBaseInfo.StudentBaseInfoMapper;
import teaching.service.baseInfoManage.studentBaseInfo.StudentBaseInfoService;

@Service
public class StudentBaseInfoImpl implements StudentBaseInfoService{

	@Autowired
	private StudentBaseInfoMapper studentBaseInfoMapper;
	
	/**
	 * 分页查询学生信息map(start,pageSize,className,studentId,studentName)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	@Override
	public List<Map<String, Object>> getStudent(Map<String, Object> map)
			throws SQLException {
		// TODO Auto-generated method stub
		return studentBaseInfoMapper.getStudent(map);
	}

	/**
	 * 添加素学生信息
	 * map(studentId,studentName,studentSex,studentBirth,classId,studentJob,bankCard,IdNum,studentPhoto,phone,studentEmail)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int addStudent(List<Map<String,Object>> list) throws SQLException {
		// TODO Auto-generated method stub
		return studentBaseInfoMapper.addStudent(list);
	}
	/**
	 * 删除学生信息
	 * @author GuoFei
	 * @param studentId
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int deleteStudent(@Param("studentId")String studentId) throws SQLException {
		// TODO Auto-generated method stub
		return studentBaseInfoMapper.deleteStudent(studentId);
	}
	/**
	 * 修改学生信息
	 * map(oldStudentId,studentId,studentName,studentSex,studentBirth,
	 * classId,studentJob,bankCard,IdNum,studentPhoto,phone,studentEmail)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int updateStudent(Map<String, Object> map) throws SQLException {
		// TODO Auto-generated method stub
		return studentBaseInfoMapper.updateStudent(map);
	}
	/**
	 * 统计学生总数量map(className,studentName)
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int getStudentCountNum(Map<String,Object> map) throws SQLException {
		// TODO Auto-generated method stub
		return studentBaseInfoMapper.getStudentCountNum(map);
	}

}

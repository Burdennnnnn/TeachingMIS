package teaching.service.impl.baseInfoManage.common;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.baseInfoManage.common.BaseInfoCommonMapper;
import teaching.service.baseInfoManage.common.BaseInfoCommonService;

@Service
public class BaseInfoCommonImpl implements BaseInfoCommonService{

	@Autowired
	private BaseInfoCommonMapper baseInfoCommonMapper;
	
	
	/**查询所有的专业名称+id
	 * @author GuoFei
	 * @return
	 * @throws SQLException
	 */
	@Override
	public List<Map<String, Object>> getAllMajorAndId() throws SQLException {
		// TODO Auto-generated method stub
		return baseInfoCommonMapper.getAllMajorAndId();
	}

	 /**
     * 查询所有培养方案+id
     * @author GuoFei
     * @return
     * @throws SQLException
     */
	@Override
	public List<Map<String, Object>> getAllTrainingScheme() throws SQLException {
		// TODO Auto-generated method stub
		return baseInfoCommonMapper.getAllTrainingScheme();
	}

	 /**
     * 查询班级名称+id
     * @author GuoFei
     * @return
     * @throws SQLException
     */
	@Override
	public List<Map<String, Object>> getClassNameAndId() throws SQLException {
		// TODO Auto-generated method stub
		return baseInfoCommonMapper.getClassNameAndId();
	}
	 /**
     * 根据名称查询班级id
     * @param className
     * @return
     * @throws SQLException
     */
	@Override
	public String getClassId(@Param("className")String className) throws SQLException {
		// TODO Auto-generated method stub
		return baseInfoCommonMapper.getClassId(className);
	}
	  
   
	 /**
	    * 根据教材名称查询教材id
	    * @param Map<String,Object> map
	    * @return
	    * @throws SQLException
	    */
	@Override
	public String getTextBookId(Map<String,Object> map) throws SQLException {
		// TODO Auto-generated method stub
		return baseInfoCommonMapper.getTextBookId(map);
	}
	   /**
     * 根据课程名称查询课程id
     * @param courseNameCN
     * @return
     * @throws SQLException
     */
	@Override
	public String getCourseId(@Param("courseNameCN")String courseNameCN) throws SQLException {
		// TODO Auto-generated method stub
		return baseInfoCommonMapper.getCourseId(courseNameCN);
	}
	 /**
     * 查出所有的学院
     * @return
     * @throws SQLException
     */
	@Override
	public List<Map<String, Object>> getXy() throws SQLException {
		// TODO Auto-generated method stub
		return baseInfoCommonMapper.getXy();
	}
	 /**
     * 按学院查询专业
     * @return
     * @throws SQLException
     */
	@Override
	public List<Map<String, Object>> getMajorByXy(@Param("collegeId")String collegeId)
			throws SQLException {
		// TODO Auto-generated method stub
		return baseInfoCommonMapper.getMajorByXy(collegeId);
	}
	 /**
     * 按专业查询入学份
     * @param majorId
     * @return
     * @throws SQLException
     */
	@Override
	public List<Map<String, Object>> getJoinTime(@Param("majorId")String majorId)
			throws SQLException {
		// TODO Auto-generated method stub
		return baseInfoCommonMapper.getJoinTime(majorId);
	}
	 /**
     * 按入学年份查询班级
     * @param enrollmentTime
     * @return
     * @throws SQLException
     */
	@Override
	public List<Map<String, Object>> getClassByJoinTime(Map<String,Object> map)
			throws SQLException {
		// TODO Auto-generated method stub
		return baseInfoCommonMapper.getClassByJoinTime(map);
	}
	  /**
     *  根据专业名称查询id 
     * @param majorName
     * @return
     * @throws SQLException
     */
	@Override
	public String getMajorIdByName(@Param("majorName")String majorName) throws SQLException {
		// TODO Auto-generated method stub
		return baseInfoCommonMapper.getMajorIdByName(majorName);
	}
	   /**
     * 根据培养方案名称查询id 
     * @param trainingSchemeStore
     * @return
     */
	@Override
	public String getTrainingSchemeIdByName(@Param("trainingSchemeStore")String trainingSchemeStore) {
		// TODO Auto-generated method stub
		return baseInfoCommonMapper.getTrainingSchemeIdByName(trainingSchemeStore);
	}

	@Override
	public String getCollegeIdByName(@Param("collegeName")String collegeName)
			throws SQLException {
		// TODO Auto-generated method stub
		return baseInfoCommonMapper.getCollegeIdByName(collegeName);
	}

	
 
}

package teaching.service.impl.baseInfoManage.majorBaseInfo;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.entity.Major;
import teaching.mapper.baseInfoManage.majorBaseInfo.MajorBaseInfoMapper;
import teaching.service.baseInfoManage.majorBaseInfo.MajorBaseInfoService;

@Service
public class MajorBaseInfoServiceImpl implements MajorBaseInfoService{
    
	@Autowired
	private MajorBaseInfoMapper majorBaseInfoMapper;
	
	/**
	 * 添加专业信息
	 * @author GuoFei
	 * @param major
	 * @return
	 * @throws SQLException
	 */
	@Override
	public int addMajor(List<Major> list) throws SQLException {
		// TODO Auto-generated method stub
		if(list!=null){
			return majorBaseInfoMapper.addMajor(list);
		}else{
			return 0;
		}
		
	}
    
	 /**
     * 修改专业信息(根据专业id修改)
     * @author GuoFei
     * @param map
     * @return
     * @throws SQLException
     */
	@Override
	public int updateMajor(Map<String, Object> map) throws SQLException {
		// TODO Auto-generated method stub
		if(map!=null){
			return majorBaseInfoMapper.updateMajor(map);
		}else{
			return 0;
		}
		
	}
	 /**
     * 分页查询专业(条件:pageSize+page)
     * @author GuoFei
     * @param map
     * @return
     * @throws SQLException
     */
	@Override
	public List<Major> getMajor(Map<String, Object> map) throws SQLException {
		// TODO Auto-generated method stub
		if(map!=null){
			return majorBaseInfoMapper.getMajor(map);
		}else{
			return null;
		}
	}

	 /**
     * 根据专业id删除专业+班级+学生+毕设结果
     * @author GuoFei
     * @param majorId
     * @return
     * @throws SQLException
     */
	@Override
	public int deleteMajor(String majorId) throws SQLException {
		// TODO Auto-generated method stub
		if(majorId!=null && !("").equals(majorId) && !("null").equals(majorId)){		
			return majorBaseInfoMapper.deleteMajor(majorId);			
		}
	    return 0;
		
		
	}
	  /**
     * 统计专业数量
     * @return
     * @throws SQLException
     */
	@Override
	public int getMajorCountNum() throws SQLException {
		// TODO Auto-generated method stub
		return majorBaseInfoMapper.getMajorCountNum();
	}
	
	
	

}

package teaching.service.impl.SystemManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.SystemManage.MenuMapper;
import teaching.service.SystemManage.MenuService;

@Service
public class MenuServiceImpl implements MenuService {

	@Autowired
	private MenuMapper menuMapper;
	
	@Override
	public List<Map<String, Object>> getFIrstMenu(String userID) throws SQLException {
		return menuMapper.getFIrstMenu(userID);
	}
	
	@Override
	public List<Map<String, Object>> getChildMenu(Map<String, Object> map) throws SQLException {
		return menuMapper.getChildMenu(map);
	}
	
}

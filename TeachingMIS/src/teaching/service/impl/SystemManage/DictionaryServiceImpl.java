package teaching.service.impl.SystemManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.SystemManage.DictionaryMapper;
import teaching.service.SystemManage.DictionaryService;

@Service
public class DictionaryServiceImpl implements DictionaryService{

	@Autowired
	private DictionaryMapper dictionaryMapper;
	
	@Override
	public List<Map<String, Object>> getOneDictionary() throws SQLException {
		return dictionaryMapper.getOneDictionary();
	}

	@Override
	public List<Map<String, Object>> getDictionnaryByUpId(Map<String, String> upId) throws SQLException {
		return dictionaryMapper.getDictionnaryByUpId(upId);
	}

	@Override
	public List<Map<String, Object>> getOneForTree() throws SQLException {
		return dictionaryMapper.getOneForTree();
	}

	@Override
	public List<Map<String, Object>> getTwoForTree(String upId) throws SQLException {
		return dictionaryMapper.getTwoForTree(upId);
	}

	@Override
	public boolean addDictionary(Map<String, String> dictionary) throws SQLException {
		return dictionaryMapper.addDictionary(dictionary)>0?true:false;
	}

	@Override
	public boolean updateDictionary(Map<String, String> dictionary) throws SQLException {
		return dictionaryMapper.updateDictionary(dictionary)>0?true:false;
	}

	@Override
	public boolean deleteDictionary(String dictionaryID) throws SQLException {
		return dictionaryMapper.deleteDictionary(dictionaryID)>0?true:false;
	}

	@Override
	public Map<String, String> getDictionary(String dictionaryID) throws SQLException {
		return dictionaryMapper.getDictionry(dictionaryID);
	}

}

package teaching.service.impl.SystemManage;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.entity.College;
import teaching.mapper.SystemManage.CollegeMapper;
import teaching.service.SystemManage.CollegeService;

@Service
public class CollegeServiceImpl implements CollegeService{

	@Autowired
	private CollegeMapper collegeMapper;
	@Override
	public int insert(College college) throws SQLException {
		return collegeMapper.insert(college);
	}

	@Override
	public int update(College college) throws SQLException {
		return collegeMapper.update(college);
	}

	@Override
	public College getCollege() throws SQLException {
		return collegeMapper.getCollege();
	}

}

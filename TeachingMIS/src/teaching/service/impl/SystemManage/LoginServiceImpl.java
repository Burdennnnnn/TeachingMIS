package teaching.service.impl.SystemManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.SystemManage.LoginMapper;
import teaching.service.SystemManage.LoginService;

@Service
public class LoginServiceImpl implements LoginService{

	@Autowired
	private LoginMapper loginMapper;
	@Override
	public String login(Map<String, String> login) throws SQLException {
		return loginMapper.login(login);
	}

	@Override
	public List<String> getRoles(Map<String, String> userID) throws SQLException {
		return loginMapper.getRoles(userID);
	}

	@Override
	public List<Map<String, String>> getOneMenu(Map<String, String> condition) throws SQLException {
		return loginMapper.getOneMenu(condition);
	}

	@Override
	public List<Map<String, String>> getTwoMenu(Map<String, String> condition) throws SQLException {
		return loginMapper.getTwoMenu(condition);
	}

	
}

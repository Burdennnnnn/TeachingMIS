package teaching.service.impl.SystemManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.SystemManage.RoleMapper;
import teaching.service.SystemManage.RoleService;

@Service
public class RoleServiceImpl implements RoleService{

	@Autowired
	private RoleMapper roleMapper;
	@Override
	public List<Map<String, Object>> getRole() throws SQLException {
		return roleMapper.getRole();
	}
	
	@Override
	public Map<String, Object> getOneRole(String roleID) throws SQLException {
		return roleMapper.getOneRole(roleID);
	}
	
	@Override
	public int addRole(Map<String,Object> map) throws SQLException {
		return roleMapper.addRole(map);
	}
	
	@Override
	public int updateRole(Map<String,Object> map) throws SQLException {
		return roleMapper.updateRole(map);
	}
	
	@Override
	public int deleteRole(String roleID) throws SQLException {
		return  roleMapper.deleteRole(roleID);
	}
	
	@Override
	public List<Map<String, Object>> getFirstQX() throws SQLException {
		return roleMapper.getFirstQX();
	}
	
	@Override
	public List<Map<String, Object>> getChildQX(String moduleID) throws SQLException {
		return roleMapper.getChildQX(moduleID);
	}	
	
	@Override
	public List<Map<String, Object>> getRoleQXF(String roleID) throws SQLException {
		return roleMapper.getRoleQXF(roleID);
	}
	
	@Override
	public List<Map<String, Object>> getRoleQXC(String roleID) throws SQLException {
		return roleMapper.getRoleQXC(roleID);
	}
	
	@Override
	public int deleteRoleQX(String roleID) throws SQLException {
		return roleMapper.deleteRoleQX(roleID);
	}
	
	@Override
	public int addRoleQX(List<Map<String, Object>> list) throws SQLException {
		return roleMapper.addRoleQX(list);
	}
}

package teaching.service.impl.SystemManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.SystemManage.UserManageMapper;
import teaching.service.SystemManage.StudentManageService;

@Service
public class StudentManageServiceImpl implements StudentManageService{

	@Autowired
	private UserManageMapper userManageMapper;
	@Override
	public boolean addUser(Map<String, String> userInfo) throws SQLException {
		return userManageMapper.addUser(userInfo)>0?true:false;
	}

	@Override
	public boolean deleteUser(String userID) throws SQLException {
		return userManageMapper.deleteUser(userID)>0?true:false;
	}

	@Override
	public List<String> getStudentIds(Map<String, String> condition) throws SQLException {
		return userManageMapper.getStudentIds(condition);
	}

	@Override
	public void addUserRole(Map<String, String> userInfo) throws SQLException {
		userManageMapper.addUserRole(userInfo);
	}

	
}

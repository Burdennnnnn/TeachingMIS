package teaching.service.impl.wexin;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.weixin.NewsMapper;
import teaching.service.wexin.NewsService;

@Service
public class NewsServiceImpl implements NewsService{

	@Autowired
	private NewsMapper newsMapper;
	
	@Override
	public List<Map<String, String>> getNewsList() throws SQLException {
		return newsMapper.getNewsList();
	}

	@Override
	public boolean addNews(Map<String, String> news) throws SQLException {
		return newsMapper.addNews(news)>0?true:false;
	}

	@Override
	public Map<String, String> getNewsById(Map<String, String> newId) throws SQLException {
		return newsMapper.getNewsById(newId);
	}

	@Override
	public boolean deleteNews(Map<String, String> newId) throws SQLException {
		return newsMapper.deleteNews(newId)>0?true:false;
	}

	@Override
	public List<Map<String, String>> getReviewByNewsId(Map<String, String> newId) throws SQLException {
		return newsMapper.getReviewByNewsId(newId);
	}

	@Override
	public boolean addReview(Map<String, String> review) throws SQLException {
		return newsMapper.addReview(review)>0?true:false;
	}

}

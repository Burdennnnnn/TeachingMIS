package teaching.service.impl.wexin;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.weixin.CheckMapper;
import teaching.service.wexin.CheckService;

@Service
public class CheckServiceImpl implements CheckService{

	@Autowired
	private CheckMapper checkMapper;
	
	@Override
	public String wxlogin(Map<String, String> loginInfo) throws SQLException {
		return checkMapper.wxlogin(loginInfo);
	}

	@Override
	public List<Map<String, String>> wxGetRoles(Map<String, String> userId) throws SQLException {
		return checkMapper.wxGetRoles(userId);
	}

	@Override
	public Map<String, String> wxGetUinfos(Map<String, String> userId) throws SQLException {
		return checkMapper.wxGetUinfos(userId);
	}

	@Override
	public int wxUpdatePassword(Map<String, String> userPs) throws SQLException {
		return checkMapper.wxUpdatePassword(userPs);
	}

	@Override
	public int wxUpdateUserInfo(Map<String, String> userInfo) throws SQLException {
		return checkMapper.wxUpdateUserInfo(userInfo);
	}

}

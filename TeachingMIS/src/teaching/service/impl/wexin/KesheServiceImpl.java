package teaching.service.impl.wexin;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.weixin.KesheMapper;
import teaching.service.wexin.KesheService;

@Service
public class KesheServiceImpl implements KesheService{

	@Autowired
	private KesheMapper kesheMapper;

	@Override
	public List<Map<String, String>> getCourseDesign(Map<String, String> condition) throws SQLException {
		return kesheMapper.getCourseDesign(condition);
	}

	@Override
	public Map<String, String> getDesignBaseInfo(Map<String, String> kesheId) throws SQLException {
		return kesheMapper.getDesignBaseInfo(kesheId);
	}

}

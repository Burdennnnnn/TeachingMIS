package teaching.service.impl.designMange.courseDesign;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.designManage.courseDesign.CourseDesignMapper;
import teaching.service.designManage.courseDesign.CourseDesignService;

/**
*                  张哲
*  创建时间：2017年4月9日 下午8:01:27
*/
@Service
public class CourseDesignImpl implements CourseDesignService{
	@Autowired
	private CourseDesignMapper courseDesignMapper;
	
	@Override
	public List<Map<String, Object>> getDesignBaseInfo(Map<String,Object> map) throws SQLException {
		return courseDesignMapper.getDesignBaseInfo(map);
	}
	
	@Override
	public Map<String, Object> getDesignBaseInfoNum(Map<String,Object> map) throws SQLException {
		return courseDesignMapper.getDesignBaseInfoNum(map);
	}
	
	@Override
	public List<Map<String, Object>> getTeacherDesign() throws SQLException {
		return courseDesignMapper.getTeacherDesign();
	}
	
	@Override
	public List<Map<String, Object>> getAllCourseWithMajor(String majorID) throws SQLException {
		return courseDesignMapper.getAllCourseWithMajor(majorID);
	}
	
	@Override
	public List<Map<String, Object>> getClassDesign(String majorID) throws SQLException {
		return courseDesignMapper.getClassDesign(majorID);
	}
	
	@Override
	public int deleteDesignOne(String courseDesignID) throws SQLException {
		return courseDesignMapper.deleteDesignOne(courseDesignID);
	}
	
	@Override
	public int deleteDesignBatch(List<String> lists) throws SQLException {
		return courseDesignMapper.deleteDesignBatch(lists);
	}
	
	@Override
	public int insertDesignInfo(Map<String,Object> map) throws SQLException {
		return courseDesignMapper.insertDesignInfo(map);
	}
	
	@Override
	public int insertDesignTeacherClass(Map<String, Object> map) throws SQLException {
		return courseDesignMapper.insertDesignTeacherClass(map);
	}

	@Override
	public List<Map<String, Object>> getMajorDesign() throws SQLException {
		return courseDesignMapper.getMajorDesign();
	}
	
	@Override
	public List<Map<String, Object>> getCourseInfoDesign(Map<String, Object> map) throws SQLException {
		return courseDesignMapper.getCourseInfoDesign(map);
	}
	
	@Override
	public Map<String, Object> getOneDesignInfo(String courseDesignID) throws SQLException {
		return courseDesignMapper.getOneDesignInfo(courseDesignID);
	}
	
	@Override
	public int updateDesignInfo(Map<String, Object> map) throws SQLException {
		return courseDesignMapper.updateDesignInfo(map);
	}
	
	@Override
	public int updateDesignTeachetClass(Map<String, Object> map) throws SQLException {
		return courseDesignMapper.updateDesignTeachetClass(map);
	}
	
	@Override
	public List<Map<String, Object>> getCourseWithDesign() throws SQLException {
		return courseDesignMapper.getCourseWithDesign();
	}
	
	@Override
	public List<Map<String, Object>> getCourseWithOutDesign() throws SQLException {
		return courseDesignMapper.getCourseWithOutDesign();
	}
	
	@Override
	public List<Map<String, Object>> getCourseDesign(Map<String, Object> map) throws SQLException {
		return courseDesignMapper.getCourseDesign(map);
	}
	
	@Override
	public Map<String, Object> getCourseDesignNum(Map<String, Object> map) throws SQLException {
		return courseDesignMapper.getCourseDesignNum(map);
	}
	
	@Override
	public Map<String, Object> getCourseWithID(Map<String,Object> map) throws SQLException {
		return courseDesignMapper.getCourseWithID(map);
	}
	
	@Override
	public String getTeacherByTeacherID(String teacherID) throws SQLException {
		return courseDesignMapper.getTeacherByTeacherID(teacherID);
	}
	
	@Override
	public List<Map<String, Object>> getCourseDesignT(Map<String, Object> map) throws SQLException {
		return courseDesignMapper.getCourseDesignT(map);
	}
	
	@Override
	public Map<String, Object> getCourseDesignTN(Map<String, Object> map) throws SQLException {
		return courseDesignMapper.getCourseDesignTN(map);
	}
	
}

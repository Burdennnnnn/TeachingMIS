package teaching.service.impl.designMange.graduteProgram;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.designManage.graduateProgram.GraduateProgramMapper;
import teaching.service.designManage.graduateProgram.GraduateProgramService;

/**
 * 寮犲摬 鍒涘缓鏃堕棿锛�2017骞�4鏈�14鏃� 涓嬪崍5:23:01
 */
@Service
public class GraduateProgramImpl implements GraduateProgramService {

	@Autowired
	private GraduateProgramMapper graduateProgramMapper;

	@Override
	public int addGraduateProgram(Map<String, Object> map) throws SQLException {
		return graduateProgramMapper.addGraduateProgram(map);
	}

	@Override
	public List<Map<String, Object>> getGraduateProgram(Map<String,Object> map) throws SQLException {
		return graduateProgramMapper.getGraduateProgram(map);
	}
	
	@Override
	public Map<String, Object> getGraduateProgramNum(Map<String, Object> map) throws SQLException {
		return graduateProgramMapper.getGraduateProgramNum(map);
	}

	@Override
	public List<Map<String, Object>> getGraduateProgramStudent(Map<String,Object> map) throws SQLException {
		return graduateProgramMapper.getGraduateProgramStudent(map);
	}
	
	@Override
	public List<Map<String, Object>> getGraduateInStudent(String studentID) throws SQLException {
		return graduateProgramMapper.getGraduateInStudent(studentID);
	}

	@Override
	public int addStudentGraduate(Map<String, Object> map) throws SQLException {
		return graduateProgramMapper.addStudentGraduate(map);
	}

	@Override
	public int deleteStudentGraduate(String studentID) throws SQLException {
		return graduateProgramMapper.deleteStudentGraduate(studentID);
	}
	
	@Override
	public List<Map<String, Object>> getStudentNumByTeacher(Map<String,Object> map ) throws SQLException {
		return graduateProgramMapper.getStudentNumByTeacher(map);
	}

	@Override
	public Map<String, Object> getStudentByTeacherNum(Map<String, Object> map) throws SQLException {
		return graduateProgramMapper.getStudentByTeacherNum(map);
	}
	
	@Override
	public List<Map<String, Object>> getStudentNum(Map<String,Object> map) throws SQLException {
		return graduateProgramMapper.getStudentNum(map);
	}

	@Override
	public Map<String, Object> getStudentTotalNum(Map<String, Object> map) throws SQLException {
		return graduateProgramMapper.getStudentTotalNum(map);
	}
	
	@Override
	public int teacherChooseStudentDelete(Map<Object, Object> map) throws SQLException {
		return graduateProgramMapper.teacherChooseStudentDelete(map);
	}

	@Override
	public int teacherChooseStudentAdd(Map<Object, Object> map) throws SQLException {
		return graduateProgramMapper.teacherChooseStudentAdd(map);
	}

	@Override
	public int teacherChooseStudent(Map<Object, Object> map) throws SQLException {
		return graduateProgramMapper.teacherChooseStudent(map);
	}

	@Override
	public List<Map<String, Object>> getGraduateInfoStudent(String studentID) throws SQLException {
		return graduateProgramMapper.getGraduateInfoStudent(studentID);
	}

	@Override
	public List<Map<String, Object>> getStudentNOBelong(Map<String,Object> map) throws SQLException {
		return graduateProgramMapper.getStudentNOBelong(map);
	}

	@Override
	public List<Map<String, Object>> getStudentInGraduate(String graduateProgramID) throws SQLException {
		return graduateProgramMapper.getStudentInGraduate(graduateProgramID);
	}
	
	@Override
	public List<Map<String, Object>> getStudentInfo(String graduateProgramID) throws SQLException {
		return graduateProgramMapper.getStudentInfo(graduateProgramID);
	}
	
	@Override
	public List<Map<String, Object>> getGraduateInfoTeacher(Map<String, Object> map) throws SQLException {
		return graduateProgramMapper.getGraduateInfoTeacher(map);
	}
	
	@Override
	public Map<String, Object> getGraduateInfoTeacherNum(Map<String, Object> map) throws SQLException {
		return graduateProgramMapper.getGraduateInfoTeacherNum(map);
	}
	
	@Override
	public List<Map<String, Object>> getGraduateInfoStaff(Map<String, Object> map) throws SQLException {
		return graduateProgramMapper.getGraduateInfoStaff(map);
	}
	
	@Override
	public Map<String, Object> getGraduateInfoStaffNum(Map<String, Object> map) throws SQLException {
		return graduateProgramMapper.getGraduateInfoStaffNum(map);
	}
	
	@Override
	public Map<String, Object> getPlanTimeWithID(String graduateProPlanID) throws SQLException {
		return graduateProgramMapper.getPlanTimeWithID(graduateProPlanID);
	}
	
	//===============================================
	@Override
	public List<Map<String, Object>> getOneGraduateProgram(Map<String, Object> map) throws SQLException {
		return graduateProgramMapper.getOneGraduateProgram(map);
	}
	
	@Override
	public List<Map<String,Object>> exportGraduateInfoStaff(Map<String, Object> map) throws SQLException {
		return graduateProgramMapper.exportGraduateInfoStaff(map);
	}
	
}

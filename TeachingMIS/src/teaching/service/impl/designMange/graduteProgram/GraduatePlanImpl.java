package teaching.service.impl.designMange.graduteProgram;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.designManage.graduateProgram.GraduatePlanMapper;
import teaching.service.designManage.graduateProgram.GraduatePlanService;

/**
*                  张哲
*  创建时间：2017年4月14日 下午5:19:36
*/
@Service
public class GraduatePlanImpl implements GraduatePlanService {

	@Autowired
	private GraduatePlanMapper graduatePlanMapper;
	
	@Override
	public int addGraduatePlan(Map<String, Object>map) throws SQLException {
		return graduatePlanMapper.addGraduatePlan(map);
	}
	
	@Override
	public List<Map<String, Object>> getGraduatePlan(Map<String, Object> map) throws SQLException {
		return graduatePlanMapper.getGraduatePlan(map);
	}
	
	@Override
	public Map<String, Object> getGraduatePlanNum(Map<String, Object> map) throws SQLException {
		return graduatePlanMapper.getGraduatePlanNum(map);
	}
	
	@Override
	public List<Map<String, Object>> getMajorInPlan() throws SQLException {
		return graduatePlanMapper.getMajorInPlan();
	}
	
	@Override
	public int deleteGraduatePlan(String graduateProPlanID) throws SQLException {
		return graduatePlanMapper.deleteGraduatePlan(graduateProPlanID);
	}
	
	@Override
	public int updateGraduatePlan(Map<String, Object> map) throws SQLException {
		return graduatePlanMapper.updateGraduatePlan(map);
	}
	
	@Override
	public Map<String, Object> getPlanInfoWithID(String graduateProPlanID) throws SQLException {
		return graduatePlanMapper.getPlanInfoWithID(graduateProPlanID);
	}
}

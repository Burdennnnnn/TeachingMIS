package teaching.service.impl.designMange.graduteProgram;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teaching.mapper.designManage.graduateProgram.GraduateResultMapper;
import teaching.service.designManage.graduateProgram.GraduateResultService;

/**
*            
*/
@Service
public class GraduteResultImpl implements GraduateResultService {

	@Autowired
	private GraduateResultMapper graduateResultMapper;
	
	@Override
	public Map<String, Object> getStudentGraduate(String studentID) throws SQLException {
		return graduateResultMapper.getStudentGraduate(studentID);
	}
	
	@Override
	public int addGraduateResult(Map<String, Object> map) throws SQLException {
		return graduateResultMapper.addGraduateResult(map);
	}
	
	@Override
	public List<Map<String, Object>> getGraduateResult(Map<String, Object> map) throws SQLException {
		return graduateResultMapper.getGraduateResult(map);
	}
	
	@Override
	public Map<String, Object> getGraduateResultNum(Map<String, Object> map) throws SQLException {
		return graduateResultMapper.getGraduateResultNum(map);
	}
	
	@Override
	public Map<String, Object> getCheck(String studentID) throws SQLException {
		return graduateResultMapper.getCheck(studentID);
	}
	
	@Override
	public int updateGraduateResult(Map<String, Object> map) throws SQLException {
		return graduateResultMapper.updateGraduateResult(map);
	}
	
	@Override
	public int updateCheck(String studentID) throws SQLException {
		return graduateResultMapper.updateCheck(studentID);
	}
}

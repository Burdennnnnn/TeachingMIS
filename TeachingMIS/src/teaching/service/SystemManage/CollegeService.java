package teaching.service.SystemManage;

import java.sql.SQLException;

import teaching.entity.College;

public interface CollegeService {

	int insert(College college) throws SQLException;
	int update(College college) throws SQLException;
	College getCollege() throws SQLException;
}

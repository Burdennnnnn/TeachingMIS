package teaching.service.SystemManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface StudentManageService {

	boolean addUser(Map<String,String> userInfo)throws SQLException;
	boolean deleteUser(String userID)throws SQLException;
	List<String> getStudentIds(Map<String,String> condition)throws SQLException;
	void addUserRole(Map<String, String> userInfo)throws SQLException;
}

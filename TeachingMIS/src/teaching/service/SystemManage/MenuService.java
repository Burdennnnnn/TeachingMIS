package teaching.service.SystemManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface MenuService {

	public List<Map<String,Object>>getFIrstMenu(@Param("userID")String userID)throws SQLException;
	
	public List<Map<String,Object>> getChildMenu(Map<String,Object> map) throws SQLException;
}

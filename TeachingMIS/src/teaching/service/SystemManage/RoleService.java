package teaching.service.SystemManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface RoleService {

	public List<Map<String,Object>> getRole()throws SQLException;
	
	public Map<String,Object>getOneRole(@Param("roleID")String roleID) throws SQLException;
	
	public int addRole(Map<String,Object> map) throws SQLException;
	
	public int updateRole(Map<String,Object> map) throws SQLException;
	
	public int deleteRole(@Param("roleID")String roleID) throws SQLException;
	
	public List<Map<String,Object>> getFirstQX() throws SQLException;
	
	public List<Map<String,Object>> getChildQX(@Param("moduleID")String moduleID)throws SQLException;
	
	public List<Map<String,Object>> getRoleQXF(@Param("roleID")String roleID)throws SQLException;
	
	public List<Map<String,Object>> getRoleQXC(@Param("roleID")String roleID)throws SQLException;
	
	public int deleteRoleQX(@Param("roleID")String roleID) throws SQLException;
	
	public int addRoleQX(List<Map<String,Object>> list) throws SQLException;
}

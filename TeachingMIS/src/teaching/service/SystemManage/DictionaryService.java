package teaching.service.SystemManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface DictionaryService {
	List<Map<String,Object>> getOneDictionary()throws SQLException;
	List<Map<String,Object>> getDictionnaryByUpId(Map<String,String> upId)throws SQLException;
	List<Map<String,Object>> getOneForTree()throws SQLException;
	List<Map<String,Object>> getTwoForTree(String upId)throws SQLException;
	boolean addDictionary(Map<String,String> dictionary)throws SQLException;
	boolean updateDictionary(Map<String,String> dictionary)throws SQLException;
	boolean deleteDictionary(String dictionaryID)throws SQLException;
	Map<String,String> getDictionary(String dictionaryID)throws SQLException;
}

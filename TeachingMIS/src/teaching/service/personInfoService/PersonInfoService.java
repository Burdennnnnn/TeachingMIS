package teaching.service.personInfoService;

import java.sql.SQLException;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface PersonInfoService {
	
	/**
	 * 得到个人信息
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public Map<String,Object> getPersonInfo(@Param("userId")String userId)throws SQLException;
	/**
	 * 修改密码map(userId,password)
	 * @return
	 * @throws SQLException
	 */
	public int updatePersonInfo(Map<String,Object> map)throws SQLException;

}

package teaching.service.wexin;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface CheckService {

	String wxlogin(Map<String,String> loginInfo) throws SQLException;
	List<Map<String,String>> wxGetRoles(Map<String,String> userId)throws SQLException;
	Map<String,String> wxGetUinfos(Map<String,String> userId)throws SQLException;
	int wxUpdatePassword(Map<String,String> userPs)throws SQLException;
	int wxUpdateUserInfo(Map<String,String> userInfo)throws SQLException;
}

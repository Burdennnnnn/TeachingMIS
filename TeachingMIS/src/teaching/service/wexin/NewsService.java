package teaching.service.wexin;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface NewsService {

	List<Map<String,String>> getNewsList()throws SQLException;
	boolean addNews(Map<String,String> news)throws SQLException;
	Map<String,String> getNewsById(Map<String,String> newId)throws SQLException;
	boolean deleteNews(Map<String,String> newId)throws SQLException;
	List<Map<String,String>> getReviewByNewsId(Map<String,String> newId)throws SQLException;
	boolean addReview(Map<String,String> review)throws SQLException;
}

package teaching.service.baseInfoManage.classBaseInfo;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface ClassBaseInfoService {
	/**
	 * 分页查询班级信息:共有四个条件Map(start+pageSize+majorName+enrollmentTime)
	 * 默认查询条件需设置(开始值start+pageSize);
	 * majorName专业名称和enrollmentTime入学年份可以为空
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String,Object>> getClass(Map<String,Object> map)throws SQLException;
	/**
	 * 增加班级信息map(classId,className,majorId,enrollmentTime,trainingSchemeId)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public int addClass(List<Map<String,Object>> list)throws SQLException;
	
	/**
	 * 删除班级信息，级联删除学生和毕设结果
	 * @author GuoFei
	 * @param classId
	 * @return
	 * @throws SQLException
	 */
	public int deleteClass(@Param("classId")String classId)throws SQLException;
	/**
	 * 修改班级信息map(oldClassId,classId,className,majorId,enrollmentTime,trainingSchemeId)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public int updateClass(Map<String,Object> map)throws SQLException;
	/**
	 * 得到班级总数量
	 * @return
	 * @throws SQLException
	 */
	public int getClassCountNum(Map<String,Object> map)throws SQLException;
}

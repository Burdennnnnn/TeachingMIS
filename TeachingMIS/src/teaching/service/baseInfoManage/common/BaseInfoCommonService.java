package teaching.service.baseInfoManage.common;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface BaseInfoCommonService {

	/**查询所有的专业名称+id
	 * @author GuoFei
	 * @return
	 * @throws SQLException
	 */
    public List<Map<String,Object>> getAllMajorAndId()throws SQLException;
    /**
     * 查询所有培养方案+id
     * @author GuoFei
     * @return
     * @throws SQLException
     */
    public List<Map<String,Object>> getAllTrainingScheme()throws SQLException;
    
    /**
     * 查询班级名称+id
     * @author GuoFei
     * @return
     * @throws SQLException
     */
    public List<Map<String,Object>> getClassNameAndId()throws SQLException;
    /**
     * 根据名称查询班级id
     * @param className
     * @return
     * @throws SQLException
     */
    public String  getClassId(@Param("className")String className)throws SQLException;
   
    /**
     * 根据教材名称查询教材id
     * @param Map<String,Object> map
     * @return
     * @throws SQLException
     */
     public String getTextBookId(Map<String,Object> map)throws SQLException;
     
     /**
      * 根据课程名称查询课程id
      * @param courseNameCN
      * @return
      * @throws SQLException
      */
     public String  getCourseId(@Param("courseNameCN")String courseNameCN)throws SQLException;
     /**
      * 查出所有的学院
      * @return
      * @throws SQLException
      */
     public List<Map<String,Object>> getXy()throws SQLException;
    
     /**
      * 按学院查询专业
      * @return
      * @throws SQLException
      */
     public List<Map<String,Object>> getMajorByXy(@Param("collegeId")String collegeId)throws SQLException;
     /**
      * 按专业查询入学份
      * @param majorId
      * @return
      * @throws SQLException
      */
     public List<Map<String,Object>> getJoinTime(@Param("majorId")String majorId)throws SQLException;
     /**
      * 按入学年份查询班级
      * @param enrollmentTime
      * @return
      * @throws SQLException
      */
     public List<Map<String,Object>> getClassByJoinTime(Map<String,Object> map)throws SQLException;

     /**
      *  根据专业名称查询id 
      * @param majorName
      * @return
      * @throws SQLException
      */
     public String getMajorIdByName(@Param("majorName")String majorName)throws SQLException;
    
     /**
      * 根据培养方案名称查询id 
      * @param trainingSchemeStore
      * @return
      */
     public String getTrainingSchemeIdByName(@Param("trainingSchemeStore")String trainingSchemeStore)throws SQLException;
     /**
      * 根据学院名称查询学院id
      * @param map
      * @return
      * @throws SQLException
      */
      public String getCollegeIdByName(@Param("collegeName")String collegeName)throws SQLException;
	 		
 

}

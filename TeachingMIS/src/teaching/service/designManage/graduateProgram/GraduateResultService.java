package teaching.service.designManage.graduateProgram;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
*                  寮犲摬
*  鍒涘缓鏃堕棿锛�2017骞�4鏈�14鏃� 涓嬪崍5:24:24
*/
public interface GraduateResultService {

	public Map<String,Object> getStudentGraduate(@Param("studentID")String studentID)throws SQLException;
	
	public int addGraduateResult (Map<String,Object>map) throws SQLException;
	
	public List<Map<String,Object>>getGraduateResult (Map<String,Object>map) throws SQLException;
	
	public Map<String,Object>getGraduateResultNum (Map<String,Object>map) throws SQLException;
	
	public Map<String,Object>getCheck (@Param("studentID")String studentID) throws SQLException;
	
	public int updateGraduateResult (Map<String,Object>map) throws SQLException;
	
	public int updateCheck (@Param("studentID")String studentID) throws SQLException;
	
}

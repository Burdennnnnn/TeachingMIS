package teaching.service.designManage.graduateProgram;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
*                  张哲
*  创建时间：2017年4月14日 下午5:19:20
*/
public interface GraduatePlanService {

	public int addGraduatePlan(Map<String,Object>map) throws SQLException;
	
	public List<Map<String,Object>> getGraduatePlan(Map<String,Object> map)throws SQLException;
	
	public Map<String,Object>getGraduatePlanNum(Map<String,Object> map)throws SQLException; 
	
	public List<Map<String,Object>> getMajorInPlan() throws SQLException; 
	
	public Map<String,Object> getPlanInfoWithID(@Param("graduateProPlanID")String graduateProPlanID) throws SQLException; 
	
	public int deleteGraduatePlan(@Param("graduateProPlanID")String graduateProPlanID)throws SQLException; 
	
	public int updateGraduatePlan(Map<String,Object> map)throws SQLException; 
}

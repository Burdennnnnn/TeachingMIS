package teaching.service.designManage.courseDesign;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * 张哲 创建时间：2017年4月9日 下午8:01:00
 */
public interface CourseDesignService {
	
	public List<Map<String, Object>> getDesignBaseInfo(Map<String,Object> map) throws SQLException;
	
	public Map<String, Object> getDesignBaseInfoNum(Map<String,Object> map)throws SQLException;
	
	public List<Map<String, Object>> getCourseDesignT(Map<String,Object> map)throws SQLException;
	
	public Map<String, Object> getCourseDesignTN(Map<String,Object> map)throws SQLException;

	public List<Map<String, Object>> getTeacherDesign() throws SQLException;
	
	public List<Map<String, Object>> getAllCourseWithMajor(@Param("majorID")String majorID)throws SQLException;

	public List<Map<String, Object>> getClassDesign(@Param("majorID")String majorID) throws SQLException;

	public int deleteDesignOne(@Param("courseDesignID") String courseDesignID) throws SQLException;
	
	public int deleteDesignBatch(List<String> lists) throws SQLException;
	
	public int insertDesignInfo(Map<String,Object> map)throws SQLException;
	
	public int insertDesignTeacherClass(Map<String,Object> map)throws SQLException;
	
	public List<Map<String,Object>> getMajorDesign() throws SQLException;
	
	public List<Map<String,Object>> getCourseInfoDesign(Map<String,Object> map) throws SQLException;
	
	public Map<String,Object> getOneDesignInfo(@Param("courseDesignID")String courseDesignID)throws SQLException;
	
	public int updateDesignInfo(Map<String,Object> map)throws SQLException;
	
	public int updateDesignTeachetClass(Map<String,Object> map)throws SQLException;
	
	public List<Map<String,Object>> getCourseWithDesign() throws SQLException;
	
	public List<Map<String,Object>> getCourseWithOutDesign() throws SQLException;
	
	public List<Map<String,Object>> getCourseDesign(Map<String,Object> map) throws SQLException;
	
	public Map<String, Object> getCourseDesignNum(Map<String,Object> map)throws SQLException;
	
	public Map<String, Object> getCourseWithID(Map<String,Object> map)throws SQLException;
	
	public String getTeacherByTeacherID (@Param("teacherID")String teacherID)throws SQLException;
	
}

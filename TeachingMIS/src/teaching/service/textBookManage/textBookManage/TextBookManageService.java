package teaching.service.textBookManage.textBookManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface TextBookManageService {
	/**
	 * 查询教材信息map(start,pageSize,textbookId,courseNameCN,textbookName)
	 * @author GuoFei
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String,Object>> getTextBook2(Map<String,Object> map) throws SQLException;

	/**
	 * 增加教材信息map(textbookId,textbookName,publishingHouse,author,isbn,price,teacherCourseId,remark1)
	 * @author GuoFei
	 * @param map
	 * @return
	 * @throws SQLException
	 */
    public int addTextBook2(List<Map<String,Object>> list)throws SQLException;
    
    /**
     * 增加教材信息界面的课程信息的初始化
     * @author GuoFei
     * @return
     * @throws SQLException
     */
    public List<Map<String,Object>> getNameAndId2()throws SQLException;
    /**
     * 修改教材信息map(oldTextbookId,textbookId,textbookName,publishingHouse,author,isbn,price,teacherCourseId,remark1)
     * @author GuoFei
     * @param map
     * @return
     * @throws SQLException
     */
    public int updateTextBook2(Map<String,Object> map)throws SQLException;
    
    /**
     * 删除教材信息
     * @author GuoFei
     * @param textbookId
     * @return
     * @throws SQLException
     */
    public int deleteTextBook2(@Param("textbookId")String textbookId)throws SQLException;
    /**
     * 统计教材数量map(courseNameCN,textbookName)
     * @param map
     * @return
     * @throws SQLException
     */
    public int getTextBookCountNum(Map<String,Object> map)throws SQLException;
    /**
     * 查询教材名称及其id
     * @return
     * @throws SQLException
     */
    public  List<Map<String,Object>>  getTextBookNameAndId()throws SQLException;
    
}

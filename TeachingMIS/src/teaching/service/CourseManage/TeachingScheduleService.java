package teaching.service.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import teaching.entity.TeachingSchedule;

public interface TeachingScheduleService {

	List<TeachingSchedule> getScheduleList(Map<String, Object> condition)throws SQLException;

	boolean addSchedule(TeachingSchedule teachingSchedule)throws SQLException;

	boolean deleteSchedule(String string)throws SQLException;
}

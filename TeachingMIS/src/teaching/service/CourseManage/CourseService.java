package teaching.service.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import teaching.entity.Course;

public interface CourseService {

	List<Map<String,Object>> getCoursesForTeacher(Map<String,String> condition)throws SQLException;
	Course getCourseById(String trainingCourseID)throws SQLException;
	List<Map<String, Object>> getCoursesForStudent(Map<String, String> condition);
	String getClassID(String studentID)throws SQLException;
}

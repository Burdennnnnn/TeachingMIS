package teaching.service.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import teaching.entity.TeachingProgram;

public interface TeachingProgramService {

	boolean addProgram(TeachingProgram teachingProgram) throws SQLException;
	boolean deleteProgram(String teachingProgramID)throws SQLException;
	List<TeachingProgram> getProgramList(Map<String,Object> condition) throws SQLException;
}

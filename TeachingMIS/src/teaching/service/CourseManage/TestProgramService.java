package teaching.service.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import teaching.entity.TestProgram;

public interface TestProgramService {

	List<TestProgram> getTestProgramList(Map<String, Object> condition)throws SQLException;

	boolean addTestProgram(TestProgram testProgram)throws SQLException;

	boolean deleteTestProgram(String TestProgramID)throws SQLException;
}

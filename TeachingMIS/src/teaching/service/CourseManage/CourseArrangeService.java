package teaching.service.CourseManage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import teaching.entity.CourseArrange;
import teaching.entity.TeacherCourse;

public interface CourseArrangeService {

	List<Map<String,Object>> getCourseArrList(Map<String,String> condition) throws SQLException;
	boolean addArrange(CourseArrange courseArrange)throws SQLException;
	boolean addTeacherCourse(TeacherCourse teacherCourse)throws SQLException;
	List<Map<String,Object>> getArrangeList(Map<String,String> condition) throws SQLException;
	String getClass(Map<String,String> condition) throws SQLException;
	
	List<Map<String,Object>> getArrangeDetailList(Map<String,String> condition) throws SQLException;
	boolean updateChenck(Map<String,String> checkInfo) throws SQLException;
	boolean addCheckOpinion(Map<String,String> checkInfo) throws SQLException;
	String getCheckOpinion(String coursesArrangeID) throws SQLException;
	
	boolean updateTeacher(Map<String,String> checkInfo) throws SQLException;
	List<Map<String,String>> getTeachers(Map<String,String> majorID) throws SQLException;
	List<Map<String,Object>> getCourseList(Map<String,String> condition) throws SQLException;
	List<Map<String,String>> getClassYear(Map<String,String> majorID) throws SQLException;
	List<Map<String,String>> getWillCourse(Map<String,String> courseInfo) throws SQLException;
	List<Map<String, String>> getPreArrange(Map<String, String> preCon);
}

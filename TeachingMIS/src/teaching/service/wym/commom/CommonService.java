package teaching.service.wym.commom;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface CommonService {

	public List<Map<String,String>> getAcademicYear() throws SQLException;

	public List<Map<String, String>> getMajorList()throws SQLException;
	
	public List<Map<String, String>> getScoringWay()throws SQLException;
}

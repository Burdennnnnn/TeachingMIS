package teaching.entity;

public class Textbook {
    private String textbookid;

    private String teachercourseid;

    private String textbookname;

    private String publishinghouse;

    private String author;

    private String isbn;

    private Double price;

    private String remark1;

    private String remark2;

    public String getTextbookid() {
        return textbookid;
    }

    public void setTextbookid(String textbookid) {
        this.textbookid = textbookid == null ? null : textbookid.trim();
    }

    public String getTeachercourseid() {
        return teachercourseid;
    }

    public void setTeachercourseid(String teachercourseid) {
        this.teachercourseid = teachercourseid == null ? null : teachercourseid.trim();
    }

    public String getTextbookname() {
        return textbookname;
    }

    public void setTextbookname(String textbookname) {
        this.textbookname = textbookname == null ? null : textbookname.trim();
    }

    public String getPublishinghouse() {
        return publishinghouse;
    }

    public void setPublishinghouse(String publishinghouse) {
        this.publishinghouse = publishinghouse == null ? null : publishinghouse.trim();
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author == null ? null : author.trim();
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn == null ? null : isbn.trim();
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }
}
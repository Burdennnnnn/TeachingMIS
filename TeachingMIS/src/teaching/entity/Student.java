package teaching.entity;

import java.util.Date;

public class Student {
    private String studentid;

    private String classid;

    private String studentname;

    private String studentsex;

    private Date studentbirth;

    private String studentphoto;

    private String hostelno;

    private String studentemail;

    private String studentjob;

    private String idnum;

    private String bankcard;

    private Boolean inschool;

    private String remark1;

    private String remark2;

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid == null ? null : studentid.trim();
    }

    public String getClassid() {
        return classid;
    }

    public void setClassid(String classid) {
        this.classid = classid == null ? null : classid.trim();
    }

    public String getStudentname() {
        return studentname;
    }

    public void setStudentname(String studentname) {
        this.studentname = studentname == null ? null : studentname.trim();
    }

    public String getStudentsex() {
        return studentsex;
    }

    public void setStudentsex(String studentsex) {
        this.studentsex = studentsex == null ? null : studentsex.trim();
    }

    public Date getStudentbirth() {
        return studentbirth;
    }

    public void setStudentbirth(Date studentbirth) {
        this.studentbirth = studentbirth;
    }

    public String getStudentphoto() {
        return studentphoto;
    }

    public void setStudentphoto(String studentphoto) {
        this.studentphoto = studentphoto == null ? null : studentphoto.trim();
    }

    public String getHostelno() {
        return hostelno;
    }

    public void setHostelno(String hostelno) {
        this.hostelno = hostelno == null ? null : hostelno.trim();
    }

    public String getStudentemail() {
        return studentemail;
    }

    public void setStudentemail(String studentemail) {
        this.studentemail = studentemail == null ? null : studentemail.trim();
    }

    public String getStudentjob() {
        return studentjob;
    }

    public void setStudentjob(String studentjob) {
        this.studentjob = studentjob == null ? null : studentjob.trim();
    }

    public String getIdnum() {
        return idnum;
    }

    public void setIdnum(String idnum) {
        this.idnum = idnum == null ? null : idnum.trim();
    }

    public String getBankcard() {
        return bankcard;
    }

    public void setBankcard(String bankcard) {
        this.bankcard = bankcard == null ? null : bankcard.trim();
    }

    public Boolean getInschool() {
        return inschool;
    }

    public void setInschool(Boolean inschool) {
        this.inschool = inschool;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }
}
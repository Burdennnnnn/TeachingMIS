package teaching.entity.weixin;

public class News {

    private String newId;
    private String newTitle;
    private String newDate;
    private String newContent;
    public String getNewId() {
        return newId;
    }
    public void setNewId(String newId) {
        this.newId = newId;
    }
    public String getNewTitle() {
        return newTitle;
    }
    public void setNewTitle(String newTitle) {
        this.newTitle = newTitle;
    }
    public String getNewDate() {
        return newDate;
    }
    public void setNewDate(String newDate) {
        this.newDate = newDate;
    }
    public String getNewContent() {
        return newContent;
    }
    public void setNewContent(String newContent) {
        this.newContent = newContent;
    }
    
    
}

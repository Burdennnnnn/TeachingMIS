package teaching.entity;

public class GraduateResultKey {
    private String graduateprogramid;

    private String studentid;

    public String getGraduateprogramid() {
        return graduateprogramid;
    }

    public void setGraduateprogramid(String graduateprogramid) {
        this.graduateprogramid = graduateprogramid == null ? null : graduateprogramid.trim();
    }

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid == null ? null : studentid.trim();
    }
}
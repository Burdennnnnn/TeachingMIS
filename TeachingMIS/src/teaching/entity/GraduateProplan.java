package teaching.entity;

import java.util.Date;

public class GraduateProplan {
    private String graduateProplanid;

    private String term;

    private Date academicYear;

    private String graduatePlanname;

    private String graduatePlanstore;

    private String majorid;

    private String graduatePlancontent;

    private Date uptime;

    private String uploader;

    public String getGraduateProplanid() {
        return graduateProplanid;
    }

    public void setGraduateProplanid(String graduateProplanid) {
        this.graduateProplanid = graduateProplanid;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public Date getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(Date academicYear) {
        this.academicYear = academicYear;
    }

    public String getGraduatePlanname() {
        return graduatePlanname;
    }

    public void setGraduatePlanname(String graduatePlanname) {
        this.graduatePlanname = graduatePlanname;
    }

    public String getGraduatePlanstore() {
        return graduatePlanstore;
    }

    public void setGraduatePlanstore(String graduatePlanstore) {
        this.graduatePlanstore = graduatePlanstore;
    }

    public String getMajorid() {
        return majorid;
    }

    public void setMajorid(String majorid) {
        this.majorid = majorid;
    }

    public String getGraduatePlancontent() {
        return graduatePlancontent;
    }

    public void setGraduatePlancontent(String graduatePlancontent) {
        this.graduatePlancontent = graduatePlancontent;
    }

    public Date getUptime() {
        return uptime;
    }

    public void setUptime(Date uptime) {
        this.uptime = uptime;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    
}
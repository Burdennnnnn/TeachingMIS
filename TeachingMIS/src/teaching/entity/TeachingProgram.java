package teaching.entity;

import java.util.Date;

public class TeachingProgram {
    private String teachingProgramID;

    private String courseID;

    private String courseName;
    private String fileID;
    private String programName;
    private String programURL;
    private String revised;

    private Date time;

    private String isUse;

    private String remark1;

    private String remark2;

	public String getTeachingProgramID() {
		return teachingProgramID;
	}

	public void setTeachingProgramID(String teachingProgramID) {
		this.teachingProgramID = teachingProgramID;
	}

	public String getCourseID() {
		return courseID;
	}

	public void setCourseID(String courseID) {
		this.courseID = courseID;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getFileID() {
		return fileID;
	}

	public void setFileID(String fileID) {
		this.fileID = fileID;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public String getProgramURL() {
		return programURL;
	}

	public void setProgramURL(String programURL) {
		this.programURL = programURL;
	}

	public String getRevised() {
		return revised;
	}

	public void setRevised(String revised) {
		this.revised = revised;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getIsUse() {
		return isUse;
	}

	public void setIsUse(String isUse) {
		this.isUse = isUse;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

    
}
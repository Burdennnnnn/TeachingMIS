package teaching.entity;

import java.util.Date;

public class CourseDesign {
    private String courseDesignID;

    private String trainingCourseID;

    private String courseDesignname;
    
    private String uploader;

	private Date startTime;

    private Date endTime;

    private String courseDesigninfoName;

    private String courseDesigninfoStore;

    private String courseDesignInfo;

    private String courseArrangeInfoName;

    private String courseArrangeInfoStore;

    private String courseArrangeInfo;

    private String remark1;

    private String remark2;

    public String getCourseDesignID() {
        return courseDesignID;
    }

    public void setCourseDesignID(String courseDesignID) {
        this.courseDesignID = courseDesignID;
    }

    public String getTrainingCourseID() {
        return trainingCourseID;
    }

    public void setTrainingCourseID(String trainingCourseID) {
        this.trainingCourseID = trainingCourseID;
    }

    public String getCourseDesignname() {
        return courseDesignname;
    }

    public void setCourseDesignname(String courseDesignname) {
        this.courseDesignname = courseDesignname;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getCourseDesigninfoName() {
        return courseDesigninfoName;
    }

    public void setCourseDesigninfoName(String courseDesigninfoName) {
        this.courseDesigninfoName = courseDesigninfoName;
    }

    public String getCourseDesigninfoStore() {
        return courseDesigninfoStore;
    }

    public void setCourseDesigninfoStore(String courseDesigninfoStore) {
        this.courseDesigninfoStore = courseDesigninfoStore;
    }

    public String getCourseDesignInfo() {
        return courseDesignInfo;
    }

    public void setCourseDesignInfo(String courseDesignInfo) {
        this.courseDesignInfo = courseDesignInfo;
    }

    public String getCourseArrangeInfoName() {
        return courseArrangeInfoName;
    }

    public void setCourseArrangeInfoName(String courseArrangeInfoName) {
        this.courseArrangeInfoName = courseArrangeInfoName;
    }

    public String getCourseArrangeInfoStore() {
        return courseArrangeInfoStore;
    }

    public void setCourseArrangeInfoStore(String courseArrangeInfoStore) {
        this.courseArrangeInfoStore = courseArrangeInfoStore;
    }

    public String getCourseArrangeInfo() {
        return courseArrangeInfo;
    }

    public void setCourseArrangeInfo(String courseArrangeInfo) {
        this.courseArrangeInfo = courseArrangeInfo;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public String getUploader() {
  		return uploader;
  	}

  	public void setUploader(String uploader) {
  		this.uploader = uploader;
  	}

    
}
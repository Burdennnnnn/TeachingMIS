package teaching.entity;

import java.util.Date;

public class Course {
    private String trainingCourseID;

    private String courseID;

    private String trainingSchemeID;

    private String coursePlatform;

    private String courseNature;

    private String courseNameCN;

    private String courseNameEN;

    private String credit;

    private String courseHour;

    private String teachHour;

    private String experimentHour;

    private String computerHour;

    private String practiceHour;

    private String term;

    private String academicYear;

    private String weeklyHour;

    private String scoringWay;

    private String courseRequirement;

    private String remark1;

    private String remark2;

    public String getTrainingCourseID() {
        return trainingCourseID;
    }

    public void setTrainingCourseID(String trainingCourseID) {
        this.trainingCourseID = trainingCourseID;
    }

    public String getCourseID() {
        return courseID;
    }

    public void setCourseID(String courseID) {
        this.courseID = courseID;
    }

    public String getTrainingSchemeID() {
        return trainingSchemeID;
    }

    public void setTrainingSchemeID(String trainingSchemeID) {
        this.trainingSchemeID = trainingSchemeID;
    }

    public String getCoursePlatform() {
        return coursePlatform;
    }

    public void setCoursePlatform(String coursePlatform) {
        this.coursePlatform = coursePlatform;
    }

    public String getCourseNature() {
        return courseNature;
    }

    public void setCourseNature(String courseNature) {
        this.courseNature = courseNature;
    }

    public String getCourseNameCN() {
        return courseNameCN;
    }

    public void setCourseNameCN(String courseNameCN) {
        this.courseNameCN = courseNameCN;
    }

    public String getCourseNameEN() {
        return courseNameEN;
    }

    public void setCourseNameEN(String courseNameEN) {
        this.courseNameEN = courseNameEN;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getCourseHour() {
        return courseHour;
    }

    public void setCourseHour(String courseHour) {
        this.courseHour = courseHour;
    }

    public String getTeachHour() {
        return teachHour;
    }

    public void setTeachHour(String teachHour) {
        this.teachHour = teachHour;
    }

    public String getExperimentHour() {
        return experimentHour;
    }

    public void setExperimentHour(String experimentHour) {
        this.experimentHour = experimentHour;
    }

    public String getComputerHour() {
        return computerHour;
    }

    public void setComputerHour(String computerHour) {
        this.computerHour = computerHour;
    }

    public String getPracticeHour() {
        return practiceHour;
    }

    public void setPracticeHour(String practiceHour) {
        this.practiceHour = practiceHour;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getWeeklyHour() {
        return weeklyHour;
    }

    public void setWeeklyHour(String weeklyHour) {
        this.weeklyHour = weeklyHour;
    }

    public String getScoringWay() {
        return scoringWay;
    }

    public void setScoringWay(String scoringWay) {
        this.scoringWay = scoringWay;
    }

    public String getCourseRequirement() {
        return courseRequirement;
    }

    public void setCourseRequirement(String courseRequirement) {
        this.courseRequirement = courseRequirement;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

	@Override
	public String toString() {
		return "Course [trainingCourseID=" + trainingCourseID + ", courseID=" + courseID + ", trainingSchemeID="
				+ trainingSchemeID + ", coursePlatform=" + coursePlatform + ", courseNature=" + courseNature
				+ ", courseNameCN=" + courseNameCN + ", courseNameEN=" + courseNameEN + ", credit=" + credit
				+ ", courseHour=" + courseHour + ", teachHour=" + teachHour + ", experimentHour=" + experimentHour
				+ ", computerHour=" + computerHour + ", practiceHour=" + practiceHour + ", term=" + term
				+ ", academicYear=" + academicYear + ", weeklyHour=" + weeklyHour + ", scoringWay=" + scoringWay
				+ ", courseRequirement=" + courseRequirement + ", remark1=" + remark1 + ", remark2=" + remark2 + "]";
	}

    
}
package teaching.entity;

public class FileIndex {
	
	    private String fileID;

	    private String fileType;

	    private String originalName;

	    private String currentName;

	    private String fileUrl;

	    private String remark;

	    public String getFileID() {
	        return fileID;
	    }

	    public void setFileId(String fileID) {
	        this.fileID = fileID == null ? null : fileID.trim();
	    }

	    public String getFileType() {
	        return fileType;
	    }

	    public void setFileType(String fileType) {
	        this.fileType = fileType == null ? null : fileType.trim();
	    }

	    public String getOriginalName() {
	        return originalName;
	    }

	    public void setOriginalName(String originalName) {
	        this.originalName = originalName == null ? null : originalName.trim();
	    }

	    public String getCurrentName() {
	        return currentName;
	    }

	    public void setCurrentName(String currentName) {
	        this.currentName = currentName == null ? null : currentName.trim();
	    }

	    public String getFileUrl() {
	        return fileUrl;
	    }

	    public void setFileUrl(String fileUrl) {
	        this.fileUrl = fileUrl == null ? null : fileUrl.trim();
	    }

	    public String getRemark() {
	        return remark;
	    }

	    public void setRemark(String remark) {
	        this.remark = remark == null ? null : remark.trim();
	    }
}

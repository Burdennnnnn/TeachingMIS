package teaching.entity;

public class TeacherCourse {

	private String teacherCourseID;
	private String teacherID;
	private String courseArrangeID;
	private String trainingCourseID;
	private String courseID;
	private String classID;
	private String isUseMedia;
	public String getTeacherCourseID() {
		return teacherCourseID;
	}
	public void setTeacherCourseID(String teacherCourseID) {
		this.teacherCourseID = teacherCourseID;
	}
	public String getTeacherID() {
		return teacherID;
	}
	public void setTeacherID(String teacherID) {
		this.teacherID = teacherID;
	}
	public String getCourseArrangeID() {
		return courseArrangeID;
	}
	public void setCourseArrangeID(String courseArrangeID) {
		this.courseArrangeID = courseArrangeID;
	}
	public String getTrainingCourseID() {
		return trainingCourseID;
	}
	public void setTrainingCourseID(String trainingCourseID) {
		this.trainingCourseID = trainingCourseID;
	}
	public String getCourseID() {
		return courseID;
	}
	public void setCourseID(String courseID) {
		this.courseID = courseID;
	}
	
	public String getClassID() {
		return classID;
	}
	public void setClassID(String classID) {
		this.classID = classID;
	}
	public String getIsUseMedia() {
		return isUseMedia;
	}
	public void setIsUseMedia(String isUseMedia) {
		this.isUseMedia = isUseMedia;
	}
	
}

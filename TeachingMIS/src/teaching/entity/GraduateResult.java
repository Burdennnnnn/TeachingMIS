package teaching.entity;

public class GraduateResult extends GraduateResultKey {
	
	private String graduateProgramID;
	private String studentID;
    private String graduateResultname;

    private String graduateResultstore;

    private String graduateResult;

    private String dissertationname;

    private String dissertationStore;

    private String dissertation;

    private String remark1;

    private String remark2;

    public String getGraduateResultname() {
        return graduateResultname;
    }

    public void setGraduateResultname(String graduateResultname) {
        this.graduateResultname = graduateResultname;
    }

    public String getGraduateResultstore() {
        return graduateResultstore;
    }

    public void setGraduateResultstore(String graduateResultstore) {
        this.graduateResultstore = graduateResultstore;
    }

    public String getGraduateResult() {
        return graduateResult;
    }

    public void setGraduateResult(String graduateResult) {
        this.graduateResult = graduateResult;
    }

    public String getDissertationname() {
        return dissertationname;
    }

    public void setDissertationname(String dissertationname) {
        this.dissertationname = dissertationname;
    }

    public String getDissertationStore() {
        return dissertationStore;
    }

    public void setDissertationStore(String dissertationStore) {
        this.dissertationStore = dissertationStore;
    }

    public String getDissertation() {
        return dissertation;
    }

    public void setDissertation(String dissertation) {
        this.dissertation = dissertation;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

	public String getGraduateProgramID() {
		return graduateProgramID;
	}

	public void setGraduateProgramID(String graduateProgramID) {
		this.graduateProgramID = graduateProgramID;
	}

	public String getStudentID() {
		return studentID;
	}

	public void setStudentID(String studentID) {
		this.studentID = studentID;
	}

    
}
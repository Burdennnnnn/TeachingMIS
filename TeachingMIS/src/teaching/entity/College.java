package teaching.entity;

public class College {
    private String collegeID;

    private String collegeName;

    private String collegeShortName;

    private String collegeIntroduction;

    private String collegePresident;

    private String collegePresidentF;


    public String getCollegeID() {
        return collegeID;
    }

    public void setCollegeID(String collegeID) {
        this.collegeID = collegeID;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    
    public String getCollegeShortName() {
		return collegeShortName;
	}

	public void setCollegeShortName(String collegeShortName) {
		this.collegeShortName = collegeShortName;
	}

	public String getCollegeIntroduction() {
        return collegeIntroduction;
    }

    public void setCollegeIntroduction(String collegeIntroduction) {
        this.collegeIntroduction = collegeIntroduction;
    }

    public String getCollegePresident() {
        return collegePresident;
    }

    public void setCollegePresident(String collegePresident) {
        this.collegePresident = collegePresident;
    }

    
    public String getCollegePresidentF() {
		return collegePresidentF;
	}

	public void setCollegePresidentF(String collegePresidentF) {
		this.collegePresidentF = collegePresidentF;
	}

    
}
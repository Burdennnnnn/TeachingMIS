package teaching.entity;

import java.util.Date;


public class TrainingSchema {
    private String trainingSchemeID;

    private String majorID;

    private String majorName;
    private String revised;

    private String auditor;

    private String createTime;

    private String fileID;
    private String fileName;
    private String fileURL;
    
    private String remark1;


    
    public String getTrainingSchemeID() {
        return trainingSchemeID;
    }

    public void setTrainingSchemeID(String trainingSchemeID) {
        this.trainingSchemeID = trainingSchemeID;
    }

    public String getMajorID() {
        return majorID;
    }

    public void setMajorID(String majorID) {
        this.majorID = majorID;
    }
    
    public String getMajorName() {
		return majorName;
	}

	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}

	public String getRevised() {
        return revised;
    }

    public void setRevised(String revised) {
        this.revised = revised == null ? null : revised.trim();
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor == null ? null : auditor.trim();
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    
    public String getFileID() {
		return fileID;
	}

	public void setFileID(String fileID) {
		this.fileID = fileID;
	}

    public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileURL() {
		return fileURL;
	}

	public void setFileURL(String fileURL) {
		this.fileURL = fileURL;
	}

	public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }


	@Override
	public String toString() {
		return "TrainingSchema [trainingSchemeID=" + trainingSchemeID + ", majorID=" + majorID + ", revised=" + revised
				+ ", auditor=" + auditor + ", createTime=" + createTime + ", fileID=" + fileID + 
				 ", remark1=" + remark1 + "]";
	}

	
}
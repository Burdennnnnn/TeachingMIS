package teaching.entity;

import java.util.Date;

import com.mysql.fabric.xmlrpc.base.Data;

public class CourseArrange {
    private String coursesArrangeID;

    private String majorID;

    private String enrollmentTime;
    
    private String term;
    
    private String academicYear;

    private String isCheck;

    private Data createTime;
    
    private String classID;
    
	public String getCoursesArrangeID() {
		return coursesArrangeID;
	}
	public void setCoursesArrangeID(String coursesArrangeID) {
		this.coursesArrangeID = coursesArrangeID;
	}
	public String getMajorID() {
		return majorID;
	}
	public void setMajorID(String majorID) {
		this.majorID = majorID;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getEnrollmentTime() {
		return enrollmentTime;
	}
	public void setEnrollmentTime(String enrollmentTime) {
		this.enrollmentTime = enrollmentTime;
	}
	public String getAcademicYear() {
		return academicYear;
	}
	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}
	public String getIsCheck() {
		return isCheck;
	}
	public void setIsCheck(String isCheck) {
		this.isCheck = isCheck;
	}
	public Data getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Data createTime) {
		this.createTime = createTime;
	}
	public String getClassID() {
		return classID;
	}
	public void setClassID(String classID) {
		this.classID = classID;
	}
   

    
}
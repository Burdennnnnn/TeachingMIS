package teaching.entity;

import java.util.Date;

public class Major {
    private String majorId;

    private String collegeId;

    private String majorName;

    private String majorShortName;

    private String majorSort;

    private String majorManager;
    
    private String teachingDirector;
    
    private Date createTime;

    private String remark1;

    private String remark2;

    

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

	public String getMajorId() {
		return majorId;
	}

	public void setMajorId(String majorId) {
		this.majorId = majorId;
	}

	public String getCollegeId() {
		return collegeId;
	}

	public void setCollegeId(String collegeId) {
		this.collegeId = collegeId;
	}

	public String getMajorName() {
		return majorName;
	}

	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}

	public String getMajorShortName() {
		return majorShortName;
	}

	public void setMajorShortName(String majorShortName) {
		this.majorShortName = majorShortName;
	}

	public String getMajorSort() {
		return majorSort;
	}

	public void setMajorSort(String majorSort) {
		this.majorSort = majorSort;
	}

	public String getMajorManager() {
		return majorManager;
	}

	public void setMajorManager(String majorManager) {
		this.majorManager = majorManager;
	}

	public String getTeachingDirector() {
		return teachingDirector;
	}

	public void setTeachingDirector(String teachingDirector) {
		this.teachingDirector = teachingDirector;
	}

	@Override
	public String toString() {
		return "Major [majorId=" + majorId + ", collegeId=" + collegeId
				+ ", majorName=" + majorName + ", majorShortName="
				+ majorShortName + ", majorSort=" + majorSort
				+ ", majorManager=" + majorManager + ", teachingDirector="
				+ teachingDirector + ", remark1=" + remark1 + ", remark2="
				+ remark2 + "]";
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	
}
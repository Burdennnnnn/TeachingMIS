package teaching.entity;

import java.util.Date;

public class TeachingSchedule {
    private String teachingScheduleID;

    private String teacherCourseID;

    private String fileID;
    
    private String fileName;
    
    private String fileURL;

    private String uplodePerson;

    private Date uplodeTime;

    private String isUse;

    private String remark;

	public String getTeachingScheduleID() {
		return teachingScheduleID;
	}

	public void setTeachingScheduleID(String teachingScheduleID) {
		this.teachingScheduleID = teachingScheduleID;
	}

	public String getTeacherCourseID() {
		return teacherCourseID;
	}

	public void setTeacherCourseID(String teacherCourseID) {
		this.teacherCourseID = teacherCourseID;
	}

	public String getFileID() {
		return fileID;
	}

	public void setFileID(String fileID) {
		this.fileID = fileID;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileURL() {
		return fileURL;
	}

	public void setFileURL(String fileURL) {
		this.fileURL = fileURL;
	}

	public String getUplodePerson() {
		return uplodePerson;
	}

	public void setUplodePerson(String uplodePerson) {
		this.uplodePerson = uplodePerson;
	}

	public Date getUplodeTime() {
		return uplodeTime;
	}

	public void setUplodeTime(Date uplodeTime) {
		this.uplodeTime = uplodeTime;
	}

	

	public String getIsUse() {
		return isUse;
	}

	public void setIsUse(String isUse) {
		this.isUse = isUse;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

    
    
}
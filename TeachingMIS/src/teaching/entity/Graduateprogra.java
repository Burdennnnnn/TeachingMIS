package teaching.entity;

import java.util.Date;

public class Graduateprogra {
    private String graduateProgramID;

    private String teacherID;

    private String graduateProplanID;

    private Date graduateProgramTime;

    private String graduateTitle;

    private String titleIntroduce;

    private String titleAttachment;

    private String titleSource;

    private String itemCategoryName;

    private String itemCategoryID;

    private Integer num;

    private String majorID;

    private String remark1;

    private String remark2;

    public String getGraduateProgramID() {
        return graduateProgramID;
    }

    public void setGraduateProgramID(String graduateProgramID) {
        this.graduateProgramID = graduateProgramID;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    public String getGraduateProplanID() {
        return graduateProplanID;
    }

    public void setGraduateProplanID(String graduateProplanID) {
        this.graduateProplanID = graduateProplanID;
    }

    public Date getGraduateProgramTime() {
        return graduateProgramTime;
    }

    public void setGraduateProgramTime(Date graduateProgramTime) {
        this.graduateProgramTime = graduateProgramTime;
    }

    public String getGraduateTitle() {
        return graduateTitle;
    }

    public void setGraduateTitle(String graduateTitle) {
        this.graduateTitle = graduateTitle;
    }

    public String getTitleIntroduce() {
        return titleIntroduce;
    }

    public void setTitleIntroduce(String titleIntroduce) {
        this.titleIntroduce = titleIntroduce;
    }

    public String getTitleAttachment() {
        return titleAttachment;
    }

    public void setTitleAttachment(String titleAttachment) {
        this.titleAttachment = titleAttachment;
    }

    public String getTitleSource() {
        return titleSource;
    }

    public void setTitleSource(String titleSource) {
        this.titleSource = titleSource;
    }

    public String getItemCategoryName() {
        return itemCategoryName;
    }

    public void setItemCategoryName(String itemCategoryName) {
        this.itemCategoryName = itemCategoryName;
    }

    public String getItemCategoryID() {
        return itemCategoryID;
    }

    public void setItemCategoryID(String itemCategoryID) {
        this.itemCategoryID = itemCategoryID;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getMajorID() {
        return majorID;
    }

    public void setMajorID(String majorID) {
        this.majorID = majorID;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

   
}
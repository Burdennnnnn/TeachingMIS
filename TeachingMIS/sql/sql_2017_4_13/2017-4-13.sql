#将课程信息表现中的学年改成了String类型  便于存储类似“2016-2017”这样的字段
alter table t_course_base_info modify academicYear varchar(20);
#向 课设信息表中添加专业编号列表
alter table t_coursedesign_info add majorID varchar(20);
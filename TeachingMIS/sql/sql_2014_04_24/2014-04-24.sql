ALTER TABLE `t_teachingprogram_info`
DROP COLUMN `teachingProgramStore`,
DROP COLUMN `teachingProgram`,
CHANGE COLUMN `teachingProgramName` `fileID`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `courseID`;

ALTER TABLE `t_coursesarrange_info`
ADD COLUMN `createTime`  datetime NULL ON UPDATE CURRENT_TIMESTAMP AFTER `isCheck`;

SELECT
	`t_coursesarrange_info`.`coursesArrangeID` AS `coursesArrangeID`,
	`t_coursesarrange_info`.`term` AS `term`,
	`t_coursesarrange_info`.`academicYear` AS `academicYear`,
	`teacher_course`.`teacherID` AS `teacherID`,
	`teacher_course`.`trainingCourseID` AS `trainingCourseID`,
	`t_course_base_info`.`courseID` AS `courseID`,
	`t_course_base_info`.`coursePlatform` AS `coursePlatform`,
	`t_course_base_info`.`courseNature` AS `courseNature`,
	`t_course_base_info`.`courseNameCN` AS `courseNameCN`,
	`t_course_base_info`.`courseNameEN` AS `courseNameEN`,
	`t_course_base_info`.`credit` AS `credit`,
	`t_course_base_info`.`courseHour` AS `courseHour`,
	`t_course_base_info`.`teachHour` AS `teachHour`,
	`t_course_base_info`.`experimentHour` AS `experimentHour`,
	`t_course_base_info`.`computerHour` AS `computerHour`,
	`t_course_base_info`.`practiceHour` AS `practiceHour`,
	`t_course_base_info`.`weeklyHour` AS `weeklyHour`,
	`t_course_base_info`.`scoringWay` AS `scoringWay`,
	`t_course_base_info`.`courseRequirement` AS `courseRequirement`,
	`teacher_course`.`teacherCourseID` AS `teacherCourseID`
FROM
	(
		(
			`t_coursesarrange_info`
			JOIN `teacher_course`
		)
		JOIN `t_course_base_info`
	)
WHERE
	(
		(
			`teacher_course`.`coursesArrangeID` = `t_coursesarrange_info`.`coursesArrangeID`
		)
		AND (
			`teacher_course`.`trainingCourseID` = `t_course_base_info`.`trainingCourseID`
		)
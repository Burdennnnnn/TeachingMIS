#修改t_coursedesign_info 中 的 courseID 为trainingCourseID
alter table t_coursedesign_info CHANGE column courseID trainingCourseID varchar(20);
#向coursedesign_teacher表中添加classID列
alter table coursedesign_teacher add classID varchar(20);
#向t_coursedesign_info表中添加上传者uploader字段
alter table t_coursedesign_info add uploader varchar(20);
#将coursedesign_teacher表中的courseDesignID设为外键并设置级联操作
alter table coursedesign_teacher  add CONSTRAINT del_design FOREIGN KEY (courseDesignID)  REFERENCES t_coursedesign_info  (courseDesignID)
on DELETE CASCADE on UPDATE CASCADE;
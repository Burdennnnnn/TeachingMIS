--创建文件表
CREATE TABLE `t_file_index` (
`fileID`  varchar(36) NOT NULL ,
`fileType`  varchar(6) NULL ,
`originalName`  varchar(100) NULL ,
`currentName`  varchar(100) NULL ,
`fileURL`  varchar(255) NULL ,
`remark`  varchar(255) NULL ,
PRIMARY KEY (`filedID`)
);

ALTER TABLE `t_trainingscheme_info`
DROP COLUMN `trainingSchemeName`,
DROP COLUMN `trainingSchemeStore`,
DROP COLUMN `trainingScheme`;

ALTER TABLE `teacher_course`
DROP COLUMN `isUseMedia`,
ADD COLUMN `isUseMedia`  bit NULL AFTER `courseID`;

ALTER TABLE `teacher_course`
ADD COLUMN `class`  varchar(100) NULL AFTER `isUseMedia`;


ALTER TABLE `t_coursesarrange_info`
MODIFY COLUMN `academicYear`  varchar(30) NULL DEFAULT NULL AFTER `term`;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_coursematerial
-- ----------------------------
DROP TABLE IF EXISTS `t_coursematerial`;
CREATE TABLE `t_coursematerial` (
  `materialID` varchar(40) NOT NULL,
  `teacherCourseID` varchar(36) DEFAULT NULL,
  `dailyGradeReport` varchar(16) DEFAULT '未提交',
  `dayGraRepTime` varchar(30) DEFAULT NULL,
  `experimentReport` varchar(16) DEFAULT '未提交',
  `experiRepTime` varchar(30) DEFAULT NULL,
  `testPaper` varchar(16) DEFAULT '未提交',
  `testPaperTime` varchar(30) DEFAULT NULL,
  `testPaperAnalyse` varchar(16) DEFAULT '未提交',
  `testPaAnalyTime` varchar(30) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `remark1` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`materialID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_coursematerial
-- ----------------------------
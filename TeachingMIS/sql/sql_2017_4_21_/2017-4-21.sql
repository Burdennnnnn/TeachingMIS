主要是将student_graduateprogram中的主键由graduateProgramID改成studentID
1、删除student_graduateprogram中graduateProgramID的外键关联
alter table student_graduateprogram drop foreign key FK_Reference_23;
2、删除主键graduateProgramID
alter table student_graduateprogram drop primary key;
3、添加主键studentID
alter table student_graduateprogram add primary key(studentID);
4、增加外键关系
alter table student_graduateprogram  add CONSTRAINT del_graduateprogram FOREIGN KEY (graduateProgramID)  REFERENCES t_graduateprogram_info (graduateProgramID)
on DELETE CASCADE on UPDATE CASCADE;

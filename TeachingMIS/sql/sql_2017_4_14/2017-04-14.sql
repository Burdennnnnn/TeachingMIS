#将t_graduateproplan_info中的学年改成了String类型  便于存储类似“2016-2017”这样的字段
alter table t_graduateproplan_info modify academicYear varchar(20);
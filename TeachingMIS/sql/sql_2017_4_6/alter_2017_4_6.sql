#将班级表中的专业id设置为外键，并设置级联操作
alter table t_class_base_info  add CONSTRAINT del_major FOREIGN KEY (majorID)  REFERENCES t_major_base_info  (majorID)
on DELETE CASCADE on UPDATE CASCADE;
#将学生表中的班级id设置为外键，并设置级联操作
alter table t_student_base_info  add CONSTRAINT del_class FOREIGN KEY (classID)  REFERENCES t_class_base_info  (classID)
on DELETE CASCADE on UPDATE CASCADE;
#将毕设结果表中的学号id设置为外键，并设置级联操作
alter table t_graduateResult_info  add CONSTRAINT del_student FOREIGN KEY (studentID)  REFERENCES t_student_base_info  (studentID)
on DELETE CASCADE on UPDATE CASCADE;

create view v_classCourse as 
select majorID,trainingSchemeID,YEAR(enrollmentTime) as enrollmentTime,
group_concat(classID order by classID separator ",")as classID,
group_concat(className order by className separator ",") as className
 from t_class_base_info GROUP BY majorID,YEAR(enrollmentTime),trainingSchemeID;
 
 
 alter TABLE coursedesign_teacher modify classID varchar(100);
 
 alter table coursedesign_teacher drop foreign key FK_Reference_21;
-- 向毕设信息表中添加学期 学年属性 题目附件名称
alter table t_graduateprogram_info add term varchar(20);
alter table t_graduateprogram_info add academicYear varchar(20);
alter table t_graduateprogram_info add titleAttachmentName varchar(20);

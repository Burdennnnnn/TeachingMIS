alter table  t_major_base_info add createTime datetime;
alter table  t_college_base_info add createTime datetime;
alter table  t_class_base_info add createTime datetime;
alter table  t_student_base_info add createTime datetime;
alter table  t_teacher_base_info add createTime datetime;
alter table t_class_base_info drop createTime;
alter table  t_class_base_info add createTime datetime;

